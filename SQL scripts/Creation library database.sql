-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema library
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `library` ;

-- -----------------------------------------------------
-- Schema library
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `library` DEFAULT CHARACTER SET utf8 ;
USE `library` ;

-- -----------------------------------------------------
-- Table `library`.`author`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `library`.`author` ;

CREATE TABLE IF NOT EXISTS `library`.`author` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name_EN` VARCHAR(100) NOT NULL,
  `name_UA` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `name_EN_UNIQUE` (`name_EN` ASC) VISIBLE,
  UNIQUE INDEX `name_UA_UNIQUE` (`name_UA` ASC) VISIBLE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `library`.`book`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `library`.`book` ;

CREATE TABLE IF NOT EXISTS `library`.`book` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(100) NOT NULL,
  `author_id` INT NOT NULL,
  `publisher_house` VARCHAR(45) NOT NULL,
  `year` YEAR(4) NOT NULL,
  `language` ENUM('EN', 'UA') NOT NULL COMMENT 'Language of the translation of the book',
  `quantity` INT NOT NULL,
  PRIMARY KEY (`id`),
  FULLTEXT INDEX `idx_book_title` (`title`) INVISIBLE,
  INDEX `fk_book_author1_idx` (`author_id` ASC) VISIBLE,
  FULLTEXT INDEX `idx_book_publisher_house` (`publisher_house`) VISIBLE,
  CONSTRAINT `fk_book_author1`
    FOREIGN KEY (`author_id`)
    REFERENCES `library`.`author` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `library`.`user`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `library`.`user` ;

CREATE TABLE IF NOT EXISTS `library`.`user` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `login` VARCHAR(45) NOT NULL,
  `email` VARCHAR(45) NOT NULL,
  `password` VARCHAR(60) NOT NULL,
  `phone_number` VARCHAR(9) NULL,
  `role` ENUM('reader', 'librarian', 'administrator') NOT NULL COMMENT 'User roles:\nreader - ordinary user\nlibrarian - employee of the library that can provide books to the readers (modify subscriptions)\nadministrator - employee of the library that can modify book and user lists',
  `first_name` VARCHAR(45) NOT NULL,
  `last_name` VARCHAR(45) NOT NULL,
  `blocked_at` TIMESTAMP NULL COMMENT 'If this field is not NULL then the user is blocked',
  PRIMARY KEY (`id`),
  UNIQUE INDEX `email_UNIQUE` (`email` ASC) VISIBLE,
  UNIQUE INDEX `phone_number_UNIQUE` (`phone_number` ASC) VISIBLE,
  UNIQUE INDEX `login_UNIQUE` (`login` ASC) VISIBLE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `library`.`subscription`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `library`.`subscription` ;

CREATE TABLE IF NOT EXISTS `library`.`subscription` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `user_id` INT NOT NULL,
  `type` ENUM('reading room', 'home') NOT NULL COMMENT 'Type of the subsctiption: \nreading room - book can be ordered for inside library use for one day\nhome - book can be ordered for home use for several days (q-ty of days is provided by librarian, but not more that 14 days)',
  `status` ENUM('requested', 'confirmed', 'ongoing', 'closed') NOT NULL DEFAULT 'requested' COMMENT 'Status of the subscription:\nrequested - created by reader\nconfirmed - confirmed by librarian that book is ready to be provided\nongoing - book is in the subscription\nclosed - subscription is closed',
  `created_at` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `book_return_deadline` DATE NULL COMMENT 'Deadline date (inclusively) when book(s) can be returned without the fine',
  `fine` DECIMAL(5,2) NOT NULL DEFAULT 0.00,
  PRIMARY KEY (`id`),
  INDEX `fk_subscription_user1_idx` (`user_id` ASC) INVISIBLE,
  INDEX `idx_created_at` (`created_at` ASC) INVISIBLE,
  INDEX `idx_updated_at` (`updated_at` ASC) VISIBLE,
  CONSTRAINT `fk_subscription_user1`
    FOREIGN KEY (`user_id`)
    REFERENCES `library`.`user` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `library`.`subscription_has_book`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `library`.`subscription_has_book` ;

CREATE TABLE IF NOT EXISTS `library`.`subscription_has_book` (
  `subscription_id` INT NOT NULL,
  `book_id` INT NOT NULL,
  PRIMARY KEY (`subscription_id`, `book_id`),
  INDEX `fk_subscription_has_book_book1_idx` (`book_id` ASC) INVISIBLE,
  INDEX `fk_subscription_has_book_subscription1_idx` (`subscription_id` ASC) VISIBLE,
  CONSTRAINT `fk_subscription_has_book_subscription1`
    FOREIGN KEY (`subscription_id`)
    REFERENCES `library`.`subscription` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_subscription_has_book_book1`
    FOREIGN KEY (`book_id`)
    REFERENCES `library`.`book` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
