/* book constraints */
ALTER TABLE book
ADD CONSTRAINT CHK_correct_quantity CHECK (quantity >= 0),
ADD CONSTRAINT CHK_identical_data UNIQUE (title, author_id, publisher_house, `year`, `language`);

/* user constraints */
ALTER TABLE `user`
ADD CONSTRAINT CHK_correct_login CHECK (login REGEXP '^[a-zA-ZЄ-ЯҐа-їґ0-9_-]{3,45}$'),
ADD CONSTRAINT CHK_correct_email CHECK (email REGEXP '^[a-zA-Z0-9][a-zA-Z0-9.!#$%&\'*+-/=?^_`{|}~]*?[a-zA-Z0-9._-]?@[a-zA-Z0-9][a-zA-Z0-9._-]*?[a-zA-Z0-9]?\\.[a-zA-Z]{2,63}$'),
ADD CONSTRAINT CHK_correct_first_name CHECK (first_name REGEXP '^[a-zA-ZЄ-ЯҐа-їґ\']{2,45}$'),
ADD CONSTRAINT CHK_correct_last_name CHECK (first_name REGEXP '^[a-zA-ZЄ-ЯҐа-їґ\']{2,45}$'),
ADD CONSTRAINT CHK_correct_phone_number CHECK (phone_number REGEXP '^[1-9][0-9]{8}$');


