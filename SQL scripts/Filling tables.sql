/* filling author */
INSERT INTO author (name_EN, name_UA)
VALUES
	('George R. R. Martin', 'Джордж Р.Р. Мартін'),
	('Taras Shevchenko', 'Тарас Шевченко'),
	('Ivan Franko', 'Іван Франко'),
	('George Orwell', 'Джордж Орвелл'),
	('John R. R. Tolkien', 'Джон Р. Р. Толкін'),
	('Jack London', 'Джек Лондон'),
	('Joanne Rowling', 'Джоан Роулінг'),
	('Howard Lovecraft', 'Говард Лавкрафт');

/* filling book */
INSERT INTO book (title, author_id, publisher_house, `year`, `language`, quantity)
VALUES
	('A Game of Thrones', (SELECT id FROM author WHERE name_EN = 'George R. R. Martin'), 'Voyager Books', 1996, 'EN', 2),
	('A Clash of Kings', (SELECT id FROM author WHERE name_EN = 'George R. R. Martin'), 'Voyager Books', 1998, 'EN', 1),
	('A Storm of Swords', (SELECT id FROM author WHERE name_EN = 'George R. R. Martin'), 'Voyager Books', 2000, 'EN', 2),
	('A Feast for Crows', (SELECT id FROM author WHERE name_EN = 'George R. R. Martin'), 'Bantam Spectra', 2005, 'EN', 1),
	('A Dance with Dragons', (SELECT id FROM author WHERE name_EN = 'George R. R. Martin'), 'Bantam Spectra', 2011, 'EN', 3),
	('Танок Драконів', (SELECT id FROM author WHERE name_UA = 'Джордж Р.Р. Мартін'), 'КМ-Букс', 2018, 'UA', 2),
	('Вибране', (SELECT id FROM author WHERE name_UA = 'Тарас Шевченко'), 'Фоліо', 2008, 'UA', 3),
	('Гайдамаки', (SELECT id FROM author WHERE name_UA = 'Тарас Шевченко'), 'Фоліо', 2011, 'UA', 5),
	('Захар Беркут', (SELECT id FROM author WHERE name_UA = 'Іван Франко'), 'Фоліо', 2009, 'UA', 2),
	('Zakhar Berkut', (SELECT id FROM author WHERE name_EN = 'Ivan Franko'), 'Bantam Spectra', 2004, 'EN', 1),
	('Лірика', (SELECT id FROM author WHERE name_UA = 'Іван Франко'), 'Фоліо', 2007, 'UA', 2),
	('1984', (SELECT id FROM author WHERE name_UA = 'Джордж Орвелл'), 'Андронум', 2021, 'UA', 1),
	('1984', (SELECT id FROM author WHERE name_EN = 'George Orwell'), 'Vintage', 2015, 'EN', 1),
	('Гобіт, або Туди і Звідти', (SELECT id FROM author WHERE name_UA = 'Джон Р. Р. Толкін'), 'Астролябія', 2020, 'UA', 3),
	('Володар Перснів. Братство Персня', (SELECT id FROM author WHERE name_UA = 'Джон Р. Р. Толкін'), 'Астролябія', 2019, 'UA', 2),
	('Володар Перснів. Дві вежі', (SELECT id FROM author WHERE name_UA = 'Джон Р. Р. Толкін'), 'Астролябія', 2019, 'UA', 2),
	('Володар Перснів. Повернення Короля', (SELECT id FROM author WHERE name_UA = 'Джон Р. Р. Толкін'), 'Астролябія', 2020, 'UA', 2),
	('The Silmarillion', (SELECT id FROM author WHERE name_EN = 'John R. R. Tolkien'), 'George Allen & Unwin', 2005, 'EN', 1),
	('Martin Eden', (SELECT id FROM author WHERE name_EN = 'Jack London'), 'George Allen & Unwin', 2009, 'EN', 1),
	('White Fang', (SELECT id FROM author WHERE name_EN = 'Jack London'), 'Vintage', 2018, 'EN', 1),
	('Hearts of Three', (SELECT id FROM author WHERE name_EN = 'Jack London'), 'Vintage', 2022, 'EN', 1),
	('Серця Трьох', (SELECT id FROM author WHERE name_UA = 'Джек Лондон'), 'Арій', 2019, 'UA', 2),
	('Гаррі Поттер і Філософський Камінь', (SELECT id FROM author WHERE name_UA = 'Джоан Роулінг'), 'А-ба-ба-га-ла-ма-га', 2018, 'UA', 4),
	('Гаррі Поттер і Таємна Кімната', (SELECT id FROM author WHERE name_UA = 'Джоан Роулінг'), 'А-ба-ба-га-ла-ма-га', 2017, 'UA', 3),
	('Гаррі Поттер і В\'язень Азкабану', (SELECT id FROM author WHERE name_UA = 'Джоан Роулінг'), 'А-ба-ба-га-ла-ма-га', 2017, 'UA', 4),
	('Гаррі Поттер і Келих Вогню', (SELECT id FROM author WHERE name_UA = 'Джоан Роулінг'), 'А-ба-ба-га-ла-ма-га', 2021, 'UA', 4),
	('Гаррі Поттер і Орден Фенікса', (SELECT id FROM author WHERE name_UA = 'Джоан Роулінг'), 'А-ба-ба-га-ла-ма-га', 2020, 'UA', 4),
	('Гаррі Поттер і Напікровний Принц', (SELECT id FROM author WHERE name_UA = 'Джоан Роулінг'), 'А-ба-ба-га-ла-ма-га', 2019, 'UA', 3),
	('Гаррі Поттер і Смертельні Реліквії', (SELECT id FROM author WHERE name_UA = 'Джоан Роулінг'), 'А-ба-ба-га-ла-ма-га', 2018, 'UA', 3),
	('The Cuckoo\'s Calling', (SELECT id FROM author WHERE name_EN = 'Joanne Rowling'), 'Voyager Books', 2015, 'EN', 2),
	('The Silkworm', (SELECT id FROM author WHERE name_EN = 'Joanne Rowling'), 'Voyager Books', 2016, 'EN', 1),
	('Career of Evil', (SELECT id FROM author WHERE name_EN = 'Joanne Rowling'), 'Voyager Books', 2016, 'EN', 1),
	('Lethal White', (SELECT id FROM author WHERE name_EN = 'Joanne Rowling'), 'Voyager Books', 2019, 'EN', 1),
	('Troubled Blood', (SELECT id FROM author WHERE name_EN = 'Joanne Rowling'), 'Voyager Books', 2021, 'EN', 2),
	('The Call of Cthulhu', (SELECT id FROM author WHERE name_EN = 'Howard Lovecraft'), 'Weird Tales', 2007, 'EN', 2),
	('Dagon', (SELECT id FROM author WHERE name_EN = 'Howard Lovecraft'), 'Weird Tales', 2007, 'EN', 3),
	('The Dunwich Horror', (SELECT id FROM author WHERE name_EN = 'Howard Lovecraft'), 'Weird Tales', 2007, 'EN', 2),
	('At the Mountains of Madness', (SELECT id FROM author WHERE name_EN = 'Howard Lovecraft'), 'Weird Tales', 2007, 'EN', 4),
	('The Rats in the Walls', (SELECT id FROM author WHERE name_EN = 'Howard Lovecraft'), 'Weird Tales', 2007, 'EN', 1),
    ('На стрімчаках божевілля', (SELECT id FROM author WHERE name_UA = 'Говард Лавкрафт'), 'А-ба-ба-га-ла-ма-га', 2014, 'UA', 2);

/* filling user */
INSERT INTO user (login, email, `password`, phone_number, `role`, first_name, last_name)
VALUES
	('Victor-777','pobeditel@mail.ru', '$2a$10$aAFscXxylWf9c3t1eC0IKuC2SZvfZFbOKZhp9mtx6HfGQpQEDXxZ2', '973329911', 'reader', 'Victor', 'Kotyk'),
	('Misha','mychalos@gmail.com', '$2a$10$aAFscXxylWf9c3t1eC0IKuC2SZvfZFbOKZhp9mtx6HfGQpQEDXxZ2', '638753300', 'reader', 'Mychailo', 'Los'),
	('JustReader','reader@gmail.com', '$2a$10$aAFscXxylWf9c3t1eC0IKuC2SZvfZFbOKZhp9mtx6HfGQpQEDXxZ2', '638759876', 'reader', 'Ігор', 'Веселий'),
	('GreatLibrarian','gl12@gmail.com', '$2a$10$tGfT0rRYiTpD6RE.cezv4eF6Z7Fr5BoHbGXQQwr9PakKfvOEwV5fW', '639864322', 'librarian', 'Шарль', 'д\'Артаньян'),
	('BigBoss','big@boss.com', '$2a$10$jFf0CVGBYBEOmVeU/g4XeOcpzRBMj1Bk7w6wmF3SdEzvjSdwhJvUG', '974998820', 'administrator', 'Валерій', 'Залужний');
