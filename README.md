# Library project

Main task of the project is to implement a library web application that supports the functionality according to the 
description below.

## Functionality

***The reader registers in the system and then can:***
- search (by author / title);
- place an order for a Book from the Catalog. An unregistered reader cannot order a book.

***For the catalog the ability to sort books has to be implemented:***
- by name;
- by author;
- by publication;
- by date of publication.


The librarian gives the reader a book on a subscription or to the reading room. The book is issued to the reader for 
a certain period. If the book is not returned within the specified period, the reader will be fined. The book may be 
present in the library in one or more copies. The system keeps track of the available number of books. Each user has 
a personal account that displays registration information.

***Additionally, following functionality should be implemented:***
1) Readers can review:
   - list of books on the subscription and date of possible return (if the date is overdue, the amount of the fine is displayed);


2) Librarian can review:
   - list of readers' orders;
   - list of readers and their subscriptions.
   

3) The system administrator has the rights:
   - adding / deleting a book, editing information about the book;
   - create / delete librarian;
   - blocking / unblocking the user.


## Database structure

![database.jpg](database.png)

## How to install

For proper software installation please follow the instructions. Used DBMS is MySQL.

1) Launch scripts that are located in the `SQL sripts` folder in the following order:
   - creation library database.sql
   - library constraints.sql
   - filling tables.sql
2) After deploying the database, write the connection parameters in the `resources/db.properties file`
3) Launch