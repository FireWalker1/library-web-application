<%@ include file="/WEB-INF/jspf/resources.jspf"%>

<!DOCTYPE html>
<html>
<head>
    <title><fmt:message key="title.all_subscriptions"/></title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/bootstrap/css/bootstrap.min.css">
    <script src="${pageContext.request.contextPath}/bootstrap/js/bootstrap.bundle.min.js"></script>
</head>
<body style="background-image: url('images/subscriptions_banner.jpg'); background-position: top center; background-size: cover">

<%@ include file="/WEB-INF/jspf/menu.jspf" %>

<div class="py-3"></div>

<div class="px-4 py-5 text-center text-white">
    <h1 class="display-6 fw-bold"><fmt:message key="all_subscriptions.header"/></h1>
    <p class="text-start fst-italic fw-bold text-warning px-5 mt-5"><fmt:message key="my_subscriptions.info_text"/></p>

    <tags:success/>
    <tags:error/>

    <div class="col-12 mx-auto pt-4 pb-5 px-5">
        <table class="table table-secondary table-hover">
            <thead class="table-dark">
            <tr>
                <th scope="col">
                    <a href="controller?action=getAllSubscriptions&orderBy=id&desc=false&pageNumber=1"
                       style="text-decoration: none">
                        <img src="images/arrow-down.svg" width="16" height="16" alt="arrow-down">
                    </a>
                    <fmt:message key="my_subscriptions.id"/>
                    <a href="controller?action=getAllSubscriptions&orderBy=id&desc=true&pageNumber=1">
                        <img src="images/arrow-up.svg" width="16" height="16" alt="arrow-up">
                    </a>
                </th>
                <th scope="col">
                    <a href="controller?action=getAllSubscriptions&orderBy=userLogin&desc=false&pageNumber=1"
                       style="text-decoration: none">
                        <img src="images/arrow-down.svg" width="16" height="16" alt="arrow-down">
                    </a>
                    <fmt:message key="all_subscriptions.userLogin"/>
                    <a href="controller?action=getAllSubscriptions&orderBy=userLogin&desc=true&pageNumber=1">
                        <img src="images/arrow-up.svg" width="16" height="16" alt="arrow-up">
                    </a>
                </th>
                <th scope="col">
                    <a href="controller?action=getAllSubscriptions&orderBy=type&desc=false&pageNumber=1"
                       style="text-decoration: none">
                        <img src="images/arrow-down.svg" width="16" height="16" alt="arrow-down">
                    </a>
                    <fmt:message key="my_subscriptions.type"/>
                    <a href="controller?action=getAllSubscriptions&orderBy=type&desc=true&pageNumber=1">
                        <img src="images/arrow-up.svg" width="16" height="16" alt="arrow-up">
                    </a>
                </th>
                <th scope="col">
                    <a href="controller?action=getAllSubscriptions&orderBy=status&desc=false&pageNumber=1"
                       style="text-decoration: none">
                        <img src="images/arrow-down.svg" width="16" height="16" alt="arrow-down">
                    </a>
                    <fmt:message key="my_subscriptions.status"/>
                    <a href="controller?action=getAllSubscriptions&orderBy=status&desc=true&pageNumber=1">
                        <img src="images/arrow-up.svg" width="16" height="16" alt="arrow-up">
                    </a>
                </th>
                <th scope="col">
                    <a href="controller?action=getAllSubscriptions&orderBy=createdAt&desc=false&pageNumber=1"
                       style="text-decoration: none">
                        <img src="images/arrow-down.svg" width="16" height="16" alt="arrow-down">
                    </a>
                    <fmt:message key="my_subscriptions.created"/>
                    <a href="controller?action=getAllSubscriptions&orderBy=createdAt&desc=true&pageNumber=1">
                        <img src="images/arrow-up.svg" width="16" height="16" alt="arrow-up">
                    </a>
                </th>
                <th scope="col">
                    <a href="controller?action=getAllSubscriptions&orderBy=updatedAt&desc=false&pageNumber=1"
                       style="text-decoration: none">
                        <img src="images/arrow-down.svg" width="16" height="16" alt="arrow-down">
                    </a>
                    <fmt:message key="my_subscriptions.updated"/>
                    <a href="controller?action=getAllSubscriptions&orderBy=updatedAt&desc=true&pageNumber=1">
                        <img src="images/arrow-up.svg" width="16" height="16" alt="arrow-up">
                    </a>
                </th>
                <th scope="col">
                    <a href="controller?action=getAllSubscriptions&orderBy=bookReturnDeadline&desc=false&pageNumber=1"
                       style="text-decoration: none">
                        <img src="images/arrow-down.svg" width="16" height="16" alt="arrow-down">
                    </a>
                    <fmt:message key="my_subscriptions.return_due"/>
                    <a href="controller?action=getAllSubscriptions&orderBy=bookReturnDeadline&desc=true&pageNumber=1">
                        <img src="images/arrow-up.svg" width="16" height="16" alt="arrow-up">
                    </a>
                </th>
                <th scope="col">
                    <a href="controller?action=getAllSubscriptions&orderBy=fine&desc=false&pageNumber=1"
                       style="text-decoration: none">
                        <img src="images/arrow-down.svg" width="16" height="16" alt="arrow-down">
                    </a>
                    <fmt:message key="my_subscriptions.fine"/>
                    <a href="controller?action=getAllSubscriptions&orderBy=fine&desc=true&pageNumber=1">
                        <img src="images/arrow-up.svg" width="16" height="16" alt="arrow-up">
                    </a>
                </th>
                <th scope="col">
                    <fmt:message key="my_subscriptions.actions"/>
                </th>
            </tr>
            </thead>
            <tbody class="table-group-divider">
            <c:forEach var="subscription" items="${requestScope.subscriptions}">
                <tr>
                    <th scope="col"><c:out value="${subscription.id}"/></th>
                    <td><c:out value="${subscription.user.login}"/></td>
                    <td><fmt:message key="${subscription.type}"/></td>
                    <td><fmt:message key="${subscription.status}"/></td>
                    <td><fmt:formatDate pattern="HH:mm dd-MM-yyyy" value="${subscription.createdAt}"/></td>
                    <td><fmt:formatDate pattern="HH:mm dd-MM-yyyy" value="${subscription.updatedAt}"/></td>
                    <td><c:out value="${subscription.bookReturnDeadline}"/></td>
                    <td><c:out value="${subscription.fine}"/></td>
                    <td>
                        <a href="controller?action=getSubscriptionInfo&id=${subscription.id}">
                            <img src="images/info.svg" width="18" height="18" alt="arrow-up">
                        </a>
                    </td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </div>

    <div class="container">
        <c:if test="${param.pageNumber != 1 && param.pageNumber != null}">
            <button class="btn btn-dark"
                    onclick="window.location.href='controller?action=getAllSubscriptions&orderBy=${param.orderBy}&desc=${param.desc}&pageNumber=${param.pageNumber - 1}'">
                <fmt:message key="books.previous"/></button>
        </c:if>
        <c:if test="${requestScope.pageQuantity != param.pageNumber && param.pageNumber != null}">
            <button class="btn btn-dark"
                    onclick="window.location.href='controller?action=getAllSubscriptions&orderBy=${param.orderBy}&desc=${param.desc}&pageNumber=${param.pageNumber + 1}'">
                <fmt:message key="books.next"/></button>
        </c:if>
    </div>

    <div class="container py-5 mx-auto">
        <fmt:message key="my_subscriptions.current_date"/> <custom:currentDate/>
    </div>

</div>

<%@ include file="/WEB-INF/jspf/footer.jspf" %>

</body>
</html>