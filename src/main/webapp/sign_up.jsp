<%@ include file="/WEB-INF/jspf/resources.jspf"%>

<!DOCTYPE html>
<html>
<head>
    <title><fmt:message key="title.sign_up"/></title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/bootstrap/css/bootstrap.min.css">
    <script src="${pageContext.request.contextPath}/bootstrap/js/bootstrap.bundle.min.js"></script>
</head>
<body style="background-image: url('images/sign_up_banner.jpg'); background-position: center center; background-size: cover">

<%@ include file="/WEB-INF/jspf/menu.jspf" %>

<div class="container align-items-center text-white">

    <div class="py-5 mb-5 text-center">
        <img class="d-block mx-auto mb-4" src="images/logo_150.png" alt="logo">
        <h2><fmt:message key="sign_up.header"/></h2>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-3"></div>
            <div class="col-6">
                <span class="text-warning"><i><b><fmt:message key="sign_up.info_text"/></b></i></span>
                <form class="mt-5" action="controller" method="post">
                    <input type="hidden" name="action" value="signUp">
                    <div class="row g-3">

                        <div class="col-12">
                            <label for="login" class="form-label"><fmt:message key="sign_up.login"/></label>
                            <input type="text" class="form-control" id="login" name="login" required>
                            <c:if test="${param.loginError != null}">
                                <div class="text-danger mb-3 small fw-bold">
                                    <fmt:message key="${param.loginError}"/>
                                </div>
                            </c:if>
                        </div>

                        <div class="col-12">
                            <label for="password" class="form-label"><fmt:message key="sign_up.password"/></label>
                            <input type="password" class="form-control" id="password" required name="password">
                            <c:if test="${param.passwordError != null}">
                                <div class="text-danger mb-3 small fw-bold">
                                    <fmt:message key="${param.passwordError}"/>
                                </div>
                            </c:if>
                        </div>

                        <div class="col-12">
                            <label for="firstName" class="form-label"><fmt:message key="sign_up.name"/></label>
                            <input type="text" class="form-control" id="firstName" name="firstName" required>
                            <c:if test="${param.firstNameError != null}">
                                <div class="text-danger mb-3 small fw-bold">
                                    <fmt:message key="${param.firstNameError}"/>
                                </div>
                            </c:if>
                        </div>

                        <div class="col-12">
                            <label for="lastName" class="form-label"><fmt:message key="sign_up.surname"/></label>
                            <input type="text" class="form-control" id="lastName" name="lastName" required>
                            <c:if test="${param.lastNameError != null}">
                                <div class="text-danger mb-3 small fw-bold">
                                    <fmt:message key="${param.lastNameError}"/>
                                </div>
                            </c:if>
                        </div>

                        <div class="col-12">
                            <label for="email" class="form-label"><fmt:message key="sign_up.email"/></label>
                            <input type="email" class="form-control" id="email" placeholder="you@example.com"
                                   name="email" required>
                            <c:if test="${param.emailError != null}">
                                <div class="text-danger mb-3 small fw-bold">
                                    <fmt:message key="${param.emailError}"/>
                                </div>
                            </c:if>
                        </div>

                        <div class="col-12">
                            <label for="phoneNumber" class="form-label"><fmt:message key="sign_up.phone_number"/> <span class="text-muted">
                                <fmt:message key="sign_up.optional"/></span></label>
                            <div class="input-group col-12">
                                <span class="input-group-text">+380</span>
                                <input type="text" class="form-control" placeholder="631234567" id="phoneNumber"
                                name="phoneNumber">
                            </div>
                            <c:if test="${param.phoneNumberError != null}">
                                <div class="text-danger mb-3 small fw-bold">
                                    <fmt:message key="${param.phoneNumberError}"/>
                                </div>
                            </c:if>
                        </div>
                    </div>

                    <hr class="my-4">

                    <div class="row">
                        <div class="col-3"></div>
                        <button class="text-center align-self-center w-50 btn btn-warning btn-lg mb-4"
                                type="submit"><fmt:message key="sign_up.sign_up"/>
                        </button>
                        <div class="col-3"></div>
                    </div>
                </form>
            </div>
            <div class="col-3"></div>
        </div>
    </div>
</div>

<div class="my-5"></div>
<%@ include file="/WEB-INF/jspf/footer.jspf" %>

</body>
</html>