<%@ include file="/WEB-INF/jspf/resources.jspf"%>

<!DOCTYPE html>
<html>
<head>
  <title><fmt:message key="title.add_new_book"/></title>
  <link rel="stylesheet" href="${pageContext.request.contextPath}/bootstrap/css/bootstrap.min.css">
  <script src="${pageContext.request.contextPath}/bootstrap/js/bootstrap.bundle.min.js"></script>
</head>
<body style="background-image: url('images/add_book_banner.jpg'); background-position: center center; background-size: cover">

<%@ include file="/WEB-INF/jspf/menu.jspf" %>

<div class="container align-items-center text-white">

  <div class="py-5 mb-5 text-center">
    <img class="d-block mx-auto mb-4" src="images/logo_150.png" alt="logo">
    <h2><fmt:message key="add_new_book.header"/></h2>
  </div>

  <div class="container">
    <div class="row">
      <div class="col-3"></div>
      <div class="col-6">
        <span class="text-warning"><i><b><fmt:message key="add_new_book.info_text"/></b></i></span>

        <c:if test="${param.duplicateError != null}">
          <div class="text-danger my-2">
            <fmt:message key="${param.duplicateError}"/>
          </div>
        </c:if>

        <form class="mt-5" action="controller" method="post">
          <input type="hidden" name="action" value="addNewBook">
          <div class="row g-3">

            <div class="col-12">
              <label for="title" class="form-label"><fmt:message key="add_new_book.title"/></label>
              <input type="text" class="form-control" id="title" name="title" required>
              <c:if test="${param.titleError != null}">
                <div class="text-danger mb-3 small fw-bold">
                  <fmt:message key="${param.titleError}"/>
                </div>
              </c:if>
            </div>

            <div class="col-12">
              <label for="authorNameUA" class="form-label"><fmt:message key="add_new_book.author_name_UA"/></label>
              <input type="text" class="form-control" id="authorNameUA" required name="authorNameUA">
              <c:if test="${param.authorNameUAError != null}">
                <div class="text-danger mb-3 small fw-bold">
                  <fmt:message key="${param.authorNameUAError}"/>
                </div>
              </c:if>
            </div>

            <div class="col-12">
              <label for="authorNameEN" class="form-label"><fmt:message key="add_new_book.author_name_EN"/></label>
              <input type="text" class="form-control" id="authorNameEN" name="authorNameEN" required>
              <c:if test="${param.authorNameENError != null}">
                <div class="text-danger mb-3 small fw-bold">
                  <fmt:message key="${param.authorNameENError}"/>
                </div>
              </c:if>
            </div>

            <div class="col-12">
              <label for="publisherHouse" class="form-label"><fmt:message key="add_new_book.publisher_house"/></label>
              <input type="text" class="form-control" id="publisherHouse" name="publisherHouse" required>
              <c:if test="${param.publisherHouseError != null}">
                <div class="text-danger mb-3 small fw-bold">
                  <fmt:message key="${param.publisherHouseError}"/>
                </div>
              </c:if>
            </div>

            <div class="col-12">
              <label for="year" class="form-label"><fmt:message key="add_new_book.year"/></label>
              <input type="text" class="form-control" id="year" name="year" required>
              <c:if test="${param.yearError != null}">
                <div class="text-danger mb-3 small fw-bold">
                  <fmt:message key="${param.yearError}"/>
                </div>
              </c:if>
            </div>

            <div class="col-12">
              <fmt:message key="add_new_book.language"/>
            </div>

            <div class="col-6 form-check mx-auto px-5">
              <input class="form-check-input" type="radio" name="language" id="radio1" value="UA" checked>
              <label class="form-check-label" for="radio1">
                <fmt:message key="menu.ukrainian"/>
              </label>
            </div>
            <div class="col-6 form-check mx-auto px-5">
              <input class="form-check-input" type="radio" name="language" id="radio2" value="EN">
              <label class="form-check-label" for="radio2">
                <fmt:message key="menu.english"/>
              </label>
            </div>

            <div class="col-12">
              <label for="quantity" class="form-label"><fmt:message key="add_new_book.quantity"/></label>
              <input type="text" class="form-control" id="quantity" name="quantity" required>
              <c:if test="${param.quantityError != null}">
                <div class="text-danger mb-3 small fw-bold">
                  <fmt:message key="${param.quantityError}"/>
                </div>
              </c:if>
            </div>

          </div>

          <hr class="my-4">

          <div class="row">
            <div class="col-3"></div>
            <button class="text-center align-self-center w-50 btn btn-warning btn-lg mb-4"
                    type="submit"><fmt:message key="add_new_book.confirm"/>
            </button>
            <div class="col-3"></div>
          </div>
        </form>
      </div>
      <div class="col-3"></div>
    </div>
  </div>
</div>

<div class="my-5"></div>
<%@ include file="/WEB-INF/jspf/footer.jspf" %>

</body>
</html>