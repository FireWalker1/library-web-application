<%@ include file="/WEB-INF/jspf/resources.jspf" %>

<!DOCTYPE html>
<html>
<head>
    <title><fmt:message key="title.contacts"/></title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/bootstrap/css/bootstrap.min.css">
    <script src="${pageContext.request.contextPath}/bootstrap/js/bootstrap.bundle.min.js"></script>
</head>
<body style="background-image: url('images/contact_banner.jpg'); background-position: top right">

<%@ include file="/WEB-INF/jspf/menu.jspf" %>

<div class="py-5"></div>
<div class="px-4 py-5 text-center text-white">
    <h1 class="display-6 fw-bold"><fmt:message key="contacts.how_to_find"/></h1>
    <div class="col-lg-6 mx-auto">
        <p class="lead mb-4"><fmt:message key="contacts.info_text"/></p>
        <div class="d-grid gap-2 d-sm-flex justify-content-sm-center my-5 py-3 text-secondary">
            <address><fmt:message key="contacts.library"/><br>
                <fmt:message key="contacts.address_1"/><br>
                <fmt:message key="contacts.address_2"/><br>
                <hr>
                forbidden@knowledge.com.ua
            </address>
        </div>
    </div>
</div>

<%@ include file="/WEB-INF/jspf/footer.jspf" %>

</body>
</html>