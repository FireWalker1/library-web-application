<%@ include file="/WEB-INF/jspf/resources.jspf"%>

<!DOCTYPE html>
<html>
<head>
    <c:if test="${sessionScope.role == 'role.reader'}">
        <title><fmt:message key="title.my_subscriptions"/></title>
    </c:if>
    <c:if test="${sessionScope.role == 'role.librarian'}">
        <title><fmt:message key="title.my_subscriptions_librarian"/> ${requestScope.login}</title>
    </c:if>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/bootstrap/css/bootstrap.min.css">
    <script src="${pageContext.request.contextPath}/bootstrap/js/bootstrap.bundle.min.js"></script>
</head>
<body style="background-image: url('images/subscriptions_banner.jpg'); background-position: top center; background-size: cover">

<%@ include file="/WEB-INF/jspf/menu.jspf" %>

<div class="py-3"></div>

<div class="px-4 py-5 text-center text-white">
    <c:if test="${sessionScope.role == 'role.reader'}">
        <h1 class="display-6 fw-bold"><fmt:message key="my_subscriptions.header"/></h1>
    </c:if>
    <c:if test="${sessionScope.role == 'role.librarian'}">
        <h1 class="display-6 fw-bold"><fmt:message key="my_subscriptions.header_librarian"/> ${requestScope.login}</h1>
    </c:if>

    <p class="text-start fst-italic fw-bold text-warning px-5 mt-5"><fmt:message key="my_subscriptions.info_text"/></p>

    <tags:success/>
    <tags:error/>

    <div class="col-12 mx-auto pt-4 pb-5 px-5">
        <table class="table table-secondary table-hover">
            <thead class="table-dark">
            <tr>
                <th scope="col">
                    <fmt:message key="my_subscriptions.id"/>
                </th>
                <th scope="col">
                    <fmt:message key="my_subscriptions.type"/>
                </th>
                <th scope="col">
                    <fmt:message key="my_subscriptions.status"/>
                </th>
                <th scope="col">
                    <fmt:message key="my_subscriptions.created"/>
                </th>
                <th scope="col">
                    <fmt:message key="my_subscriptions.updated"/>
                </th>
                <th scope="col">
                    <fmt:message key="my_subscriptions.return_due"/>
                </th>
                <th scope="col">
                    <fmt:message key="my_subscriptions.fine"/>
                </th>
                <th scope="col">
                    <fmt:message key="my_subscriptions.actions"/>
                </th>
            </tr>
            </thead>
            <tbody class="table-group-divider">
            <c:forEach var="subscription" items="${requestScope.subscriptions}">
                <tr>
                    <th scope="col"><c:out value="${subscription.id}"/></th>
                    <td><fmt:message key="${subscription.type}"/></td>
                    <td><fmt:message key="${subscription.status}"/></td>
                    <td><fmt:formatDate pattern="HH:mm dd-MM-yyyy" value="${subscription.createdAt}"/></td>
                    <td><fmt:formatDate pattern="HH:mm dd-MM-yyyy" value="${subscription.updatedAt}"/></td>
                    <td><c:out value="${subscription.bookReturnDeadline}"/></td>
                    <td><c:out value="${subscription.fine}"/></td>
                    <td>
                        <a href="controller?action=getSubscriptionInfo&id=${subscription.id}">
                            <img src="images/info.svg" width="18" height="18" alt="arrow-up">
                        </a>
                    </td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </div>

    <c:if test="${sessionScope.role == 'role.librarian'}">
        <div class="container mx-auto">
            <button type="button" class="btn btn-outline-light btn-w px-4 my-3 mx-auto"
                    onclick="window.location.href='controller?action=getAllReaders'"><fmt:message
                    key="subscription_info.return"/>
            </button>
        </div>
    </c:if>

    <div class="container py-5 mx-auto">
        <fmt:message key="my_subscriptions.current_date"/> <custom:currentDate/>
    </div>

</div>

<%--<div class="py-5"></div>--%>

<%@ include file="/WEB-INF/jspf/footer.jspf" %>

</body>
</html>