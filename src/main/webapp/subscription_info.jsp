<%@ include file="/WEB-INF/jspf/resources.jspf" %>

<!DOCTYPE html>
<html>
<head>
    <title><fmt:message key="title.subscription_info"/></title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/bootstrap/css/bootstrap.min.css">
    <script src="${pageContext.request.contextPath}/bootstrap/js/bootstrap.bundle.min.js"></script>
</head>
<body style="background-image: url('images/subscription_info_banner.jpg'); background-position: top center; background-size: cover">

<%@ include file="/WEB-INF/jspf/menu.jspf" %>

<div class="py-3"></div>

<div class="px-4 py-5 text-center text-white">
    <h1 class="display-6 fw-bold mb-5">
        <fmt:message key="subscription_info.header"/>
        <a href="controller?action=printPdfSubscriptionInfo&id=${requestScope.subscription.id}">
            <img src="images/download.svg" width="22" height="22" alt="arrow-up">
        </a>
    </h1>

    <c:if test="${sessionScope.role == 'role.librarian'}">
        <p class="text-start fst-italic fw-bold text-warning px-5 mt-5 py-3"><fmt:message
                key="subscription_info.subscription_text"/></p>
    </c:if>

    <tags:success/>
    <tags:error/>

    <div class="container mx-auto text-center col-12">
        <ol class="list-group list-group-horizontal mx-auto text-center col-12">
            <li class="list-group-item flex-fill justify-content-between align-items-start">
                <div class="ms-2 me-auto">
                    <div class="fw-bold"><fmt:message key="my_subscriptions.id"/></div>
                    ${requestScope.subscription.id}
                </div>
            </li>
            <c:if test="${sessionScope.role == 'role.librarian'}">
                <li class="list-group-item flex-fill justify-content-between align-items-start">
                    <div class="ms-2 me-auto">
                        <div class="fw-bold"><fmt:message key="all_subscriptions.userLogin"/></div>
                            ${requestScope.subscription.user.login}
                    </div>
                </li>
            </c:if>
            <li class="list-group-item flex-fill justify-content-between align-items-start">
                <div class="ms-2 me-auto">
                    <div class="fw-bold"><fmt:message key="my_subscriptions.type"/></div>
                    <fmt:message key="${requestScope.subscription.type}"/>
                </div>
            </li>
            <li class="list-group-item flex-fill justify-content-between align-items-start">
                <div class="ms-2 me-auto">
                    <div class="fw-bold"><fmt:message key="my_subscriptions.status"/></div>
                    <fmt:message key="${requestScope.subscription.status}"/>
                </div>
            </li>
            <li class="list-group-item flex-fill justify-content-between align-items-start">
                <div class="ms-2 me-auto">
                    <div class="fw-bold"><fmt:message key="my_subscriptions.created"/></div>
                    <fmt:formatDate pattern="HH:mm dd-MM-yyyy" value="${requestScope.subscription.createdAt}"/>
                </div>
            </li>
            <li class="list-group-item flex-fill justify-content-between align-items-start">
                <div class="ms-2 me-auto">
                    <div class="fw-bold"><fmt:message key="my_subscriptions.updated"/></div>
                    <fmt:formatDate pattern="HH:mm dd-MM-yyyy" value="${requestScope.subscription.updatedAt}"/>
                </div>
            </li>
            <li class="list-group-item flex-fill justify-content-between align-items-start">
                <div class="ms-2 me-auto">
                    <div class="fw-bold"><fmt:message key="my_subscriptions.return_due"/></div>
                    ${requestScope.subscription.bookReturnDeadline}
                </div>
            </li>
            <li class="list-group-item flex-fill justify-content-between align-items-start">
                <div class="ms-2 me-auto">
                    <div class="fw-bold"><fmt:message key="my_subscriptions.fine"/></div>
                    ${requestScope.subscription.fine}
                </div>
            </li>
        </ol>
    </div>

    <p class="text-start fst-italic fw-bold text-warning px-5 mt-5"><fmt:message
            key="subscription_info.books_text"/></p>

    <div class="col-12 mx-auto pt-4 pb-5 px-5">
        <table class="table table-secondary table-hover">
            <thead class="table-dark">
            <tr>
                <th scope="col">
                    <fmt:message key="books.book_id"/>
                </th>
                <th scope="col">
                    <fmt:message key="books.title"/>
                </th>
                <th scope="col">
                    <fmt:message key="books.author"/>
                </th>
                <th scope="col">
                    <fmt:message key="books.publisher"/>
                </th>
                <th scope="col">
                    <fmt:message key="books.year"/>
                </th>
                <th scope="col">
                    <fmt:message key="books.language"/>
                </th>
            </tr>
            </thead>
            <tbody class="table-group-divider">
            <c:forEach var="book" items="${requestScope.subscription.books}">
                <tr>
                    <th scope="col"><c:out value="${book.id}"/></th>
                    <td><c:out value="${book.title}"/></td>
                    <c:if test="${sessionScope.language == '' || sessionScope.language == null}">
                        <td><c:out value="${book.author.nameUA}"/></td>
                    </c:if>
                    <c:if test="${sessionScope.language == 'en'}">
                        <td><c:out value="${book.author.nameEN}"/></td>
                    </c:if>
                    <td><c:out value="${book.publisherHouse}"/></td>
                    <td><c:out value="${book.year}"/></td>
                    <td><c:out value="${book.language}"/></td>
                </tr>
            </c:forEach>
            </tbody>
        </table>

        <c:if test="${sessionScope.role == 'role.reader'}">
            <div class="container btn-group gap-2 text-center justify-content-center py-4 mx-auto col-4">
                <c:if test="${requestScope.subscription.status == 'status.closed' && requestScope.subscription.fine > 0}">
                    <button type="button" class="btn btn-primary btn-warning btn-lg px-4 col-2"
                            onclick="window.location.href='controller?action=payFine&id=${requestScope.subscription.id}'">
                        <fmt:message key="subscription_info.pay_fine"/>
                    </button>
                </c:if>
            </div>
            <div class="container mx-auto">
                <button type="button" class="btn btn-outline-light btn-w px-4 my-3 mx-auto"
                        onclick="window.location.href='controller?action=getMySubscriptions'"><fmt:message
                        key="subscription_info.return"/>
                </button>
            </div>
        </c:if>
        <c:if test="${sessionScope.role == 'role.librarian'}">
            <div class="container btn-group gap-2 text-center justify-content-center py-4 mx-auto col-4">
                <c:if test="${requestScope.subscription.status != 'status.closed'}">
                    <button type="button" class="btn btn-primary btn-danger btn-lg px-4 col-2"
                            onclick="window.location.href='controller?action=rejectSubscription&id=${requestScope.subscription.id}'">
                        <fmt:message key="subscription_info.reject"/>
                    </button>
                </c:if>
                <c:if test="${requestScope.subscription.status == 'status.requested' || requestScope.subscription.status == 'status.ongoing'}">
                    <button type="button" class="btn btn-primary btn-success btn-lg px-4 col-2"
                            onclick="window.location.href='controller?action=confirmSubscription&id=${requestScope.subscription.id}'">
                        <fmt:message key="subscription_info.confirm"/>
                    </button>
                </c:if>
                <c:if test="${requestScope.subscription.status == 'status.confirmed' && requestScope.subscription.type == 'type.home'}">
                    <button type="button" class="btn btn-primary btn-success btn-lg px-4 col-2" data-bs-toggle="modal"
                            data-bs-target="#modalDate">
                        <fmt:message key="subscription_info.confirm"/>
                    </button>
                </c:if>
                <c:if test="${requestScope.subscription.status == 'status.confirmed' && requestScope.subscription.type == 'type.reading_room'}">
                    <button type="button" class="btn btn-primary btn-success btn-lg px-4 col-2"
                            onclick="window.location.href='controller?action=confirmSubscription&id=${requestScope.subscription.id}&date=1'">
                        <fmt:message key="subscription_info.confirm"/>
                    </button>
                </c:if>
            </div>
            <div class="container mx-auto">
                <button type="button" class="btn btn-outline-light btn-w px-4 my-3 mx-auto"
                        onclick="window.location.href='controller?action=getAllSubscriptions'"><fmt:message
                        key="subscription_info.return"/>
                </button>
            </div>
        </c:if>

    </div>
</div>

<%@ include file="/WEB-INF/jspf/footer.jspf" %>

<%--modal window for choosing date--%>

<div class="modal py-5" tabindex="-1" role="dialog" id="modalDate">
    <div class="modal-dialog" role="document">
        <div class="modal-content rounded-4 shadow bg-light">
            <div class="modal-header p-5 pb-4 border-bottom-0">
                <h1 class="fw-bold mb-0 fs-2"><fmt:message key="subscription_info.deadline"/></h1>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>

            <div class="modal-body p-5 pt-0">
                <form class="" data-np-autofill-type="register" data-np-checked="1" data-np-watching="1"
                      action="controller" method="post" id="dateForm">
                    <div class="form-floating mb-3">
                        <input type="hidden" name="action" value="confirmSubscription">
                        <input type="hidden" name="id" value="${requestScope.subscription.id}">
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="date" id="radio1" value="1" checked>
                            <label class="form-check-label" for="radio1">
                                <fmt:message key="subscription_info.return_today"/>
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="date" id="radio2" value="2">
                            <label class="form-check-label" for="radio2">
                                <fmt:message key="subscription_info.return_3_days"/>
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="date" id="radio3" value="3">
                            <label class="form-check-label" for="radio3">
                                <fmt:message key="subscription_info.return_week"/>
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="date" id="radio4" value="4">
                            <label class="form-check-label" for="radio4">
                                <fmt:message key="subscription_info.return_2_weeks"/>
                            </label>
                        </div>
                    </div>
                    <button class="w-100 mb-2 btn btn-lg rounded-3 btn-warning" type="submit"><fmt:message
                            key="subscription_info.confirm"/></button>
                </form>
            </div>
        </div>
    </div>
</div>

</body>
</html>