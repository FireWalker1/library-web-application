<%@ include file="/WEB-INF/jspf/resources.jspf" %>

<!DOCTYPE html>
<html>
<head>
    <title><fmt:message key="title.cart"/></title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/bootstrap/css/bootstrap.min.css">
    <script src="${pageContext.request.contextPath}/bootstrap/js/bootstrap.bundle.min.js"></script>
</head>
<body style="background-image: url('images/cart_banner.jpg'); background-position: top center; background-size: cover">

<%@ include file="/WEB-INF/jspf/menu.jspf" %>

<div class="py-3"></div>

<div class="px-4 py-5 text-center text-white">
    <h1 class="display-6 fw-bold"><fmt:message key="cart.cart_with_books"/></h1>
    <p class="text-start fst-italic fw-bold text-warning px-5 mt-5"><fmt:message key="cart.check_text"/></p>

    <tags:success/>
    <tags:error/>

    <div class="col-12 mx-auto pt-4 pb-5 px-5">
        <table class="table table-secondary table-hover">
            <thead class="table-dark">
            <tr>
                <th scope="col">
                    <fmt:message key="books.book_id"/>
                </th>
                <th scope="col">
                    <fmt:message key="books.title"/>
                </th>
                <th scope="col">
                    <fmt:message key="books.author"/>
                </th>
                <th scope="col">
                    <fmt:message key="books.publisher"/>
                </th>
                <th scope="col">
                    <fmt:message key="books.year"/>
                </th>
                <th scope="col">
                    <fmt:message key="books.language"/>
                </th>
                <th scope="col">
                    <fmt:message key="books.actions"/>
                </th>
            </tr>
            </thead>
            <tbody class="table-group-divider">
            <c:forEach var="book" items="${sessionScope.bookCart}">
                <tr>
                    <th scope="col"><c:out value="${book.id}"/></th>
                    <td><c:out value="${book.title}"/></td>
                    <c:if test="${sessionScope.language == '' || sessionScope.language == null}">
                        <td><c:out value="${book.author.nameUA}"/></td>
                    </c:if>
                    <c:if test="${sessionScope.language == 'en'}">
                        <td><c:out value="${book.author.nameEN}"/></td>
                    </c:if>
                    <td><c:out value="${book.publisherHouse}"/></td>
                    <td><c:out value="${book.year}"/></td>
                    <td><c:out value="${book.language}"/></td>
                    <td>
                        <a href="controller?action=deleteBookFromCart&id=${book.id}">
                            <img src="images/delete.svg" width="21" height="21" alt="arrow-up">
                        </a>
                    </td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-5"></div>
            <div class="col-2 text-center">
                <form class="" action="controller" method="post">
                    <input type="hidden" name="action" value="orderBooks">
                    <div class="form-check text-center">
                        <input class="form-check-input" type="radio" name="type" id="radio1" value="type.home" checked>
                        <label class="form-check-label" for="radio1">
                            <fmt:message key="cart.home_read"/>
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="type" id="radio2" value="type.reading_room">
                        <label class="form-check-label" for="radio2">
                            <fmt:message key="cart.reading_room_read"/>
                        </label>
                    </div>
                    <button type="submit" class="btn btn-lg btn-warning mb-3 mt-5 px-5"><fmt:message
                            key="cart.order"/></button>
                </form>
            </div>
            <div class="col-5"></div>
        </div>
    </div>

</div>

<%@ include file="/WEB-INF/jspf/footer.jspf" %>

</body>
</html>