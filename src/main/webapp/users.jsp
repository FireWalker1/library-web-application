<%@ include file="/WEB-INF/jspf/resources.jspf" %>

<!DOCTYPE html>
<html>
<head>
    <title><fmt:message key="title.users"/></title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/bootstrap/css/bootstrap.min.css">
    <script src="${pageContext.request.contextPath}/bootstrap/js/bootstrap.bundle.min.js"></script>
</head>
<body style="background-image: url('images/users_banner.jpg'); background-position: center center">

<%@ include file="/WEB-INF/jspf/menu.jspf" %>

<div class="py-3"></div>

<div class="px-4 py-5 text-center text-white">
    <c:if test="${sessionScope.role == 'role.librarian'}">
        <h1 class="display-6 fw-bold"><fmt:message key="users.readers"/></h1>
        <p class="text-start fst-italic fw-bold text-warning px-5 mt-5"><fmt:message key="users.info_text"/></p>
    </c:if>
    <c:if test="${sessionScope.role == 'role.administrator'}">
        <h1 class="display-6 fw-bold"><fmt:message key="users.users"/></h1>
        <p class="text-start fst-italic fw-bold text-warning px-5 mt-5"><fmt:message key="users.info_text_admin"/></p>
    </c:if>

    <tags:success/>
    <tags:error/>

    <div class="col-12 mx-auto pt-4 pb-5 px-5">
        <table class="table table-secondary table-hover">
            <thead class="table-dark">
            <c:if test="${sessionScope.role == 'role.librarian'}">
                <tr>
                    <th scope="col">
                        <a href="controller?action=getAllReaders&orderBy=id&desc=false&pageNumber=1"
                           style="text-decoration: none">
                            <img src="images/arrow-down.svg" width="16" height="16" alt="arrow-down">
                        </a>
                        <fmt:message key="users.id"/>
                        <a href="controller?action=getAllReaders&orderBy=id&desc=true&pageNumber=1">
                            <img src="images/arrow-up.svg" width="16" height="16" alt="arrow-up">
                        </a>
                    </th>
                    <th scope="col">
                        <a href="controller?action=getAllReaders&orderBy=login&desc=false&pageNumber=1"
                           style="text-decoration: none">
                            <img src="images/arrow-down.svg" width="16" height="16" alt="arrow-down">
                        </a>
                        <fmt:message key="users.login"/>
                        <a href="controller?action=getAllReaders&orderBy=login&desc=true&pageNumber=1">
                            <img src="images/arrow-up.svg" width="16" height="16" alt="arrow-up">
                        </a>
                    </th>
                    <th scope="col">
                        <a href="controller?action=getAllReaders&orderBy=email&desc=false&pageNumber=1"
                           style="text-decoration: none">
                            <img src="images/arrow-down.svg" width="16" height="16" alt="arrow-down">
                        </a>
                        <fmt:message key="users.email"/>
                        <a href="controller?action=getAllReaders&orderBy=email&desc=true&pageNumber=1">
                            <img src="images/arrow-up.svg" width="16" height="16" alt="arrow-up">
                        </a>
                    </th>
                    <th scope="col">
                        <a href="controller?action=getAllReaders&orderBy=phoneNumber&desc=false&pageNumber=1"
                           style="text-decoration: none">
                            <img src="images/arrow-down.svg" width="16" height="16" alt="arrow-down">
                        </a>
                        <fmt:message key="users.phone_number"/>
                        <a href="controller?action=getAllReaders&orderBy=phoneNumber&desc=true&pageNumber=1">
                            <img src="images/arrow-up.svg" width="16" height="16" alt="arrow-up">
                        </a>
                    </th>
                    <th scope="col">
                        <a href="controller?action=getAllReaders&orderBy=role&desc=false&pageNumber=1"
                           style="text-decoration: none">
                            <img src="images/arrow-down.svg" width="16" height="16" alt="arrow-down">
                        </a>
                        <fmt:message key="users.role"/>
                        <a href="controller?action=getAllReaders&orderBy=role&desc=true&pageNumber=1">
                            <img src="images/arrow-up.svg" width="16" height="16" alt="arrow-up">
                        </a>
                    </th>
                    <th scope="col">
                        <a href="controller?action=getAllReaders&orderBy=firstName&desc=false&pageNumber=1"
                           style="text-decoration: none">
                            <img src="images/arrow-down.svg" width="16" height="16" alt="arrow-down">
                        </a>
                        <fmt:message key="users.first_name"/>
                        <a href="controller?action=getAllReaders&orderBy=firstName&desc=true&pageNumber=1">
                            <img src="images/arrow-up.svg" width="16" height="16" alt="arrow-up">
                        </a>
                    </th>
                    <th scope="col">
                        <a href="controller?action=getAllReaders&orderBy=lastName&desc=false&pageNumber=1"
                           style="text-decoration: none">
                            <img src="images/arrow-down.svg" width="16" height="16" alt="arrow-down">
                        </a>
                        <fmt:message key="users.last_name"/>
                        <a href="controller?action=getAllReaders&orderBy=lastName&desc=true&pageNumber=1">
                            <img src="images/arrow-up.svg" width="16" height="16" alt="arrow-up">
                        </a>
                    </th>
                    <th scope="col">
                        <a href="controller?action=getAllReaders&orderBy=blockedAt&desc=false&pageNumber=1"
                           style="text-decoration: none">
                            <img src="images/arrow-down.svg" width="16" height="16" alt="arrow-down">
                        </a>
                        <fmt:message key="users.blocked_at"/>
                        <a href="controller?action=getAllReaders&orderBy=blockedAt&desc=true&pageNumber=1">
                            <img src="images/arrow-up.svg" width="16" height="16" alt="arrow-up">
                        </a>
                    </th>
                    <th scope="col">
                        <fmt:message key="my_subscriptions.actions"/>
                    </th>
                </tr>
            </c:if>
            <c:if test="${sessionScope.role == 'role.administrator'}">
                <tr>
                    <th scope="col">
                        <a href="controller?action=getAllUsers&orderBy=id&desc=false&pageNumber=1"
                           style="text-decoration: none">
                            <img src="images/arrow-down.svg" width="16" height="16" alt="arrow-down">
                        </a>
                        <fmt:message key="users.id"/>
                        <a href="controller?action=getAllUsers&orderBy=id&desc=true&pageNumber=1">
                            <img src="images/arrow-up.svg" width="16" height="16" alt="arrow-up">
                        </a>
                    </th>
                    <th scope="col">
                        <a href="controller?action=getAllUsers&orderBy=login&desc=false&pageNumber=1"
                           style="text-decoration: none">
                            <img src="images/arrow-down.svg" width="16" height="16" alt="arrow-down">
                        </a>
                        <fmt:message key="users.login"/>
                        <a href="controller?action=getAllUsers&orderBy=login&desc=true&pageNumber=1">
                            <img src="images/arrow-up.svg" width="16" height="16" alt="arrow-up">
                        </a>
                    </th>
                    <th scope="col">
                        <a href="controller?action=getAllUsers&orderBy=email&desc=false&pageNumber=1"
                           style="text-decoration: none">
                            <img src="images/arrow-down.svg" width="16" height="16" alt="arrow-down">
                        </a>
                        <fmt:message key="users.email"/>
                        <a href="controller?action=getAllUsers&orderBy=email&desc=true&pageNumber=1">
                            <img src="images/arrow-up.svg" width="16" height="16" alt="arrow-up">
                        </a>
                    </th>
                    <th scope="col">
                        <a href="controller?action=getAllUsers&orderBy=phoneNumber&desc=false&pageNumber=1"
                           style="text-decoration: none">
                            <img src="images/arrow-down.svg" width="16" height="16" alt="arrow-down">
                        </a>
                        <fmt:message key="users.phone_number"/>
                        <a href="controller?action=getAllUsers&orderBy=phoneNumber&desc=true&pageNumber=1">
                            <img src="images/arrow-up.svg" width="16" height="16" alt="arrow-up">
                        </a>
                    </th>
                    <th scope="col">
                        <a href="controller?action=getAllUsers&orderBy=role&desc=false&pageNumber=1"
                           style="text-decoration: none">
                            <img src="images/arrow-down.svg" width="16" height="16" alt="arrow-down">
                        </a>
                        <fmt:message key="users.role"/>
                        <a href="controller?action=getAllUsers&orderBy=role&desc=true&pageNumber=1">
                            <img src="images/arrow-up.svg" width="16" height="16" alt="arrow-up">
                        </a>
                    </th>
                    <th scope="col">
                        <a href="controller?action=getAllUsers&orderBy=firstName&desc=false&pageNumber=1"
                           style="text-decoration: none">
                            <img src="images/arrow-down.svg" width="16" height="16" alt="arrow-down">
                        </a>
                        <fmt:message key="users.first_name"/>
                        <a href="controller?action=getAllUsers&orderBy=firstName&desc=true&pageNumber=1">
                            <img src="images/arrow-up.svg" width="16" height="16" alt="arrow-up">
                        </a>
                    </th>
                    <th scope="col">
                        <a href="controller?action=getAllUsers&orderBy=lastName&desc=false&pageNumber=1"
                           style="text-decoration: none">
                            <img src="images/arrow-down.svg" width="16" height="16" alt="arrow-down">
                        </a>
                        <fmt:message key="users.last_name"/>
                        <a href="controller?action=getAllUsers&orderBy=lastName&desc=true&pageNumber=1">
                            <img src="images/arrow-up.svg" width="16" height="16" alt="arrow-up">
                        </a>
                    </th>
                    <th scope="col">
                        <a href="controller?action=getAllUsers&orderBy=blockedAt&desc=false&pageNumber=1"
                           style="text-decoration: none">
                            <img src="images/arrow-down.svg" width="16" height="16" alt="arrow-down">
                        </a>
                        <fmt:message key="users.blocked_at"/>
                        <a href="controller?action=getAllUsers&orderBy=blockedAt&desc=true&pageNumber=1">
                            <img src="images/arrow-up.svg" width="16" height="16" alt="arrow-up">
                        </a>
                    </th>
                    <th scope="col">
                        <fmt:message key="my_subscriptions.actions"/>
                    </th>
                </tr>
            </c:if>
            </thead>
            <tbody class="table-group-divider">
            <c:forEach var="user" items="${requestScope.users}">
                <tr>
                    <th scope="col"><c:out value="${user.id}"/></th>
                    <td><c:out value="${user.login}"/></td>
                    <td><c:out value="${user.email}"/></td>
                    <td>+380 <c:out value="${user.phoneNumber}"/></td>
                    <td><fmt:message key="${user.role}"/></td>
                    <td><c:out value="${user.firstName}"/></td>
                    <td><c:out value="${user.lastName}"/></td>
                    <td><fmt:formatDate pattern="HH:mm dd.MM.yy" value="${user.blockedAt}"/></td>
                    <td>
                        <c:if test="${sessionScope.role == 'role.librarian'}">
                            <a href="controller?action=getReaderSubscriptions&login=${user.login}">
                                <img src="images/info.svg" width="18" height="18" alt="arrow-up">
                            </a>
                        </c:if>
                        <c:if test="${sessionScope.role == 'role.administrator' && user.role != 'role.administrator'}">
                            <a href="controller?action=changeUserBlockStatus&login=${user.login}" style="text-decoration: none">
                                <img src="images/block.svg" width="18" height="18" alt="arrow-up">
                            </a>
                        </c:if>
                        <c:if test="${sessionScope.role == 'role.administrator' && user.role == 'role.librarian'}">
                            <a href="controller?action=deleteLibrarian&login=${user.login} "
                               style="text-decoration: none">
                                <img src="images/delete.svg" width="21" height="21" alt="arrow-up">
                            </a>
                        </c:if>
                    </td>

                </tr>
            </c:forEach>
            </tbody>
        </table>
    </div>

    <div class="container">
        <c:if test="${param.pageNumber != 1 && param.pageNumber != null}">
            <button class="btn btn-dark"
                    onclick="window.location.href='controller?action=getAllReaders&orderBy=${param.orderBy}&desc=${param.desc}&pageNumber=${param.pageNumber - 1}'">
                <fmt:message key="books.previous"/></button>
        </c:if>
        <c:if test="${requestScope.pageQuantity != param.pageNumber && param.pageNumber != null}">
            <button class="btn btn-dark"
                    onclick="window.location.href='controller?action=getAllReaders&orderBy=${param.orderBy}&desc=${param.desc}&pageNumber=${param.pageNumber + 1}'">
                <fmt:message key="books.next"/></button>
        </c:if>
    </div>

</div>

<%@ include file="/WEB-INF/jspf/footer.jspf" %>

</body>
</html>