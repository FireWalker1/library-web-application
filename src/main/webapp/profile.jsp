<%@ include file="/WEB-INF/jspf/resources.jspf" %>

<!DOCTYPE html>
<html>
<head>
    <title><fmt:message key="title.profile"/></title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/bootstrap/css/bootstrap.min.css">
    <script src="${pageContext.request.contextPath}/bootstrap/js/bootstrap.bundle.min.js"></script>
</head>
<body style="background-image: url('images/profile_banner.jpg'); background-position: top center">

<%@ include file="/WEB-INF/jspf/menu.jspf" %>

<div class="py-3"></div>

<div class="px-4 py-5 text-center text-white">
    <h1 class="display-6 fw-bold"><fmt:message key="profile.header"/></h1>

    <c:if test="${param.infoText != null}">
        <div class="text-success mt-3">
            <fmt:message key="${param.infoText}"/>
        </div>
    </c:if>
    <c:if test="${param.errorText != null}">
        <div class="text-danger mt-3">
            <fmt:message key="${param.errorText}"/>
        </div>
    </c:if>

    <div class="col-6 mx-auto pt-5 pb-5 px-5">
        <table class="table table-secondary table-hover">
            <thead class="table-dark">
            <tr>
                <th scope="col">
                    <fmt:message key="profile.field_name"/>
                </th>
                <th scope="col">
                    <fmt:message key="profile.field_value"/>
                </th>
                <th scope="col">
                    <fmt:message key="profile.actions"/>
                </th>
            </tr>
            </thead>
            <tbody class="table-group-divider">
            <tr>
                <th scope="col"><fmt:message key="profile.your_login"/></th>
                <td><c:out value="${sessionScope.login}"/></td>
                <td>
                    <a href="#" data-bs-toggle="modal" data-bs-target="#modalLoginChange">
                        <img src="images/edit.svg" width="18" height="18" alt="edit">
                    </a>
                </td>
            </tr>
            <tr>
                <th scope="col"><fmt:message key="profile.your_email"/></th>
                <td><c:out value="${requestScope.user.email}"/></td>
                <td>
                    <a href="#" data-bs-toggle="modal" data-bs-target="#modalEmailChange">
                        <img src="images/edit.svg" width="18" height="18" alt="edit">
                    </a>
                </td>
            </tr>
            <tr>
                <th scope="col"><fmt:message key="profile.your_password"/></th>
                <td>************</td>
                <td>
                    <a href="#" data-bs-toggle="modal" data-bs-target="#modalPasswordChange">
                        <img src="images/edit.svg" width="18" height="18" alt="edit">
                    </a>
                </td>
            </tr>
            <tr>
                <th scope="col"><fmt:message key="profile.your_phone_number"/></th>
                <td>+380 <c:out value="${requestScope.user.phoneNumber}"/></td>
                <td>
                    <a href="#" data-bs-toggle="modal" data-bs-target="#modalPhoneNumberChange">
                        <img src="images/edit.svg" width="18" height="18" alt="edit">
                    </a>
                </td>
            </tr>
            <tr>
                <th scope="col"><fmt:message key="profile.your_role"/></th>
                <td><fmt:message key="${requestScope.user.role}"/></td>
                <td>
                </td>
            </tr>
            <tr>
                <th scope="col"><fmt:message key="profile.your_name"/></th>
                <td><c:out value="${requestScope.user.firstName}"/></td>
                <td>
                    <a href="#" data-bs-toggle="modal" data-bs-target="#modalFirstNameChange">
                        <img src="images/edit.svg" width="18" height="18" alt="edit">
                    </a>
                </td>
            </tr>
            <tr>
                <th scope="col"><fmt:message key="profile.your_surname"/></th>
                <td><c:out value="${requestScope.user.lastName}"/></td>
                <td>
                    <a href="#" data-bs-toggle="modal" data-bs-target="#modalLastNameChange">
                        <img src="images/edit.svg" width="18" height="18" alt="edit">
                    </a>
                </td>
            </tr>
            </tbody>
        </table>
    </div>

</div>

<%@ include file="/WEB-INF/jspf/footer.jspf" %>

<%-- modal windows --%>

<div class="modal py-5 fade" tabindex="-1" role="dialog" id="modalLoginChange">
    <div class="modal-dialog" role="document">
        <div class="modal-content rounded-4 shadow bg-light">
            <div class="modal-header p-5 pb-4 border-bottom-0">
                <h1 class="fw-bold mb-0 fs-2"><fmt:message key="profile.change_login"/></h1>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>

            <div class="modal-body p-5 pt-0">
                <form class="" data-np-autofill-type="register" data-np-checked="1" data-np-watching="1"
                      action="controller" method="post">
                    <div class="form-floating mb-3">
                        <input type="hidden" name="action" value="changeUserField">
                        <input type="hidden" name="fieldName" value="login">
                        <input type="text" class="form-control rounded-3" id="floatingInput1" name="fieldValue"
                               autocomplete="off" required>
                        <label for="floatingInput1"><fmt:message key="profile.new_login"/></label>
                    </div>
                    <button class="w-100 mb-2 btn btn-lg rounded-3 btn-warning" type="submit"><fmt:message
                            key="profile.change"/></button>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal py-5 fade" tabindex="-1" role="dialog" id="modalEmailChange">
    <div class="modal-dialog" role="document">
        <div class="modal-content rounded-4 shadow bg-light">
            <div class="modal-header p-5 pb-4 border-bottom-0">
                <h1 class="fw-bold mb-0 fs-2"><fmt:message key="profile.change_email"/></h1>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>

            <div class="modal-body p-5 pt-0">
                <form class="" data-np-autofill-type="register" data-np-checked="1" data-np-watching="1"
                      action="controller" method="post">
                    <div class="form-floating mb-3">
                        <input type="hidden" name="action" value="changeUserField">
                        <input type="hidden" name="fieldName" value="email">
                        <input type="text" class="form-control rounded-3" id="floatingInput2" name="fieldValue"
                               autocomplete="off" required>
                        <label for="floatingInput2"><fmt:message key="profile.new_email"/></label>
                    </div>
                    <button class="w-100 mb-2 btn btn-lg rounded-3 btn-warning" type="submit"><fmt:message
                            key="profile.change"/></button>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal py-5 fade" tabindex="-1" role="dialog" id="modalPasswordChange">
    <div class="modal-dialog" role="document">
        <div class="modal-content rounded-4 shadow bg-light">
            <div class="modal-header p-5 pb-4 border-bottom-0">
                <h1 class="fw-bold mb-0 fs-2"><fmt:message key="profile.change_password"/></h1>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>

            <div class="modal-body p-5 pt-0">
                <form class="" data-np-autofill-type="register" data-np-checked="1" data-np-watching="1"
                      action="controller" method="post">
                    <div class="form-floating mb-3">
                        <input type="hidden" name="action" value="changeUserField">
                        <input type="hidden" name="fieldName" value="password">
                        <input type="password" class="form-control rounded-3" id="floatingInput3" name="fieldValue"
                               autocomplete="off" required>
                        <label for="floatingInput3"><fmt:message key="profile.new_password"/></label>
                    </div>
                    <button class="w-100 mb-2 btn btn-lg rounded-3 btn-warning" type="submit"><fmt:message
                            key="profile.change"/></button>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal py-5 fade" tabindex="-1" role="dialog" id="modalPhoneNumberChange">
    <div class="modal-dialog" role="document">
        <div class="modal-content rounded-4 shadow bg-light">
            <div class="modal-header p-5 pb-4 border-bottom-0">
                <h1 class="fw-bold mb-0 fs-2"><fmt:message key="profile.change_phone_number"/></h1>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>

            <div class="modal-body p-5 pt-0">
                <form class="" data-np-autofill-type="register" data-np-checked="1" data-np-watching="1"
                      action="controller" method="post">
                    <div class="form-floating mb-3">
                        <input type="hidden" name="action" value="changeUserField">
                        <input type="hidden" name="fieldName" value="phoneNumber">
                        <div class="input-group">
                            <span class="input-group-text">+380</span>
                            <input type="text" class="form-control rounded-3" id="floatingInput6" name="fieldValue"
                                   autocomplete="off" placeholder="637777777" required>
                        </div>
                    </div>
                    <button class="w-100 mb-2 btn btn-lg rounded-3 btn-warning" type="submit"><fmt:message
                            key="profile.change"/></button>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal py-5 fade" tabindex="-1" role="dialog" id="modalFirstNameChange">
    <div class="modal-dialog" role="document">
        <div class="modal-content rounded-4 shadow bg-light">
            <div class="modal-header p-5 pb-4 border-bottom-0">
                <h1 class="fw-bold mb-0 fs-2"><fmt:message key="profile.change_name"/></h1>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>

            <div class="modal-body p-5 pt-0">
                <form class="" data-np-autofill-type="register" data-np-checked="1" data-np-watching="1"
                      action="controller" method="post">
                    <div class="form-floating mb-3">
                        <input type="hidden" name="action" value="changeUserField">
                        <input type="hidden" name="fieldName" value="firstName">
                        <input type="text" class="form-control rounded-3" id="floatingInput4" name="fieldValue"
                               autocomplete="off" required>
                        <label for="floatingInput4"><fmt:message key="profile.new_name"/></label>
                    </div>
                    <button class="w-100 mb-2 btn btn-lg rounded-3 btn-warning" type="submit"><fmt:message
                            key="profile.change"/></button>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal py-5 fade" tabindex="-1" role="dialog" id="modalLastNameChange">
    <div class="modal-dialog" role="document">
        <div class="modal-content rounded-4 shadow bg-light">
            <div class="modal-header p-5 pb-4 border-bottom-0">
                <h1 class="fw-bold mb-0 fs-2"><fmt:message key="profile.change_surname"/></h1>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>

            <div class="modal-body p-5 pt-0">
                <form class="" data-np-autofill-type="register" data-np-checked="1" data-np-watching="1"
                      action="controller" method="post">
                    <div class="form-floating mb-3">
                        <input type="hidden" name="action" value="changeUserField">
                        <input type="hidden" name="fieldName" value="lastName">
                        <input type="text" class="form-control rounded-3" id="floatingInput5" name="fieldValue"
                               autocomplete="off" required>
                        <label for="floatingInput5"><fmt:message key="profile.new_surname"/></label>
                    </div>
                    <button class="w-100 mb-2 btn btn-lg rounded-3 btn-warning" type="submit"><fmt:message
                            key="profile.change"/></button>
                </form>
            </div>
        </div>
    </div>
</div>

</body>
</html>