<%@ include file="/WEB-INF/jspf/resources.jspf" %>

<!DOCTYPE html>
<html>
<head>
    <title><fmt:message key="title.edit_book"/></title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/bootstrap/css/bootstrap.min.css">
    <script src="${pageContext.request.contextPath}/bootstrap/js/bootstrap.bundle.min.js"></script>
</head>
<body style="background-image: url('images/edit_book_banner.jpg'); background-position: top center; background-size: cover">

<%@ include file="/WEB-INF/jspf/menu.jspf" %>

<div class="py-3"></div>

<div class="px-4 py-5 text-center text-white">
    <h1 class="display-6 fw-bold"><fmt:message key="edit_book.header"/></h1>

    <tags:success/>
    <tags:error/>

    <div class="col-6 mx-auto pt-5 pb-5 px-5">
        <table class="table table-secondary table-hover">
            <thead class="table-dark">
            <tr>
                <th scope="col">
                    <fmt:message key="profile.field_name"/>
                </th>
                <th scope="col">
                    <fmt:message key="profile.field_value"/>
                </th>
                <th scope="col">
                    <fmt:message key="profile.actions"/>
                </th>
            </tr>
            </thead>
            <tbody class="table-group-divider">
            <tr>
                <th scope="col"><fmt:message key="add_new_book.title"/></th>
                <td><c:out value="${requestScope.book.title}"/></td>
                <td>
                    <a href="#" data-bs-toggle="modal" data-bs-target="#modalTitleChange">
                        <img src="images/edit.svg" width="18" height="18" alt="edit">
                    </a>
                </td>
            </tr>
            <tr>
                <th scope="col"><fmt:message key="add_new_book.author_name_UA"/></th>
                <td><c:out value="${requestScope.book.author.nameUA}"/></td>
                <td>
                    <a href="#" data-bs-toggle="modal" data-bs-target="#modalAuthorNameUAChange">
                        <img src="images/edit.svg" width="18" height="18" alt="edit">
                    </a>
                </td>
            </tr>
            <tr>
                <th scope="col"><fmt:message key="add_new_book.author_name_EN"/></th>
                <td><c:out value="${requestScope.book.author.nameEN}"/></td>
                <td>
                    <a href="#" data-bs-toggle="modal" data-bs-target="#modalAuthorNameENChange">
                        <img src="images/edit.svg" width="18" height="18" alt="edit">
                    </a>
                </td>
            </tr>
            <tr>
                <th scope="col"><fmt:message key="add_new_book.publisher_house"/></th>
                <td><c:out value="${requestScope.book.publisherHouse}"/></td>
                <td>
                    <a href="#" data-bs-toggle="modal" data-bs-target="#modalPublisherHouseChange">
                        <img src="images/edit.svg" width="18" height="18" alt="edit">
                    </a>
                </td>
            </tr>
            <tr>
                <th scope="col"><fmt:message key="add_new_book.year"/></th>
                <td><c:out value="${requestScope.book.year}"/></td>
                <td>
                    <a href="#" data-bs-toggle="modal" data-bs-target="#modalYearChange">
                        <img src="images/edit.svg" width="18" height="18" alt="edit">
                    </a>
                </td>
            </tr>
            <tr>
                <th scope="col"><fmt:message key="add_new_book.language"/></th>
                <td><c:out value="${requestScope.book.language}"/></td>
                <td>
                    <a href="#" data-bs-toggle="modal" data-bs-target="#modalLanguageChange">
                        <img src="images/edit.svg" width="18" height="18" alt="edit">
                    </a>
                </td>
            </tr>
            <tr>
                <th scope="col"><fmt:message key="add_new_book.quantity"/></th>
                <td><c:out value="${requestScope.book.quantity}"/></td>
                <td>
                    <a href="#" data-bs-toggle="modal" data-bs-target="#modalQuantityChange">
                        <img src="images/edit.svg" width="18" height="18" alt="edit">
                    </a>
                </td>
            </tr>
            </tbody>
        </table>
    </div>

    <button type="button" class="btn btn-outline-light btn-w btn-sm px-4" data-bs-toggle="modal"
            data-bs-target="#modalAuthorNew"><fmt:message key="edit_book.change_author_name"/>
    </button>

</div>

<%@ include file="/WEB-INF/jspf/footer.jspf" %>

<%-- modal windows --%>

<div class="modal py-5 fade" tabindex="-1" role="dialog" id="modalTitleChange">
    <div class="modal-dialog" role="document">
        <div class="modal-content rounded-4 shadow bg-light">
            <div class="modal-header p-5 pb-4 border-bottom-0">
                <h1 class="fw-bold mb-0 fs-2"><fmt:message key="edit_book.change_title"/></h1>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>

            <div class="modal-body p-5 pt-0">
                <form class="" data-np-autofill-type="register" data-np-checked="1" data-np-watching="1"
                      action="controller" method="post">
                    <div class="form-floating mb-3">
                        <input type="hidden" name="action" value="changeBookField">
                        <input type="hidden" name="fieldName" value="title">
                        <input type="hidden" name="id" value="${requestScope.book.id}">
                        <input type="text" class="form-control rounded-3" id="floatingInput1" name="fieldValue"
                               autocomplete="off" required>
                        <label for="floatingInput1"><fmt:message key="edit_book.new_title"/></label>
                    </div>
                    <button class="w-100 mb-2 btn btn-lg rounded-3 btn-warning" type="submit"><fmt:message
                            key="profile.change"/></button>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal py-5 fade" tabindex="-1" role="dialog" id="modalAuthorNameUAChange">
    <div class="modal-dialog" role="document">
        <div class="modal-content rounded-4 shadow bg-light">
            <div class="modal-header p-5 pb-4 border-bottom-0">
                <h1 class="fw-bold mb-0 fs-2"><fmt:message key="edit_book.change_author_name_UA"/></h1>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>

            <div class="modal-body p-5 pt-0">
                <form class="" data-np-autofill-type="register" data-np-checked="1" data-np-watching="1"
                      action="controller" method="post">
                    <div class="form-floating mb-3">
                        <input type="hidden" name="action" value="changeBookField">
                        <input type="hidden" name="fieldName" value="authorNameUA">
                        <input type="hidden" name="id" value="${requestScope.book.id}">
                        <input type="text" class="form-control rounded-3" id="floatingInput2" name="fieldValue"
                               autocomplete="off" required>
                        <label for="floatingInput2"><fmt:message key="edit_book.new_author_name"/></label>
                    </div>
                    <button class="w-100 mb-2 btn btn-lg rounded-3 btn-warning" type="submit"><fmt:message
                            key="profile.change"/></button>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal py-5 fade" tabindex="-1" role="dialog" id="modalAuthorNameENChange">
    <div class="modal-dialog" role="document">
        <div class="modal-content rounded-4 shadow bg-light">
            <div class="modal-header p-5 pb-4 border-bottom-0">
                <h1 class="fw-bold mb-0 fs-2"><fmt:message key="edit_book.change_author_name_EN"/></h1>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>

            <div class="modal-body p-5 pt-0">
                <form class="" data-np-autofill-type="register" data-np-checked="1" data-np-watching="1"
                      action="controller" method="post">
                    <div class="form-floating mb-3">
                        <input type="hidden" name="action" value="changeBookField">
                        <input type="hidden" name="fieldName" value="authorNameEN">
                        <input type="hidden" name="id" value="${requestScope.book.id}">
                        <input type="text" class="form-control rounded-3" id="floatingInput3" name="fieldValue"
                               autocomplete="off" required>
                        <label for="floatingInput3"><fmt:message key="edit_book.new_author_name"/></label>
                    </div>
                    <button class="w-100 mb-2 btn btn-lg rounded-3 btn-warning" type="submit"><fmt:message
                            key="profile.change"/></button>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal py-5 fade" tabindex="-1" role="dialog" id="modalPublisherHouseChange">
    <div class="modal-dialog" role="document">
        <div class="modal-content rounded-4 shadow bg-light">
            <div class="modal-header p-5 pb-4 border-bottom-0">
                <h1 class="fw-bold mb-0 fs-2"><fmt:message key="edit_book.change_publisher_house"/></h1>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>

            <div class="modal-body p-5 pt-0">
                <form class="" data-np-autofill-type="register" data-np-checked="1" data-np-watching="1"
                      action="controller" method="post">
                    <div class="form-floating mb-3">
                        <input type="hidden" name="action" value="changeBookField">
                        <input type="hidden" name="fieldName" value="publisherHouse">
                        <input type="hidden" name="id" value="${requestScope.book.id}">
                        <input type="text" class="form-control rounded-3" id="floatingInput4" name="fieldValue"
                               autocomplete="off" required>
                        <label for="floatingInput4"><fmt:message key="edit_book.new_publisher_house"/></label>
                    </div>
                    <button class="w-100 mb-2 btn btn-lg rounded-3 btn-warning" type="submit"><fmt:message
                            key="profile.change"/></button>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal py-5 fade" tabindex="-1" role="dialog" id="modalYearChange">
    <div class="modal-dialog" role="document">
        <div class="modal-content rounded-4 shadow bg-light">
            <div class="modal-header p-5 pb-4 border-bottom-0">
                <h1 class="fw-bold mb-0 fs-2"><fmt:message key="edit_book.change_year"/></h1>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>

            <div class="modal-body p-5 pt-0">
                <form class="" data-np-autofill-type="register" data-np-checked="1" data-np-watching="1"
                      action="controller" method="post">
                    <div class="form-floating mb-3">
                        <input type="hidden" name="action" value="changeBookField">
                        <input type="hidden" name="fieldName" value="year">
                        <input type="hidden" name="id" value="${requestScope.book.id}">
                        <input type="text" class="form-control rounded-3" id="floatingInput5" name="fieldValue"
                               autocomplete="off" required>
                        <label for="floatingInput5"><fmt:message key="edit_book.new_year"/></label>
                    </div>
                    <button class="w-100 mb-2 btn btn-lg rounded-3 btn-warning" type="submit"><fmt:message
                            key="profile.change"/></button>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal py-5 fade" tabindex="-1" role="dialog" id="modalLanguageChange">
    <div class="modal-dialog" role="document">
        <div class="modal-content rounded-4 shadow bg-light">
            <div class="modal-header p-5 pb-4 border-bottom-0">
                <h1 class="fw-bold mb-0 fs-2"><fmt:message key="edit_book.change_language"/></h1>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>

            <div class="modal-body p-5 pt-0">
                <form class="" data-np-autofill-type="register" data-np-checked="1" data-np-watching="1"
                      action="controller" method="get">
                    <div class="form-floating mb-3">
                        <input type="hidden" name="action" value="changeBookField">
                        <input type="hidden" name="fieldName" value="language">
                        <input type="hidden" name="id" value="${requestScope.book.id}">
                        <div class="col-6 form-check">
                            <input class="form-check-input" type="radio" name="fieldValue" id="radio1" value="UA" checked>
                            <label class="form-check-label" for="radio1">
                                <fmt:message key="menu.ukrainian"/>
                            </label>
                        </div>
                        <div class="col-6 form-check">
                            <input class="form-check-input" type="radio" name="fieldValue" id="radio2" value="EN">
                            <label class="form-check-label" for="radio2">
                                <fmt:message key="menu.english"/>
                            </label>
                        </div>
                    </div>
                    <button class="w-100 mb-2 btn btn-lg rounded-3 btn-warning" type="submit"><fmt:message
                            key="profile.change"/></button>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal py-5 fade" tabindex="-1" role="dialog" id="modalQuantityChange">
    <div class="modal-dialog" role="document">
        <div class="modal-content rounded-4 shadow bg-light">
            <div class="modal-header p-5 pb-4 border-bottom-0">
                <h1 class="fw-bold mb-0 fs-2"><fmt:message key="edit_book.change_quantity"/></h1>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>

            <div class="modal-body p-5 pt-0">
                <form class="" data-np-autofill-type="register" data-np-checked="1" data-np-watching="1"
                      action="controller" method="post">
                    <div class="form-floating mb-3">
                        <input type="hidden" name="action" value="changeBookField">
                        <input type="hidden" name="fieldName" value="quantity">
                        <input type="hidden" name="id" value="${requestScope.book.id}">
                        <input type="text" class="form-control rounded-3" id="floatingInput7" name="fieldValue"
                               autocomplete="off" required>
                        <label for="floatingInput7"><fmt:message key="edit_book.new_quantity"/></label>
                    </div>
                    <button class="w-100 mb-2 btn btn-lg rounded-3 btn-warning" type="submit"><fmt:message
                            key="profile.change"/></button>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal py-5 fade" tabindex="-1" role="dialog" id="modalAuthorNew">
    <div class="modal-dialog" role="document">
        <div class="modal-content rounded-4 shadow bg-light">
            <div class="modal-header p-5 pb-4 border-bottom-0">
                <h1 class="fw-bold mb-0 fs-2"><fmt:message key="edit_book.change_author_name"/></h1>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>

            <div class="modal-body p-5 pt-0">
                <form class="" data-np-autofill-type="register" data-np-checked="1" data-np-watching="1"
                      action="controller" method="post">
                    <div class="form-floating mb-3">
                        <input type="hidden" name="action" value="reassignAuthor">
                        <input type="hidden" name="id" value="${requestScope.book.id}">
                        <input type="text" class="form-control rounded-3" id="floatingInput8" name="authorNameUA"
                               autocomplete="off" required>
                        <label for="floatingInput8"><fmt:message key="edit_book.new_author_name"/> UA</label>
                    </div>
                    <div class="form-floating mb-3">
                        <input type="text" class="form-control rounded-3" id="floatingInput9" name="authorNameEN"
                               autocomplete="off" required>
                        <label for="floatingInput9"><fmt:message key="edit_book.new_author_name"/> EN</label>
                    </div>
                    <button class="w-100 mb-2 btn btn-lg rounded-3 btn-warning" type="submit"><fmt:message
                            key="profile.change"/></button>
                </form>
            </div>
        </div>
    </div>
</div>

</body>
</html>