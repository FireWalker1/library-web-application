<%@ include file="/WEB-INF/jspf/resources.jspf" %>

<!DOCTYPE html>
<html>
<head>
    <title><fmt:message key="title.access_denied"/></title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/bootstrap/css/bootstrap.min.css">
    <script src="${pageContext.request.contextPath}/bootstrap/js/bootstrap.bundle.min.js"></script>
</head>
<body class="bg-dark">

<div class="py-3"></div>

<div class="container p-5 bg-dark" tabindex="-1" role="dialog" id="modalLogin">
    <div class="flex-row px-5 mx-5 my-5">
        <div class="modal-dialog py-5 w-50" tabindex="-1" role="dialog" id="modalError">
            <div class="modal-dialog" role="document">
                <div class="modal-content rounded-4 shadow bg-light">
                    <div class="modal-header p-5 pb-4 border-bottom-0">
                        <h1 class="fw-bold mb-0 fs-2 text-danger"><fmt:message key="title.access_denied"/></h1>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="fw-semibold pt-3 modal-body pb-4 ps-5">
                        <p class="text-black"><fmt:message key="success.info"/></p>
                    </div>
                    <div class="text-center mb-3">
                        <button type="button" class="btn btn-danger btn-lg text-center" onclick="window.location.href='index.jsp'"><fmt:message
                                key="success.continue"/></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>