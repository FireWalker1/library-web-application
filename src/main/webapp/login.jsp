<%@ include file="/WEB-INF/jspf/resources.jspf" %>

<!DOCTYPE html>
<html>
<head>
    <title><fmt:message key="title.login"/></title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/bootstrap/css/bootstrap.min.css">
    <script src="${pageContext.request.contextPath}/bootstrap/js/bootstrap.bundle.min.js"></script>
</head>
<body class="bg-dark">

<div class="container p-5 bg-dark" tabindex="-1" role="dialog" id="modalLogin">
    <div class="flex-row px-5 mx-5 my-5">
        <div class="modal-dialog w-50" role="document">
            <div class="modal-content rounded-4 shadow bg-light">
                <div class="modal-header p-5 pb-4 border-bottom-0">
                    <h1 class="fw-bold mb-0 fs-2"><fmt:message key="menu.enter_account"/></h1>
                    <a href="${pageContext.request.contextPath}/">
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </a>
                </div>

                <div class="modal-body p-5 pt-0">
                    <form class="" data-np-autofill-type="register" data-np-checked="1" data-np-watching="1"
                          action=controller method="post">
                        <div class="form-floating mb-3">
                            <input type="hidden" name="action" value="login">
                            <input type="text" class="form-control rounded-3" id="floatingInput" name="login"
                                   autocomplete="off">
                            <label for="floatingInput"><fmt:message key="menu.login"/></label>
                        </div>
                        <div class="form-floating mb-3">
                            <input type="password" class="form-control rounded-3" id="floatingPassword" name="password"
                                   autocomplete="off">
                            <label for="floatingPassword"><fmt:message key="menu.password"/></label>
                        </div>
                        <c:if test="${param.errorText != null}">
                            <div class="text-danger mb-3 small fw-bold">
                                <fmt:message key="${param.errorText}"/>
                            </div>
                        </c:if>
                        <button class="w-100 mb-2 btn btn-lg rounded-3 btn-warning" type="submit"><fmt:message
                                key="menu.enter"/></button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<%@ include file="/WEB-INF/jspf/footer.jspf" %>

</body>
</html>