<%@ include file="/WEB-INF/jspf/resources.jspf" %>

<!DOCTYPE html>
<html>
<head>
    <title><fmt:message key="title.about_us"/></title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/bootstrap/css/bootstrap.min.css">
    <script src="${pageContext.request.contextPath}/bootstrap/js/bootstrap.bundle.min.js"></script>
</head>
<body>

<%@ include file="/WEB-INF/jspf/menu.jspf" %>

<div class="py-3" style="background-color: #383840"></div>
<div class="px-4 py-5 text-center text-white" style="background-color: #383840">
    <h1 class="display-6 fw-bold"><fmt:message key="about_us.header"/></h1>
</div>
<div class="px-4 py-5 row row-cols-2 text-center text-white" style="background-color: #383840">
    <div class="col-6 mx-auto">
        <img src="images/about_us.png" alt="about" class="rounded" style="box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19)">
    </div>
    <div class="col-6 mx-auto">
        <p class="lead mb-4"><fmt:message key="about_us.info"/></p>
        <p class="lead mb-4"><fmt:message key="about_us.info2"/></p>
        <p class="lead mb-4"><fmt:message key="about_us.info3"/></p>
    </div>
</div>

<div class="py-3" style="background-color: #383840"></div>

<%@ include file="/WEB-INF/jspf/footer.jspf" %>

</body>
</html>