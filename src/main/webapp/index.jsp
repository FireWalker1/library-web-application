<%@ include file="/WEB-INF/jspf/resources.jspf" %>

<!DOCTYPE html>
<html>
<head>
    <title><fmt:message key="title.index"/></title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/bootstrap/css/bootstrap.min.css">
    <script src="${pageContext.request.contextPath}/bootstrap/js/bootstrap.bundle.min.js"></script>
</head>
<body>

<%@ include file="/WEB-INF/jspf/menu.jspf" %>

<div class="px-4 py-5 text-center text-white"
     style="background-image: url('images/lib_banner.jpg'); background-position: center center; background-size: cover">
    <img class="d-block mx-auto mb-4 py-4" src="images/logo_150.png" alt="library">
    <h1 class="display-6 fw-bold"><fmt:message key="index.header"/></h1>
    <div class="col-lg-6 mx-auto">
        <p class="lead mb-4"><fmt:message key="index.info_text"/></p>
        <c:if test="${empty sessionScope.login}">
            <div class="d-grid gap-2 d-sm-flex justify-content-sm-center py-4">
                <button type="button" class="btn btn-primary btn-warning btn-lg px-4 gap-3"
                        onclick="window.location.href='sign_up.jsp'">
                    <fmt:message key="index.register"/>
                </button>
                <button type="button" class="btn btn-outline-light btn-w btn-lg px-4" data-bs-toggle="modal"
                        data-bs-target="#modalLogin"><fmt:message key="index.login"/>
                </button>
            </div>
        </c:if>
        <c:if test="${not empty sessionScope.login}">
            <div class="d-grid gap-2 d-sm-flex justify-content-sm-center py-4">
                <form class="col-6" role="search" action="controller">
                    <input type="hidden" name="action" value="bookSearch">
                    <input type="hidden" name="pageNumber" value="1">
                    <input type="search" name="searchRequest" class="form-control form-control-dark text-bg-dark"
                           placeholder="<fmt:message key="menu.search"/>" aria-label="Search">
                </form>
            </div>
        </c:if>

    </div>
</div>

<%@ include file="/WEB-INF/jspf/footer.jspf" %>

</body>
</html>