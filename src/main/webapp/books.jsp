<%@ include file="/WEB-INF/jspf/resources.jspf" %>

<!DOCTYPE html>
<html>
<head>
    <title><fmt:message key="title.books"/></title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/bootstrap/css/bootstrap.min.css">
    <script src="${pageContext.request.contextPath}/bootstrap/js/bootstrap.bundle.min.js"></script>
</head>
<body style="background-image: url('images/book_banner.jpg'); background-position: top center">

<%@ include file="/WEB-INF/jspf/menu.jspf" %>

<div class="py-3"></div>

<div class="px-4 py-5 text-center text-white">
    <h1 class="display-6 fw-bold"><fmt:message key="books.book_catalog"/></h1>
    <p class="text-start fst-italic px-5"><fmt:message key="books.search_text"/></p>
    <form class="col-3 px-5 pb-4" role="search" action="controller">
        <input type="hidden" name="action" value="bookSearch">
        <input type="hidden" name="pageNumber" value="1">
        <input type="search" name="searchRequest" class="form-control form-control-dark text-bg-dark"
               placeholder="<fmt:message key="menu.search"/>" aria-label="Search">
    </form>

    <tags:success/>
    <tags:error/>

    <div class="col-12 mx-auto pt-4 pb-5 px-5">
        <table class="table table-secondary table-hover">
            <thead class="table-dark">
            <tr>
                <th scope="col">
                    <a href="controller?action=bookSearch&searchRequest=${param.searchRequest}&orderBy=id&desc=false&pageNumber=${param.pageNumber}"
                       style="text-decoration: none">
                        <img src="images/arrow-down.svg" width="16" height="16" alt="arrow-down">
                    </a>
                    <fmt:message key="books.book_id"/>
                    <a href="controller?action=bookSearch&searchRequest=${param.searchRequest}&orderBy=id&desc=true&pageNumber=${param.pageNumber}">
                        <img src="images/arrow-up.svg" width="16" height="16" alt="arrow-up">
                    </a>
                </th>
                <th scope="col">
                    <a href="controller?action=bookSearch&searchRequest=${param.searchRequest}&orderBy=title&desc=false&pageNumber=${param.pageNumber}"
                       style="text-decoration: none">
                        <img src="images/arrow-down.svg" width="16" height="16" alt="arrow-down">
                    </a>
                    <fmt:message key="books.title"/>
                    <a href="controller?action=bookSearch&searchRequest=${param.searchRequest}&orderBy=title&desc=true&pageNumber=${param.pageNumber}">
                        <img src="images/arrow-up.svg" width="16" height="16" alt="arrow-up">
                    </a>
                </th>
                <th scope="col">
                    <a href="controller?action=bookSearch&searchRequest=${param.searchRequest}&orderBy=name_UA&desc=false&pageNumber=${param.pageNumber}"
                       style="text-decoration: none">
                        <img src="images/arrow-down.svg" width="16" height="16" alt="arrow-down">
                    </a>
                    <fmt:message key="books.author"/>
                    <a href="controller?action=bookSearch&searchRequest=${param.searchRequest}&orderBy=name_UA&desc=true&pageNumber=${param.pageNumber}">
                        <img src="images/arrow-up.svg" width="16" height="16" alt="arrow-up">
                    </a>
                </th>
                <th scope="col">
                    <a href="controller?action=bookSearch&searchRequest=${param.searchRequest}&orderBy=publisher_house&desc=false&pageNumber=${param.pageNumber}"
                       style="text-decoration: none">
                        <img src="images/arrow-down.svg" width="16" height="16" alt="arrow-down">
                    </a>
                    <fmt:message key="books.publisher"/>
                    <a href="controller?action=bookSearch&searchRequest=${param.searchRequest}&orderBy=publisher_house&desc=true&pageNumber=${param.pageNumber}">
                        <img src="images/arrow-up.svg" width="16" height="16" alt="arrow-up">
                    </a>
                </th>
                <th scope="col">
                    <a href="controller?action=bookSearch&searchRequest=${param.searchRequest}&orderBy=year&desc=false&pageNumber=${param.pageNumber}"
                       style="text-decoration: none">
                        <img src="images/arrow-down.svg" width="16" height="16" alt="arrow-down">
                    </a>
                    <fmt:message key="books.year"/>
                    <a href="controller?action=bookSearch&searchRequest=${param.searchRequest}&orderBy=year&desc=true&pageNumber=${param.pageNumber}">
                        <img src="images/arrow-up.svg" width="16" height="16" alt="arrow-up">
                    </a>
                </th>
                <th scope="col">
                    <a href="controller?action=bookSearch&searchRequest=${param.searchRequest}&orderBy=language&desc=false&pageNumber=${param.pageNumber}"
                       style="text-decoration: none">
                        <img src="images/arrow-down.svg" width="16" height="16" alt="arrow-down">
                    </a>
                    <fmt:message key="books.language"/>
                    <a href="controller?action=bookSearch&searchRequest=${param.searchRequest}&orderBy=language&desc=true&pageNumber=${param.pageNumber}">
                        <img src="images/arrow-up.svg" width="16" height="16" alt="arrow-up">
                    </a>
                </th>
                <th scope="col">
                    <a href="controller?action=bookSearch&searchRequest=${param.searchRequest}&orderBy=quantity&desc=false&pageNumber=${param.pageNumber}"
                       style="text-decoration: none">
                        <img src="images/arrow-down.svg" width="16" height="16" alt="arrow-down">
                    </a>
                    <fmt:message key="books.quantity"/>
                    <a href="controller?action=bookSearch&searchRequest=${param.searchRequest}&orderBy=quantity&desc=true&pageNumber=${param.pageNumber}">
                        <img src="images/arrow-up.svg" width="16" height="16" alt="arrow-up">
                    </a>
                </th>
                <th scope="col">
                    <fmt:message key="books.actions"/>
                </th>
            </tr>
            </thead>
            <tbody class="table-group-divider">
            <c:forEach var="book" items="${requestScope.books}">
                <tr>
                    <th scope="col"><c:out value="${book.id}"/></th>
                    <td><c:out value="${book.title}"/></td>
                    <c:if test="${sessionScope.language == '' || sessionScope.language == null}">
                        <td><c:out value="${book.author.nameUA}"/></td>
                    </c:if>
                    <c:if test="${sessionScope.language == 'en'}">
                        <td><c:out value="${book.author.nameEN}"/></td>
                    </c:if>
                    <td><c:out value="${book.publisherHouse}"/></td>
                    <td><c:out value="${book.year}"/></td>
                    <td><c:out value="${book.language}"/></td>
                    <td><c:out value="${book.quantity}"/></td>
                    <td>
                        <c:if test="${sessionScope.role == 'role.reader'}">
                            <a href="controller?action=addBookToCart&id=${book.id}&searchRequest=${param.searchRequest}&orderBy=${param.orderBy}&desc=${param.desc}&pageNumber=${param.pageNumber}">
                                <img src="images/plus.svg" width="18" height="18" alt="arrow-up">
                            </a>
                        </c:if>
                        <c:if test="${sessionScope.role == 'role.administrator'}">
                            <a href="controller?action=getBook&id=${book.id}" style="text-decoration: none">
                                <img src="images/edit.svg" width="18" height="18" alt="edit">
                            </a>
                            <a href="controller?action=deleteBook&id=${book.id}">
                                <img src="images/delete.svg" width="21" height="21" alt="delete">
                            </a>
                        </c:if>
                    </td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </div>
    <div class="container">
        <c:if test="${param.pageNumber != 1}">
            <button class="btn btn-dark"
                    onclick="window.location.href='controller?action=bookSearch&searchRequest=${param.searchRequest}&orderBy=${param.orderBy}&desc=${param.desc}&pageNumber=${param.pageNumber - 1}'">
                <fmt:message key="books.previous"/></button>
        </c:if>
        <c:if test="${requestScope.pageQuantity != param.pageNumber}">
            <button class="btn btn-dark"
                    onclick="window.location.href='controller?action=bookSearch&searchRequest=${param.searchRequest}&orderBy=${param.orderBy}&desc=${param.desc}&pageNumber=${param.pageNumber + 1}'">
                <fmt:message key="books.next"/></button>
        </c:if>
    </div>
</div>

<div class="py-4"></div>

<%@ include file="/WEB-INF/jspf/footer.jspf" %>

</body>
</html>