<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<fmt:setLocale value="${sessionScope.language}"/>
<fmt:setBundle basename="resources"/>

<c:if test="${param.infoText != null}">
    <div class="text-success mt-3 fw-bold">
        <fmt:message key="${param.infoText}"/>
    </div>
</c:if>
