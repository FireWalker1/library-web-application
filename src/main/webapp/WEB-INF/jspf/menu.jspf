<header class="text-bg-dark py-2">
    <div class="container-fluid">
        <div class="d-flex flex-wrap align-items-center justify-content-center justify-content-lg-start">
            <a href="index.jsp" class="d-flex align-items-center mb-2 mb-lg-0 text-white text-decoration-none">
                <img src="images/logo_70.png" alt="logo">
            </a>
            <ul class="nav col-12 col-lg-auto me-lg-auto mb-2 justify-content-center mb-md-0">
                <li class="d-block px-4"></li>
                <li><a href="index.jsp" class="nav-link px-2 text-white"><fmt:message key="menu.main"/></a></li>
                <li><a href="about_us.jsp" class="nav-link px-2 text-white"><fmt:message key="menu.about"/></a></li>
                <li><a href="controller?action=bookSearch&searchRequest=&orderBy=id&desc=false&pageNumber=1" class="nav-link px-2 text-white"><fmt:message
                        key="menu.books"/></a></li>
                <li><a href="contacts.jsp" class="nav-link px-2 text-white"><fmt:message key="menu.contact"/></a></li>
            </ul>

            <div class="dropdown text-end dropdown-menu-dark me-3">
                <a href="#" class="d-block link-warning text-decoration-none dropdown-toggle text-warning"
                   data-bs-toggle="dropdown" aria-expanded="false">
                    <fmt:message key="menu.language"/>
                </a>
                <ul class="dropdown-menu text-small dropdown-menu-dark">
                    <li><a class="dropdown-item" href="controller?action=changeLanguage&language=ua"><fmt:message
                            key="menu.ukrainian"/></a></li>
                    <li><a class="dropdown-item" href="controller?action=changeLanguage&language=en"><fmt:message
                            key="menu.english"/></a></li>
                </ul>
            </div>

            <form class="col-12 col-lg-auto mb-3 mb-lg-0 me-lg-3" role="search" action="controller">
                <input type="hidden" name="action" value="bookSearch">
                <input type="hidden" name="pageNumber" value="1">
                <input type="search" name="searchRequest" class="form-control form-control-dark text-bg-dark"
                       placeholder="<fmt:message key="menu.search"/>" aria-label="Search">
            </form>

            <c:if test="${empty sessionScope.login}">
                <div class="text-end">
                    <button type="button" class="btn btn-outline-light me-2" data-bs-toggle="modal"
                            data-bs-target="#modalLogin"><fmt:message key="menu.enter"/>
                    </button>
                    <button type="button" class="btn btn-warning" onclick="window.location.href='sign_up.jsp'">
                        <fmt:message key="menu.register"/>
                    </button>
                </div>
            </c:if>

            <c:if test="${not empty sessionScope.login}">

                <div class="pe-3 text-white align-self-center">
                    <fmt:message key="menu.greetings"/> <i>${sessionScope.login}!</i>
                </div>

                <div class="dropdown text-end dropdown-menu-dark">
                    <a href="#" class="d-block link-warning text-decoration-none dropdown-toggle text-warning"
                       data-bs-toggle="dropdown" aria-expanded="false">
                        <img src="images/person.svg" alt="mdo" width="32" height="32" class="rounded-circle">
                    </a>
                    <ul class="dropdown-menu text-small dropdown-menu-dark">
                        <li><a class="dropdown-item" href="controller?action=getUser"><fmt:message
                                key="menu.profile"/></a></li>
                        <c:if test="${sessionScope.role == 'role.reader'}">
                            <li><a class="dropdown-item" href="cart.jsp"><fmt:message key="menu.cart"/></a></li>
                            <li><a class="dropdown-item" href="controller?action=getMySubscriptions"><fmt:message
                                    key="menu.my_subscriptions"/></a></li>
                        </c:if>
                        <c:if test="${sessionScope.role == 'role.librarian'}">
                            <li><a class="dropdown-item" href="controller?action=getAllSubscriptions"><fmt:message
                                    key="menu.all_subscriptions"/></a></li>
                            <li><a class="dropdown-item" href="controller?action=getAllReaders"><fmt:message
                                    key="menu.readers_list"/></a></li>
                        </c:if>
                        <c:if test="${sessionScope.role == 'role.administrator'}">
                            <li><a class="dropdown-item" href="add_new_book.jsp"><fmt:message
                                    key="menu.add_new_book"/></a></li>
                            <li><a class="dropdown-item" href="sign_up.jsp"><fmt:message
                                    key="menu.register_librarian"/></a></li>
                            <li><a class="dropdown-item" href="controller?action=getAllUsers"><fmt:message
                                    key="menu.users_list"/></a></li>
                        </c:if>
                        <li>
                            <hr class="dropdown-divider">
                        </li>
                        <li><a class="dropdown-item" href="controller?action=logout"><fmt:message
                                key="menu.logout"/></a></li>
                    </ul>
                </div>
            </c:if>
        </div>
    </div>
</header>

<%--Login modal window--%>

<div class="modal py-5 fade" tabindex="-1" role="dialog" id="modalLogin">
    <div class="modal-dialog" role="document">
        <div class="modal-content rounded-4 shadow bg-light">
            <div class="modal-header p-5 pb-4 border-bottom-0">
                <h1 class="fw-bold mb-0 fs-2"><fmt:message key="menu.enter_account"/></h1>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>

            <div class="modal-body p-5 pt-0">
                <form class="" data-np-autofill-type="register" data-np-checked="1" data-np-watching="1"
                      action="controller" method="post">
                    <div class="form-floating mb-3">
                        <input type="hidden" name="action" value="login">
                        <input type="text" class="form-control rounded-3" id="floatingInput" name="login"
                               autocomplete="off">
                        <label for="floatingInput"><fmt:message key="menu.login"/></label>
                    </div>
                    <div class="form-floating mb-3">
                        <input type="password" class="form-control rounded-3" id="floatingPassword" name="password"
                               autocomplete="off">
                        <label for="floatingPassword"><fmt:message key="menu.password"/></label>
                    </div>
                    <button class="w-100 mb-2 btn btn-lg rounded-3 btn-warning" type="submit"><fmt:message
                            key="menu.enter"/></button>
                </form>
            </div>
        </div>
    </div>
</div>

