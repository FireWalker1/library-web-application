package com.ilin.library.controller.commands.administrator;

import com.ilin.library.controller.Path;
import com.ilin.library.controller.commands.Command;
import com.ilin.library.dto.BookDTO;
import com.ilin.library.entities.Book;
import com.ilin.library.services.BookService;
import com.ilin.library.services.implementation.BookServiceImpl;
import com.ilin.library.utils.AddressBuilder;
import com.ilin.library.utils.Validator;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * AddNewBookCommand class. Restricted access for administrator role only. Implements PRG pattern.
 * After verification of all input information calls service method to add new book to database.
 *
 * @author Pavlo Ilin
 * @version 1.0
 */
public class AddNewBookCommand implements Command {
    private final BookService service;

    /**
     * Constructor to use in all cases except testing
     */
    public AddNewBookCommand() {
        service = new BookServiceImpl();
    }

    /**
     * Constructor to use during testing
     *
     * @param service is mocked service
     */
    public AddNewBookCommand(BookService service) {
        this.service = service;
    }


    /**
     * Gets all parameters from request. Calls validator to check parameters and generating errors (if necessary).
     * Calls appropriate methods of the service. Redirecting to appropriate page with generated information.
     *
     * @param request  provided by Controller
     * @param response provided by Controller
     * @return address to redirect or forward by front-controller
     */
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws IOException {
        BookDTO book = getBookDTOFromRequest(request);
        Map<String, String> info = new HashMap<>();

        validateBookDTOFields(book, info);
        if (!info.isEmpty()) {
            response.sendRedirect(Path.PAGE_ADD_NEW_BOOK
                    + AddressBuilder.formPageProperties(request.getQueryString(), info));
            return Path.REDIRECT;
        }

        if (service.insertBook(book)) {
            response.sendRedirect(Path.PAGE_SUCCESS);
            return Path.REDIRECT;
        } else {
            return AddressBuilder.logAndRedirectToErrorPage("Error redirect from AddNewBookCommand", response);
        }
    }

    private void validateBookDTOFields(BookDTO book, Map<String, String> info) {
        String titleError = Validator.checkTitle(book.getTitle());
        String authorNameUAError = Validator.checkAuthorNameUA(book.getAuthor().getNameUA());
        String authorNameENError = Validator.checkAuthorNameEN(book.getAuthor().getNameEN());
        String publisherHouseError = Validator.checkPublisherHouse(book.getPublisherHouse());
        String yearError = Validator.checkYear(book.getYear());
        String quantityError = Validator.checkQuantity(book.getQuantity());
        setAllErrorAttributes(info, titleError, authorNameUAError, authorNameENError,
                publisherHouseError, yearError, quantityError);
        if (service.isSuchBookExists(book)) {
            info.put("duplicateError", "add_new_book.error_already_exists");
        }
    }

    private BookDTO getBookDTOFromRequest(HttpServletRequest request) {
        String title = request.getParameter("title").trim();
        String authorNameUA = request.getParameter("authorNameUA").trim();
        String authorNameEN = request.getParameter("authorNameEN").trim();
        String publisherHouse = request.getParameter("publisherHouse").trim();
        int year = Validator.validateInt(request.getParameter("year").trim());
        String language = request.getParameter("language");
        int quantity = Validator.validateInt(request.getParameter("quantity").trim());
        return service.createBook(title, authorNameUA, authorNameEN, publisherHouse, year, language, quantity);
    }

    private void setAllErrorAttributes(Map<String, String> info, String titleError, String authorNameUAError,
                                       String authorNameENError, String publisherHouseError,
                                       String yearError, String quantityError) {
        if (titleError != null) {
            info.put("titleError", titleError);
        }
        if (authorNameUAError != null) {
            info.put("authorNameUAError", authorNameUAError);
        }
        if (authorNameENError != null) {
            info.put("authorNameENError", authorNameENError);
        }
        if (publisherHouseError != null) {
            info.put("publisherHouseError", publisherHouseError);
        }
        if (yearError != null) {
            info.put("yearError", yearError);
        }
        if (quantityError != null) {
            info.put("quantityError", quantityError);
        }
    }
}
