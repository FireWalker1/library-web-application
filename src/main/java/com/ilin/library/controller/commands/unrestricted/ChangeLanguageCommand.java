package com.ilin.library.controller.commands.unrestricted;

import com.ilin.library.controller.Path;
import com.ilin.library.controller.commands.Command;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

import java.io.IOException;

/**
 * ChangeLanguageCommand class. Unrestricted access.
 * Changing locale language of the .jsp pages.
 *
 * @author Pavlo Ilin
 * @version 1.0
 */
public class ChangeLanguageCommand implements Command {

    /**
     * Gets session and parameters from request.
     * Setting new language parameter. Saving language to Cookie. Forwarding to appropriate page.
     *
     * @param request  provided by Controller
     * @param response provided by Controller
     * @return address to redirect or forward by front-controller
     */
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws IOException {
        HttpSession session = request.getSession();
        String language = request.getParameter("language");

        if (language.equals("ua")) {
            language = "";
        }
        session.setAttribute("language", language);
        Cookie cookie = new Cookie("language", language);
        response.addCookie(cookie);
        response.sendRedirect(request.getHeader("referer"));
        return Path.REDIRECT;
    }
}
