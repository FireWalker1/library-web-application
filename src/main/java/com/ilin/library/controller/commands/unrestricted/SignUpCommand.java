package com.ilin.library.controller.commands.unrestricted;

import com.ilin.library.controller.Path;
import com.ilin.library.controller.commands.Command;
import com.ilin.library.dto.UserDTO;
import com.ilin.library.services.UserService;
import com.ilin.library.services.implementation.UserServiceImpl;
import com.ilin.library.utils.AddressBuilder;
import com.ilin.library.utils.Validator;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * SignUpCommand class. Unrestricted access. Implements PRG pattern.
 * After verification of all input information calls service method to add new user to database.
 *
 * @author Pavlo Ilin
 * @version 1.0
 */
public class SignUpCommand implements Command {
    private final UserService service;

    /**
     * Constructor to use in all cases except testing
     */
    public SignUpCommand() {
        service = new UserServiceImpl();
    }

    /**
     * Constructor to use during testing
     *
     * @param service is mocked service
     */
    public SignUpCommand(UserService service) {
        this.service = service;
    }

    /**
     * Gets all parameters from request. Calls validator to check parameters and generating errors (if necessary).
     * Calls appropriate methods of the service. Redirecting to appropriate page with generated information.
     *
     * @param request  provided by Controller
     * @param response provided by Controller
     * @return address to redirect or forward by front-controller
     */
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws IOException {
        UserDTO user = getUserDTOFromRequest(request);
        Map<String, String> info = new HashMap<>();

        validateUserDTOFields(user, info);
        if (!info.isEmpty()) {
            response.sendRedirect(Path.PAGE_SIGN_UP + AddressBuilder.formPageProperties(request.getQueryString(), info));
            return Path.REDIRECT;
        }

        if (user.getPhoneNumber() != null && user.getPhoneNumber().isBlank()) {
            user.setPhoneNumber(null);
        }
        if (service.insertUser(user)) {
            response.sendRedirect(Path.PAGE_SUCCESS);
            return Path.REDIRECT;
        } else {
            return AddressBuilder.logAndRedirectToErrorPage("Error redirect from SignUpCommand", response);
        }
    }

    private UserDTO getUserDTOFromRequest(HttpServletRequest request) {
        String firstName = request.getParameter("firstName").trim();
        String lastName = request.getParameter("lastName").trim();
        String login = request.getParameter("login").trim();
        String password = request.getParameter("password").trim();
        String email = request.getParameter("email").trim();
        String phoneNumber = request.getParameter("phoneNumber");
        if (phoneNumber != null) {
            phoneNumber = phoneNumber.trim();
        }
        String role = (String) request.getSession().getAttribute("role");
        String signUpRole;
        if (Objects.equals(role, "role.administrator")) {
            signUpRole = "role.librarian";
        } else {
            signUpRole = "role.reader";
        }
        return new UserDTO(0, login, email, password, phoneNumber, signUpRole, firstName, lastName, null);
    }

    private void validateUserDTOFields (UserDTO user, Map<String, String> info) {
        String firstNameError = Validator.checkFirstName(user.getFirstName());
        String lastNameError = Validator.checkLastName(user.getLastName());
        String loginError = Validator.checkLogin(user.getLogin());
        String passwordError = Validator.checkPassword(user.getPassword());
        String emailError = Validator.checkEmail(user.getEmail());
        String phoneNumberError = Validator.checkPhoneNumber(user.getPhoneNumber());

        if (service.isSuchLoginExists(user.getLogin())) {
            loginError = "sign_up.error_login_already_exists";
        }
        if (service.isSuchEmailExists(user.getEmail())) {
            emailError = "sign_up.error_email_already_exists";
        }
        if (user.getPhoneNumber() != null && service.isSuchPhoneNumberExists(user.getPhoneNumber())) {
            phoneNumberError = "sign_up.error_phone_number_already_exists";
        }
        setAllErrorAttributes(info, firstNameError, lastNameError, loginError, passwordError,
                emailError, phoneNumberError);
    }

    private void setAllErrorAttributes(Map<String, String> info, String firstNameError, String lastNameError,
                                       String loginError, String passwordError, String emailError, String phoneNumberError) {
        if (firstNameError != null) {
            info.put("firstNameError", firstNameError);
        }
        if (lastNameError != null) {
            info.put("lastNameError", lastNameError);
        }
        if (loginError != null) {
            info.put("loginError", loginError);
        }
        if (passwordError != null) {
            info.put("passwordError", passwordError);
        }
        if (emailError != null) {
            info.put("emailError", emailError);
        }
        if (phoneNumberError != null) {
            info.put("phoneNumberError", phoneNumberError);
        }
    }
}
