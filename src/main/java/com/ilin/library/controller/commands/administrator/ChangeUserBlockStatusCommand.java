package com.ilin.library.controller.commands.administrator;

import com.ilin.library.controller.Path;
import com.ilin.library.controller.commands.Command;
import com.ilin.library.services.UserService;
import com.ilin.library.services.implementation.UserServiceImpl;
import com.ilin.library.utils.AddressBuilder;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * ChangeUserBlockStatus class. Restricted access for administrator role only. Implements PRG pattern.
 * Calls service method to change current block status of user on the opposite.
 *
 * @author Pavlo Ilin
 * @version 1.0
 */
public class ChangeUserBlockStatusCommand implements Command {
    private final UserService service;
    private static final Logger logger = LogManager.getLogger(ChangeUserBlockStatusCommand.class);

    /**
     * Constructor to use in all cases except testing
     */
    public ChangeUserBlockStatusCommand() {
        service = new UserServiceImpl();
    }

    /**
     * Constructor to use during testing
     *
     * @param service is mocked service
     */
    public ChangeUserBlockStatusCommand(UserService service) {
        this.service = service;
    }

    /**
     * Gets all parameters from request. Generating errors text (if necessary).
     * Calls appropriate methods of the service. Redirecting to appropriate page with generated information.
     *
     * @param request  provided by Controller
     * @param response provided by Controller
     * @return address to redirect or forward by front-controller
     */
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String login = request.getParameter("login");
        Map<String, String> info = new HashMap<>();

        if (service.changeBlockStatus(login)) {
            info.put("infoText", "users.info_successful_block");
        }
        else {
            info.put("errorText", "users.error_unsuccessful_block");
            logger.error("Unsuccessful block in ChangeUserBlockStatusCommand - execute()");
        }
        response.sendRedirect(Path.COMMAND_GET_ALL_USERS + AddressBuilder.formCommandProperties(null, info));
        return Path.REDIRECT;
    }

}
