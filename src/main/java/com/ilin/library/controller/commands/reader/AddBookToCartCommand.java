package com.ilin.library.controller.commands.reader;

import com.ilin.library.controller.Path;
import com.ilin.library.controller.commands.Command;
import com.ilin.library.dto.BookDTO;
import com.ilin.library.services.BookService;
import com.ilin.library.services.implementation.BookServiceImpl;
import com.ilin.library.utils.AddressBuilder;
import com.ilin.library.utils.Validator;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * AddBookToCartCommand class. Restricted access for reader role only. Implements PRG pattern.
 * Calls service method to add book to the current reader's cart.
 *
 * @author Pavlo Ilin
 * @version 1.0
 */
public class AddBookToCartCommand implements Command {
    private final BookService service;

    /**
     * Constructor to use in all cases except testing
     */
    public AddBookToCartCommand() {
        service = new BookServiceImpl();
    }

    /**
     * Constructor to use during testing
     *
     * @param service is mocked service
     */
    public AddBookToCartCommand(BookService service) {
        this.service = service;
    }

    /**
     * Gets all parameters from request.
     * Calls appropriate methods of the service. Redirecting to appropriate page.
     *
     * @param request  provided by Controller
     * @param response provided by Controller
     * @return address to redirect or forward by front-controller
     */
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws IOException {
        int bookID = Validator.validateInt(request.getParameter("id"));
        HttpSession session = request.getSession();
        List<BookDTO> bookCart = Validator.validateBookListAttribute(session, "bookCart");
        Map<String, String> info = new HashMap<>();

        BookDTO book = service.addBookToCart(bookID);
        if (book == null) {
            info.put("errorText", "books.error_add_book_to_cart");
        } else {
            bookCart.add(book);
            session.setAttribute("bookCart", bookCart);
            info.put("infoText", "books.info_add_book_to_cart");
        }
        response.sendRedirect(Path.COMMAND_BOOK_SEARCH + AddressBuilder.formCommandProperties(request.getQueryString(), info));
        return Path.REDIRECT;
    }
}
