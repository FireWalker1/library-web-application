package com.ilin.library.controller.commands.common;

import com.ilin.library.controller.Path;
import com.ilin.library.controller.commands.Command;
import com.ilin.library.dto.SubscriptionDTO;
import com.ilin.library.services.SubscriptionService;
import com.ilin.library.services.implementation.SubscriptionServiceImpl;
import com.ilin.library.utils.AddressBuilder;
import com.ilin.library.utils.Validator;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;

/**
 * GetSubscriptionInfoCommand class. Restricted access for any signed in role.
 * Calls service method to get all information about certain subscription from the database.
 *
 * @author Pavlo Ilin
 * @version 1.0
 */
public class GetSubscriptionInfoCommand implements Command {
    private final SubscriptionService service;

    /**
     * Constructor to use in all cases except testing
     */
    public GetSubscriptionInfoCommand() {
        service = new SubscriptionServiceImpl();
    }

    /**
     * Constructor to use during testing
     *
     * @param service is mocked service
     */
    public GetSubscriptionInfoCommand(SubscriptionService service) {
        this.service = service;
    }

    /**
     * Gets all parameters from request.
     * Calls appropriate methods of the service. Forwarding to appropriate page with generated information.
     *
     * @param request  provided by Controller
     * @param response provided by Controller
     * @return address to redirect or forward by front-controller
     */
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws IOException {
        int subscriptionID = Validator.validateInt(request.getParameter("id"));

        SubscriptionDTO subscription = service.getSubscriptionByID(subscriptionID);
        if (subscription == null || subscription.getBooks().size() == 0) {
            return AddressBuilder.logAndRedirectToErrorPage("Error redirect from GetSubscriptionInfoCommand",
                    response);
        }
        request.setAttribute("subscription", subscription);
        return Path.PAGE_SUBSCRIPTION_INFO;
    }
}
