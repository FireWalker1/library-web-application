package com.ilin.library.controller.commands.administrator;

import com.ilin.library.controller.Path;
import com.ilin.library.controller.commands.Command;
import com.ilin.library.services.UserService;
import com.ilin.library.services.implementation.UserServiceImpl;
import com.ilin.library.utils.AddressBuilder;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * DeleteLibrarianCommand class. Restricted access for administrator role only. Implements PRG pattern.
 * Calls service method to delete user with librarian role from the database.
 *
 * @author Pavlo Ilin
 * @version 1.0
 */
public class DeleteLibrarianCommand implements Command {
    private final UserService service;
    private static final Logger logger = LogManager.getLogger(DeleteLibrarianCommand.class);

    /**
     * Constructor to use in all cases except testing
     */
    public DeleteLibrarianCommand() {
        service = new UserServiceImpl();
    }

    /**
     * Constructor to use during testing
     *
     * @param service is mocked service
     */
    public DeleteLibrarianCommand(UserService service) {
        this.service = service;
    }

    /**
     * Gets all parameters from request. Generating errors text (if necessary).
     * Calls appropriate methods of the service. Redirecting to appropriate page with generated information.
     *
     * @param request  provided by Controller
     * @param response provided by Controller
     * @return address to redirect or forward by front-controller
     */
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String login = request.getParameter("login");
        Map<String, String> info = new HashMap<>();

        if (service.deleteLibrarian(login)) info.put("infoText", "users.info_successful_delete");
        else {
            info.put("errorText", "users.error_unsuccessful_delete");
            logger.error("Unsuccessful delete in DeleteLibrarianCommand - execute()");
        }
        response.sendRedirect(Path.COMMAND_GET_ALL_USERS + AddressBuilder.formCommandProperties(null, info));
        return Path.REDIRECT;
    }
}
