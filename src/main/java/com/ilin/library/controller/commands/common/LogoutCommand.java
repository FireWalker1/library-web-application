package com.ilin.library.controller.commands.common;

import com.ilin.library.controller.Path;
import com.ilin.library.controller.commands.Command;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

import java.io.IOException;
import java.util.Arrays;

/**
 * LogoutCommand class. Restricted access for any signed in role. Implements PRG pattern.
 * Closing current session for device.
 *
 * @author Pavlo Ilin
 * @version 1.0
 */
public class LogoutCommand implements Command {

    /**
     * Get session from session and invalidating it. If "language" Cookie is saved, then setting this language.
     *
     * @param request  provided by Controller
     * @param response provided by Controller
     * @return address to redirect or forward by front-controller
     */
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws IOException {
        HttpSession session = request.getSession(false);
        if (session != null) {
            session.invalidate();
        }
        if (Arrays.stream(request.getCookies()).anyMatch(x -> x.getName().equals("language"))) {
            String language = Arrays.stream(request.getCookies())
                    .filter(x -> x.getName().equals("language"))
                    .findFirst()
                    .orElse(new Cookie("language", "ua"))
                    .getValue();
            session = request.getSession();
            session.setAttribute("language", language);
        }
        response.sendRedirect(Path.PAGE_INDEX);
        return Path.REDIRECT;
    }
}
