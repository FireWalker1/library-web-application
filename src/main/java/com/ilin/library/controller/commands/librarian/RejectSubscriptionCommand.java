package com.ilin.library.controller.commands.librarian;

import com.ilin.library.controller.Path;
import com.ilin.library.controller.commands.Command;
import com.ilin.library.services.SubscriptionService;
import com.ilin.library.services.implementation.SubscriptionServiceImpl;
import com.ilin.library.utils.AddressBuilder;
import com.ilin.library.utils.Validator;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;

/**
 * RejectSubscriptionCommand class. Restricted access for librarian role only. Implements PRG pattern.
 * Calls service method to progress status of subscription to the cancelled one at once.
 *
 * @author Pavlo Ilin
 * @version 1.0
 */
public class RejectSubscriptionCommand implements Command {
    private final SubscriptionService service;

    /**
     * Constructor to use in all cases except testing
     */
    public RejectSubscriptionCommand() {
        service = new SubscriptionServiceImpl();
    }

    /**
     * Constructor to use during testing
     *
     * @param service is mocked service
     */
    public RejectSubscriptionCommand(SubscriptionService service) {
        this.service = service;
    }

    /**
     * Gets all parameters from request.
     * Calls appropriate methods of the service. Redirecting to appropriate page.
     *
     * @param request  provided by Controller
     * @param response provided by Controller
     * @return address to redirect or forward by front-controller
     */
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws IOException {
        int subscriptionID = Validator.validateInt(request.getParameter("id"));

        if (service.rejectSubscription(subscriptionID)) {
            response.sendRedirect(Path.PAGE_SUCCESS);
            return Path.REDIRECT;
        }
        return AddressBuilder.logAndRedirectToErrorPage("Error redirect from RejectSubscriptionCommand", response);
    }
}
