package com.ilin.library.controller.commands.common;

import com.ilin.library.controller.Path;
import com.ilin.library.controller.commands.Command;
import com.ilin.library.dto.UserDTO;
import com.ilin.library.services.UserService;
import com.ilin.library.services.implementation.UserServiceImpl;
import com.ilin.library.utils.AddressBuilder;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

import java.io.IOException;

/**
 * GetUserCommand class. Restricted access for any signed in role.
 * Calls service method to get all information about certain user from the database.
 *
 * @author Pavlo Ilin
 * @version 1.0
 */
public class GetUserCommand implements Command {
    private final UserService service;

    /**
     * Constructor to use in all cases except testing
     */
    public GetUserCommand() {
        service = new UserServiceImpl();
    }

    /**
     * Constructor to use during testing
     *
     * @param service is mocked service
     */
    public GetUserCommand(UserService service) {
        this.service = service;
    }

    /**
     * Gets all parameters from request.
     * Calls appropriate methods of the service. Forwarding to appropriate page with generated information.
     *
     * @param request  provided by Controller
     * @param response provided by Controller
     * @return address to redirect or forward by front-controller
     */
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws IOException {
        HttpSession session = request.getSession();
        String userLogin = (String) session.getAttribute("login");

        UserDTO user = service.getByLogin(userLogin);
        if (user == null) {
            return AddressBuilder.logAndRedirectToErrorPage("Error redirect from GetUserCommand", response);
        }
        request.setAttribute("user", user);
        return Path.PAGE_PROFILE;
    }
}
