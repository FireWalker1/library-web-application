package com.ilin.library.controller.commands;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;

/**
 * Command interface. Required to implement for creation of the new command.
 *
 * @author Pavlo Ilin
 * @version 1.0
 */
public interface Command {

    /**
     * Main and only public method for executing certain command
     *
     * @param request  provided by Controller
     * @param response provided by Controller
     * @return address to redirect or forward by front-controller
     */
    String execute(HttpServletRequest request, HttpServletResponse response) throws IOException;
}
