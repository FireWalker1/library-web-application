package com.ilin.library.controller.commands.unrestricted;

import com.ilin.library.controller.Path;
import com.ilin.library.controller.commands.Command;
import com.ilin.library.dto.UserDTO;
import com.ilin.library.services.UserService;
import com.ilin.library.services.implementation.UserServiceImpl;
import com.ilin.library.utils.AddressBuilder;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * LoginCommand class. Unrestricted access. Implements PRG pattern.
 * Calls service method to verify whether indicated user with login and password exists and logging in.
 *
 * @author Pavlo Ilin
 * @version 1.0
 */
public class LoginCommand implements Command {
    private final UserService service;

    /**
     * Constructor to use in all cases except testing
     */
    public LoginCommand() {
        service = new UserServiceImpl();
    }

    /**
     * Constructor to use during testing
     *
     * @param service is mocked service
     */
    public LoginCommand(UserService service) {
        this.service = service;
    }

    /**
     * Gets all parameters from request.
     * Calls appropriate methods of the service. Redirecting to appropriate page.
     *
     * @param request  provided by Controller
     * @param response provided by Controller
     * @return address to redirect or forward by front-controller
     */
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws IOException {
        HttpSession session = request.getSession();
        Map<String, String> info = new HashMap<>();
        String login = request.getParameter("login");
        String password = request.getParameter("password");

        if (login == null || password == null || login.isEmpty() || password.isEmpty()) {
            info.put("errorText", "login.error_empty_fields");
            return makeRedirect(request, response, info, Path.PAGE_LOGIN);
        }

        UserDTO user = service.getByLoginAndPassword(login, password);
        if (user == null) {
            info.put("errorText", "login.error_incorrect_fields");
            return makeRedirect(request, response, info, Path.PAGE_LOGIN);
        } else if (user.getBlockedAt() != null) {
            info.put("errorText", "login.error_user_is_blocked");
            return makeRedirect(request, response, info, Path.PAGE_LOGIN);
        } else {
            session.setAttribute("login", user.getLogin());
            session.setAttribute("role", user.getRole());
            return makeRedirect(request, response, info, Path.PAGE_INDEX);
        }
    }

    private String makeRedirect(HttpServletRequest request, HttpServletResponse response, Map<String, String> info,
                                String forward) throws IOException {
        response.sendRedirect(forward + AddressBuilder.formPageProperties(request.getQueryString(), info));
        return Path.REDIRECT;
    }
}
