package com.ilin.library.controller.commands.administrator;

import com.ilin.library.controller.Path;
import com.ilin.library.controller.commands.Command;
import com.ilin.library.services.BookService;
import com.ilin.library.services.implementation.BookServiceImpl;
import com.ilin.library.utils.AddressBuilder;
import com.ilin.library.utils.Validator;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * DeleteBookCommand class. Restricted access for administrator role only. Implements PRG pattern.
 * Calls service method to delete book from the database.
 *
 * @author Pavlo Ilin
 * @version 1.0
 */
public class DeleteBookCommand implements Command {
    private final BookService service;
    private static final Logger logger = LogManager.getLogger(DeleteBookCommand.class);

    /**
     * Constructor to use in all cases except testing
     */
    public DeleteBookCommand() {
        service = new BookServiceImpl();
    }

    /**
     * Constructor to use during testing
     *
     * @param service is mocked service
     */
    public DeleteBookCommand(BookService service) {
        this.service = service;
    }

    /**
     * Gets all parameters from request. Generating errors text (if necessary).
     * Calls appropriate methods of the service. Redirecting to appropriate page with generated information.
     *
     * @param request  provided by Controller
     * @param response provided by Controller
     * @return address to redirect or forward by front-controller
     */
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws IOException {
        int id = Validator.validateInt(request.getParameter("id"));
        Map<String, String> info = new HashMap<>();

        if (service.deleteBook(id)) {
            info.put("infoText", "books.info_successful_delete");
        }
        else {
            info.put("errorText", "books.error_unsuccessful_delete");
            logger.error("Unsuccessful delete in DeleteBookCommand - execute()");
        }
        response.sendRedirect(Path.COMMAND_BOOK_SEARCH + AddressBuilder.formCommandProperties(null, info)
                + "&searchRequest=&orderBy=id&desc=false&pageNumber=1");
        return Path.REDIRECT;
    }
}
