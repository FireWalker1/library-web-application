package com.ilin.library.controller.commands.administrator;

import com.ilin.library.controller.Path;
import com.ilin.library.controller.commands.Command;
import com.ilin.library.dto.BookDTO;
import com.ilin.library.services.BookService;
import com.ilin.library.services.implementation.BookServiceImpl;
import com.ilin.library.utils.AddressBuilder;
import com.ilin.library.utils.Validator;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * ChangeBookFieldCommand class. Restricted access for administrator role only. Implements PRG pattern.
 * After verification of all input information calls service method to change indicated field of existing book.
 *
 * @author Pavlo Ilin
 * @version 1.0
 */
public class ChangeBookFieldCommand implements Command {
    private final BookService service;

    /**
     * Constructor to use in all cases except testing
     */
    public ChangeBookFieldCommand() {
        service = new BookServiceImpl();
    }

    /**
     * Constructor to use during testing
     *
     * @param service is mocked service
     */
    public ChangeBookFieldCommand(BookService service) {
        this.service = service;
    }


    /**
     * Gets all parameters from request. Calls validator to check parameters and generating errors (if necessary).
     * Calls appropriate methods of the service. Redirecting to appropriate page with generated information.
     *
     * @param request  provided by Controller
     * @param response provided by Controller
     * @return address to redirect or forward by front-controller
     */
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String fieldName = request.getParameter("fieldName");
        String fieldValue = request.getParameter("fieldValue").trim();
        int id = Validator.validateInt(request.getParameter("id"));
        String errorText = Validator.checkBookFieldValue(fieldName, fieldValue);
        Map<String, String> info = new HashMap<>();

        if (errorText != null) {
            info.put("errorText", errorText);
            return makeRedirect(request, response, info);
        }
        BookDTO book = service.getByID(id);
        if (!fieldName.equals("quantity") && service.isSuchBookExists(book, fieldName, fieldValue)) {
            info.put("errorText", "add_new_book.error_already_exists");
            return makeRedirect(request, response, info);
        }
        if (service.updateBook(id, fieldName, fieldValue)) {
            info.put("infoText", "edit_book.info_successful_update");
            return makeRedirect(request, response, info);
        } else {
            return AddressBuilder.logAndRedirectToErrorPage("Error redirect from ChangeBookFieldCommand", response);
        }

    }

    private String makeRedirect(HttpServletRequest request, HttpServletResponse response, Map<String, String> info)
            throws IOException {
        info.put("id", request.getParameter("id"));
        response.sendRedirect(Path.COMMAND_GET_BOOK + AddressBuilder.formCommandProperties(request.getQueryString(), info));
        return Path.REDIRECT;
    }
}
