package com.ilin.library.controller.commands.reader;

import com.ilin.library.controller.Path;
import com.ilin.library.controller.commands.Command;
import com.ilin.library.dto.BookDTO;
import com.ilin.library.services.BookService;
import com.ilin.library.services.implementation.BookServiceImpl;
import com.ilin.library.utils.AddressBuilder;
import com.ilin.library.utils.Validator;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * DeleteBookFromCartCommand class. Restricted access for reader role only. Implements PRG pattern.
 * Calls service method to delete book from reader's cart.
 *
 * @author Pavlo Ilin
 * @version 1.0
 */
public class DeleteBookFromCartCommand implements Command {
    private final BookService service;

    /**
     * Constructor to use in all cases except testing
     */
    public DeleteBookFromCartCommand() {
        service = new BookServiceImpl();
    }

    /**
     * Constructor to use during testing
     *
     * @param service is mocked service
     */
    public DeleteBookFromCartCommand(BookService service) {
        this.service = service;
    }

    /**
     * Gets all parameters from request.
     * Calls appropriate methods of the service. Redirecting to appropriate page.
     *
     * @param request  provided by Controller
     * @param response provided by Controller
     * @return address to redirect or forward by front-controller
     */
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws IOException {
        int bookID = Validator.validateInt(request.getParameter("id"));
        List<BookDTO> bookCart = Validator.validateBookListAttribute(request.getSession(), "bookCart");
        Map<String, String> info = new HashMap<>();
        BookDTO book = bookCart.stream()
                .filter(x -> x.getId() == bookID)
                .findFirst()
                .orElse(new BookDTO());

        if (service.deleteBookFromCart(book) && bookCart.remove(book)) {
            info.put("infoText", "cart.info_success");
        } else {
            info.put("errorText", "cart.error_incorrect_id");
        }
        response.sendRedirect(Path.PAGE_CART + AddressBuilder.formPageProperties(request.getQueryString(), info));
        return Path.REDIRECT;
    }
}
