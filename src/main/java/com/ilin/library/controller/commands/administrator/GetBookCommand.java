package com.ilin.library.controller.commands.administrator;

import com.ilin.library.controller.Path;
import com.ilin.library.controller.commands.Command;
import com.ilin.library.dto.BookDTO;
import com.ilin.library.services.BookService;
import com.ilin.library.services.implementation.BookServiceImpl;
import com.ilin.library.utils.AddressBuilder;
import com.ilin.library.utils.Validator;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;

/**
 * GetBookCommand class. Restricted access for administrator role only.
 * Calls service method to get all information about certain book from the database.
 *
 * @author Pavlo Ilin
 * @version 1.0
 */
public class GetBookCommand implements Command {
    private final BookService service;

    /**
     * Constructor to use in all cases except testing
     */
    public GetBookCommand() {
        service = new BookServiceImpl();
    }

    /**
     * Constructor to use during testing
     *
     * @param service is mocked service
     */
    public GetBookCommand(BookService service) {
        this.service = service;
    }

    /**
     * Gets all parameters from request.
     * Calls appropriate methods of the service. Forwarding to appropriate page with fetched information.
     *
     * @param request  provided by Controller
     * @param response provided by Controller
     * @return address to redirect or forward by front-controller
     */
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws IOException {
        int id = Validator.validateInt(request.getParameter("id"));

        BookDTO book = service.getByID(id);
        if (book == null) {
            return AddressBuilder.logAndRedirectToErrorPage("Error redirect from GetBookCommand", response);
        }
        request.setAttribute("book", book);
        return Path.PAGE_EDIT_BOOK;
    }
}
