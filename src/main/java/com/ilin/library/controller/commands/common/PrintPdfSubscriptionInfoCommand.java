package com.ilin.library.controller.commands.common;

import com.ilin.library.controller.Path;
import com.ilin.library.controller.commands.Command;
import com.ilin.library.dto.SubscriptionDTO;
import com.ilin.library.services.SubscriptionService;
import com.ilin.library.services.implementation.SubscriptionServiceImpl;
import com.ilin.library.utils.AddressBuilder;
import com.ilin.library.utils.PdfPrinter;
import com.ilin.library.utils.Validator;
import com.itextpdf.text.DocumentException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;

/**
 * PrintPdfSubscriptionCommand class. Restricted access for any signed in role. Implements PRG pattern.
 * Calls util method to create a pdf file with all information fetched from the database.
 *
 * @author Pavlo Ilin
 * @version 1.0
 */
public class PrintPdfSubscriptionInfoCommand implements Command {
    private final SubscriptionService service;

    /**
     * Constructor to use in all cases except testing
     */
    public PrintPdfSubscriptionInfoCommand() {
        service = new SubscriptionServiceImpl();
    }

    /**
     * Constructor to use during testing
     *
     * @param service is mocked service
     */
    public PrintPdfSubscriptionInfoCommand(SubscriptionService service) {
        this.service = service;
    }

    /**
     * Gets all parameters from request.
     * Calls appropriate methods of the service. Redirecting to appropriate page with generated pdf file.
     *
     * @param request  provided by Controller
     * @param response provided by Controller
     * @return address to redirect or forward by front-controller
     */
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws IOException {
        int subscriptionID = Validator.validateInt(request.getParameter("id"));
        String lang = (String) request.getSession().getAttribute("language");

        SubscriptionDTO subscription = service.getSubscriptionByID(subscriptionID);
        if (subscription == null || subscription.getBooks().size() == 0) {
            return AddressBuilder
                    .logAndRedirectToErrorPage("Error redirect from PrintPdfSubscriptionInfoCommand", response);
        }
        try {
            ByteArrayOutputStream output = PdfPrinter.printSubscriptionInfo(subscription, lang);
            setResponse(response, output);
        } catch (DocumentException e) {
            throw new RuntimeException(e);
        }
        return Path.REDIRECT;
    }

    private void setResponse(HttpServletResponse response, ByteArrayOutputStream output) throws IOException {
        response.setContentType("application/pdf");
        response.setContentLength(output.size());
        response.setHeader("Content-Disposition", "attachment; filename=\"Subscription info.pdf\"");
        OutputStream outputStream = response.getOutputStream();
        output.writeTo(outputStream);
        outputStream.flush();
        outputStream.close();
    }
}
