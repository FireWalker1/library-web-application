package com.ilin.library.controller.commands.administrator;

import com.ilin.library.controller.Path;
import com.ilin.library.controller.commands.Command;
import com.ilin.library.dto.UserDTO;
import com.ilin.library.services.UserService;
import com.ilin.library.services.implementation.UserServiceImpl;
import com.ilin.library.utils.Validator;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.util.List;

/**
 * GetAllUsersCommand class. Restricted access for administrator role only.
 * Calls service method to get existing users from the database (according to pagination settings).
 *
 * @author Pavlo Ilin
 * @version 1.0
 */
public class GetAllUsersCommand implements Command {
    private final UserService service;

    /**
     * Constructor to use in all cases except testing
     */
    public GetAllUsersCommand() {
        service = new UserServiceImpl();
    }

    /**
     * Constructor to use during testing
     *
     * @param service is mocked service
     */
    public GetAllUsersCommand(UserService service) {
        this.service = service;
    }

    /**
     * Gets all parameters from request.
     * Calls appropriate methods of the service. Forwarding to appropriate page with fetched information.
     *
     * @param request  provided by Controller
     * @param response provided by Controller
     * @return address to redirect or forward by front-controller
     */
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        String orderBy = request.getParameter("orderBy");
        String desc = request.getParameter("desc");
        int pageNumber = Validator.validateInt(request.getParameter("pageNumber"));

        List<UserDTO> users = service.getFullList(orderBy, desc, pageNumber);
        int pageQuantity = service.getPageQuantityUsers();
        request.setAttribute("users", users);
        request.setAttribute("pageQuantity", pageQuantity);
        return Path.PAGE_USERS;
    }
}
