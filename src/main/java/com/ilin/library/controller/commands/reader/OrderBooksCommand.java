package com.ilin.library.controller.commands.reader;

import com.ilin.library.controller.Path;
import com.ilin.library.controller.commands.Command;
import com.ilin.library.dto.BookDTO;
import com.ilin.library.dto.SubscriptionDTO;
import com.ilin.library.services.SubscriptionService;
import com.ilin.library.services.implementation.SubscriptionServiceImpl;
import com.ilin.library.utils.AddressBuilder;
import com.ilin.library.utils.Validator;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * OrderBooksCommand class. Restricted access for reader role only. Implements PRG pattern.
 * Calls service method to order books from reader's cart.
 *
 * @author Pavlo Ilin
 * @version 1.0
 */
public class OrderBooksCommand implements Command {
    private final SubscriptionService service;

    /**
     * Constructor to use in all cases except testing
     */
    public OrderBooksCommand() {
        service = new SubscriptionServiceImpl();
    }

    /**
     * Constructor to use during testing
     *
     * @param service is mocked service
     */
    public OrderBooksCommand(SubscriptionService service) {
        this.service = service;
    }

    /**
     * Gets all parameters from request.
     * Calls appropriate methods of the service. Redirecting to appropriate page.
     *
     * @param request  provided by Controller
     * @param response provided by Controller
     * @return address to redirect or forward by front-controller
     */
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws IOException {
        HttpSession session = request.getSession();
        List<BookDTO> bookCart = Validator.validateBookListAttribute(session, "bookCart");
        String userLogin = (String) session.getAttribute("login");
        String type = request.getParameter("type");
        Map<String, String> info = new HashMap<>();

        if (bookCart.size() == 0) {
            return redirectError(info, "cart.error_no_books", response, request);
        }
        if (bookCart.size() > 5) {
            return redirectError(info, "cart.error_too_much_books", response, request);
        }
        if (service.getUserFines(userLogin) > 0) {
            return redirectError(info, "cart.error_have_unpaid_fine", response, request);
        }

        SubscriptionDTO subscription = new SubscriptionDTO();
        subscription.setBooks(bookCart);
        subscription.setType(type);
        if (service.insertSubscription(subscription, userLogin)) {
            session.setAttribute("bookCart", new ArrayList<BookDTO>());
            response.sendRedirect(Path.PAGE_SUCCESS);
            return Path.REDIRECT;
        } else {
            return AddressBuilder.logAndRedirectToErrorPage("Error redirect from OrderBooksCommand", response);
        }
    }

    private String redirectError(Map<String, String> info, String errorText, HttpServletResponse response,
                                 HttpServletRequest request) throws IOException {
        info.put("errorText", errorText);
        response.sendRedirect(Path.PAGE_CART + AddressBuilder.formPageProperties(request.getQueryString(), info));
        return Path.REDIRECT;
    }
}
