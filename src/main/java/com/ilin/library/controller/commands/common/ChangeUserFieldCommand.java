package com.ilin.library.controller.commands.common;

import com.ilin.library.controller.Path;
import com.ilin.library.controller.commands.Command;
import com.ilin.library.services.UserService;
import com.ilin.library.services.implementation.UserServiceImpl;
import com.ilin.library.utils.AddressBuilder;
import com.ilin.library.utils.Validator;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * ChangeUserFieldCommand class. Restricted access for any signed in role. Implements PRG pattern.
 * After verification of all input information calls service method to change indicated field of existing user.
 *
 * @author Pavlo Ilin
 * @version 1.0
 */
public class ChangeUserFieldCommand implements Command {
    private final UserService service;

    /**
     * Constructor to use in all cases except testing
     */
    public ChangeUserFieldCommand() {
        this.service = new UserServiceImpl();
    }

    /**
     * Constructor to use during testing
     *
     * @param service is mocked service
     */
    public ChangeUserFieldCommand(UserService service) {
        this.service = service;
    }

    /**
     * Gets all parameters from request. Calls validator to check parameters and generating errors (if necessary).
     * Calls appropriate methods of the service. Redirecting to appropriate page with generated information.
     *
     * @param request  provided by Controller
     * @param response provided by Controller
     * @return address to redirect or forward by front-controller
     */
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws IOException {
        HttpSession session = request.getSession();
        String fieldName = request.getParameter("fieldName");
        String fieldValue = request.getParameter("fieldValue").trim();
        String userLogin = (String) session.getAttribute("login");
        String errorText = Validator.checkUserFieldValue(fieldName, fieldValue);
        Map<String, String> info = new HashMap<>();

        if (errorText != null) {
            info.put("errorText", errorText);
            return makeRedirect(request, response, info);
        }
        if (fieldName.equals("login") && service.isSuchLoginExists(fieldValue)) {
            info.put("errorText", "sign_up.error_login_already_exists");
            return makeRedirect(request, response, info);
        }
        if (fieldName.equals("email") && service.isSuchEmailExists(fieldValue)) {
            info.put("errorText", "sign_up.error_email_already_exists");
            return makeRedirect(request, response, info);
        }
        if (fieldName.equals("phoneNumber") && service.isSuchPhoneNumberExists(fieldValue)) {
            info.put("errorText", "sign_up.error_phone_number_already_exists");
            return makeRedirect(request, response, info);
        }

        if (service.updateUser(fieldValue, fieldName, userLogin)) {
            info.put("infoText", "profile.info_successful_update");
            if (fieldName.equals("login")) {
                session.setAttribute("login", fieldValue);
            }
            return makeRedirect(request, response, info);
        } else {
            return AddressBuilder.logAndRedirectToErrorPage("Error redirect from ChangeUserFieldCommand", response);
        }
    }

    private String makeRedirect(HttpServletRequest request, HttpServletResponse response, Map<String, String> info)
            throws IOException {
        response.sendRedirect(Path.COMMAND_GET_USER + AddressBuilder.formCommandProperties(request.getQueryString(), info));
        return Path.REDIRECT;
    }
}
