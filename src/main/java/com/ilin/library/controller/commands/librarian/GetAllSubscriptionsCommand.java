package com.ilin.library.controller.commands.librarian;

import com.ilin.library.controller.Path;
import com.ilin.library.controller.commands.Command;
import com.ilin.library.dto.SubscriptionDTO;
import com.ilin.library.services.SubscriptionService;
import com.ilin.library.services.implementation.SubscriptionServiceImpl;
import com.ilin.library.utils.Validator;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.util.List;

/**
 * GetAllSubscriptionsCommand class. Restricted access for librarian role only.
 * Calls service method to get existing subscriptions from the database (according to pagination settings).
 *
 * @author Pavlo Ilin
 * @version 1.0
 */
public class GetAllSubscriptionsCommand implements Command {
    private final SubscriptionService service;

    /**
     * Constructor to use in all cases except testing
     */
    public GetAllSubscriptionsCommand() {
        service = new SubscriptionServiceImpl();
    }

    /**
     * Constructor to use during testing
     *
     * @param service is mocked service
     */
    public GetAllSubscriptionsCommand(SubscriptionService service) {
        this.service = service;
    }

    /**
     * Gets all parameters from request.
     * Calls appropriate methods of the service. Forwarding to appropriate page with fetched information.
     *
     * @param request  provided by Controller
     * @param response provided by Controller
     * @return address to redirect or forward by front-controller
     */
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        String orderBy = request.getParameter("orderBy");
        String desc = request.getParameter("desc");
        int pageNumber = Validator.validateInt(request.getParameter("pageNumber"));
        List<SubscriptionDTO> subscriptions;
        int pageQuantity;

        subscriptions = service.getFullList(orderBy, desc, pageNumber);
        pageQuantity = service.getPageQuantity();
        request.setAttribute("subscriptions", subscriptions);
        request.setAttribute("pageQuantity", pageQuantity);
        return Path.PAGE_ALL_SUBSCRIPTIONS;
    }
}
