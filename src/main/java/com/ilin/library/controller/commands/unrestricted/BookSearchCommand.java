package com.ilin.library.controller.commands.unrestricted;

import com.ilin.library.controller.Path;
import com.ilin.library.controller.commands.Command;
import com.ilin.library.dto.BookDTO;
import com.ilin.library.services.BookService;
import com.ilin.library.services.implementation.BookServiceImpl;
import com.ilin.library.utils.Validator;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.util.List;

/**
 * BookSearchCommand class. Unrestricted access.
 * Calls service method to get books from the database (according to search request and pagination settings).
 *
 * @author Pavlo Ilin
 * @version 1.0
 */
public class BookSearchCommand implements Command {
    private final BookService service;

    /**
     * Constructor to use in all cases except testing
     */
    public BookSearchCommand() {
        this.service = new BookServiceImpl();
    }

    /**
     * Constructor to use during testing
     *
     * @param service is mocked service
     */
    public BookSearchCommand(BookService service) {
        this.service = service;
    }

    /**
     * Gets all parameters from request.
     * Calls appropriate methods of the service. Forwarding to appropriate page with fetched information.
     *
     * @param request  provided by Controller
     * @param response provided by Controller
     * @return address to redirect or forward by front-controller
     */
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        String searchRequest = request.getParameter("searchRequest");
        String orderBy = request.getParameter("orderBy");
        String desc = request.getParameter("desc");
        int pageNumber = Validator.validateInt(request.getParameter("pageNumber"));
        List<BookDTO> books;
        int pageQuantity;

        if (searchRequest == null) {
            books = service.getFullList();
            pageQuantity = service.getPageQuantity();
        } else {
            String[] split = searchRequest.split("\\s+");
            books = service.getSearchResultList(split, orderBy, desc, pageNumber);
            pageQuantity = service.getPageQuantity(split);
        }
        request.setAttribute("books", books);
        request.setAttribute("pageQuantity", pageQuantity);
        return Path.PAGE_BOOKS;
    }
}
