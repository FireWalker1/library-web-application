package com.ilin.library.controller.commands;

import com.ilin.library.controller.commands.administrator.*;
import com.ilin.library.controller.commands.common.*;
import com.ilin.library.controller.commands.librarian.*;
import com.ilin.library.controller.commands.reader.*;
import com.ilin.library.controller.commands.unrestricted.BookSearchCommand;
import com.ilin.library.controller.commands.unrestricted.ChangeLanguageCommand;
import com.ilin.library.controller.commands.unrestricted.LoginCommand;
import com.ilin.library.controller.commands.unrestricted.SignUpCommand;
import jakarta.servlet.http.HttpServletRequest;

import java.util.HashMap;
import java.util.Map;

/**
 * CommandFactory class. Contains all available commands. Implements Factory pattern.
 * All existing command are placed to the Map commands. While adding new command, it should be placed here as well.
 *
 * @author Pavlo Ilin
 * @version 1.0
 */
public class CommandFactory {
    private static CommandFactory factory = new CommandFactory();
    private static final Map<String, Command> commands = new HashMap<>();

    private CommandFactory() {
    }

    /**
     * Implements Singleton pattern.
     *
     * @return CommandFactory instance
     */
    public static CommandFactory commandFactory() {
        if (factory == null) {
            factory = new CommandFactory();
        }
        return factory;
    }

    static {
        // unrestricted commands

        commands.put("bookSearch", new BookSearchCommand());
        commands.put("login", new LoginCommand());
        commands.put("signUp", new SignUpCommand());
        commands.put("changeLanguage", new ChangeLanguageCommand());

        // common commands

        commands.put("logout", new LogoutCommand());
        commands.put("getUser", new GetUserCommand());
        commands.put("changeUserField", new ChangeUserFieldCommand());
        commands.put("getSubscriptionInfo", new GetSubscriptionInfoCommand());
        commands.put("printPdfSubscriptionInfo", new PrintPdfSubscriptionInfoCommand());

        //reader commands

        commands.put("addBookToCart", new AddBookToCartCommand());
        commands.put("orderBooks", new OrderBooksCommand());
        commands.put("deleteBookFromCart", new DeleteBookFromCartCommand());
        commands.put("getMySubscriptions", new GetMySubscriptionsCommand());
        commands.put("payFine", new PayFineCommand());

        // librarian commands

        commands.put("getAllSubscriptions", new GetAllSubscriptionsCommand());
        commands.put("confirmSubscription", new ConfirmSubscriptionCommand());
        commands.put("rejectSubscription", new RejectSubscriptionCommand());
        commands.put("getAllReaders", new GetAllReadersCommand());
        commands.put("getReaderSubscriptions", new GetReaderSubscriptionsCommand());

        // administrator commands

        commands.put("addNewBook", new AddNewBookCommand());
        commands.put("getBook", new GetBookCommand());
        commands.put("changeBookField", new ChangeBookFieldCommand());
        commands.put("deleteBook", new DeleteBookCommand());
        commands.put("getAllUsers", new GetAllUsersCommand());
        commands.put("deleteLibrarian", new DeleteLibrarianCommand());
        commands.put("changeUserBlockStatus", new ChangeUserBlockStatusCommand());
        commands.put("reassignAuthor", new ReassignAuthorCommand());
    }

    /**
     * Obtains action by its name
     *
     * @param request - provided by Controller
     * @return required Command implementation or null if action is absent
     */
    public Command getCommand(HttpServletRequest request) {
        String action = request.getParameter("action");
        if (action == null) {
            return null;
        }
        return commands.get(action);
    }
}
