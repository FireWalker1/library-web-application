package com.ilin.library.controller.commands.reader;

import com.ilin.library.controller.Path;
import com.ilin.library.controller.commands.Command;
import com.ilin.library.services.SubscriptionService;
import com.ilin.library.services.implementation.SubscriptionServiceImpl;
import com.ilin.library.utils.AddressBuilder;
import com.ilin.library.utils.Validator;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * PayFineCommand class. Restricted access for reader role only. Implements PRG pattern.
 * Calls service method to pay fine from the certain subscription.
 *
 * @author Pavlo Ilin
 * @version 1.0
 */
public class PayFineCommand implements Command {
    private final SubscriptionService service;

    /**
     * Constructor to use in all cases except testing
     */
    public PayFineCommand() {
        service = new SubscriptionServiceImpl();
    }

    /**
     * Constructor to use during testing
     *
     * @param service is mocked service
     */
    public PayFineCommand(SubscriptionService service) {
        this.service = service;
    }

    /**
     * Gets all parameters from request.
     * Calls appropriate methods of the service. Redirecting to appropriate page.
     *
     * @param request  provided by Controller
     * @param response provided by Controller
     * @return address to redirect or forward by front-controller
     */
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws IOException {
        int subscriptionID = Validator.validateInt(request.getParameter("id"));
        Map<String, String> info = new HashMap<>();

        if (service.payFine(subscriptionID)) {
            info.put("infoText", "subscription_info.info_success_pay_fine");
            response.sendRedirect(Path.COMMAND_GET_SUBSCRIPTION_INFO +
                    AddressBuilder.formCommandProperties(request.getQueryString(), info));
            return Path.REDIRECT;
        }
        return AddressBuilder.logAndRedirectToErrorPage("Error redirect from PayFineCommand", response);
    }
}
