package com.ilin.library.controller.commands.reader;

import com.ilin.library.controller.Path;
import com.ilin.library.controller.commands.Command;
import com.ilin.library.dto.SubscriptionDTO;
import com.ilin.library.services.SubscriptionService;
import com.ilin.library.services.implementation.SubscriptionServiceImpl;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.util.List;

/**
 * GetMySubscriptionsCommand class. Restricted access for reader role only.
 * Calls service method to get all subscriptions of the current reader from the database.
 *
 * @author Pavlo Ilin
 * @version 1.0
 */
public class GetMySubscriptionsCommand implements Command {
    private final SubscriptionService service;

    /**
     * Constructor to use in all cases except testing
     */
    public GetMySubscriptionsCommand() {
        service = new SubscriptionServiceImpl();
    }

    /**
     * Constructor to use during testing
     *
     * @param service is mocked service
     */
    public GetMySubscriptionsCommand(SubscriptionService service) {
        this.service = service;
    }

    /**
     * Gets all parameters from request.
     * Calls appropriate methods of the service. Forwarding to appropriate page.
     *
     * @param request  provided by Controller
     * @param response provided by Controller
     * @return address to redirect or forward by front-controller
     */
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        String userLogin = (String) request.getSession().getAttribute("login");

        List<SubscriptionDTO> subscriptions = service.getUserSubscriptions(userLogin);
        if (subscriptions.size() == 0) {
            request.setAttribute("errorText", "my_subscriptions.my_error_empty");
            return Path.PAGE_MY_SUBSCRIPTIONS;
        }
        request.setAttribute("subscriptions", subscriptions);
        return Path.PAGE_MY_SUBSCRIPTIONS;
    }
}
