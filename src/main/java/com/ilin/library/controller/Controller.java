package com.ilin.library.controller;

import com.ilin.library.controller.commands.Command;
import com.ilin.library.controller.commands.CommandFactory;
import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;

/**
 * Controller class. Implements Front-controller pattern. Chooses command to execute and redirect or forward result.
 *
 * @author Pavlo Ilin
 * @version 1.0
 */
@WebServlet("/controller")
public class Controller extends HttpServlet {

    private static final Logger logger = LogManager.getLogger(Controller.class);

    /**
     * Processing Get requests
     *
     * @param req  comes from user
     * @param resp comes from user
     */
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        processRequest(req, resp);
    }

    /**
     * Processing Post requests
     *
     * @param req  comes from user
     * @param resp comes from user
     */
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        processRequest(req, resp);
    }

    /**
     * Calls and executes indicated in the request command. Uses CommandFactory to create command.
     * In case of error will return error page
     *
     * @param req  comes from user
     * @param resp comes from user
     */
    private void processRequest(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        CommandFactory commandFactory = CommandFactory.commandFactory();
        Command command = commandFactory.getCommand(req);
        try {
            String page = command.execute(req, resp);
            RequestDispatcher dispatcher = req.getRequestDispatcher(page);
            if (!page.equals(Path.REDIRECT)) {
                dispatcher.forward(req, resp);
            }
            logger.info("Executing command " + command.getClass().getName());
        } catch (Exception e) {
            logger.error("Error catch in controller", e);
            resp.sendRedirect(Path.PAGE_ERROR);
        }
    }

}
