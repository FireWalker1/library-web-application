package com.ilin.library.controller.tags;

import jakarta.servlet.jsp.JspWriter;
import jakarta.servlet.jsp.tagext.SimpleTagSupport;

import java.io.IOException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;

/**
 * CurrentDateTag class. Describing custom tag for printing out current date of several jsp pages.
 *
 * @author Pavlo Ilin
 * @version 1.0
 */
public class CurrentDateTag extends SimpleTagSupport {

    /**
     * Printing current date
     */
    @Override
    public void doTag() throws IOException {
        final JspWriter writer = getJspContext().getOut();
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        writer.print(sdf.format(timestamp));
    }
}
