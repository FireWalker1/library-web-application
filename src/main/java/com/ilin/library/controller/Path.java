package com.ilin.library.controller;

/**
 * Path class. Contains path's of all pages and commands as constant Strings for easier use.
 *
 * @author Pavlo Ilin
 * @version 1.0
 */
public class Path {


    public static final String PAGE_404 = "404.jsp";
    public static final String PAGE_ACCESS_DENIED = "access_denied.jsp";
    public static final String PAGE_ADD_NEW_BOOK = "add_new_book.jsp";
    public static final String PAGE_ALL_SUBSCRIPTIONS = "all_subscriptions.jsp";
    public static final String PAGE_BOOKS = "books.jsp";
    public static final String PAGE_CART = "cart.jsp";
    public static final String PAGE_CONTACTS = "contacts.jsp";
    public static final String PAGE_EDIT_BOOK = "edit_book.jsp";
    public static final String PAGE_ERROR = "error.jsp";
    public static final String PAGE_INDEX = "index.jsp";
    public static final String PAGE_LOGIN = "login.jsp";
    public static final String PAGE_MY_SUBSCRIPTIONS = "my_subscriptions.jsp";
    public static final String PAGE_PROFILE = "profile.jsp";
    public static final String PAGE_SIGN_UP = "sign_up.jsp";
    public static final String PAGE_SUBSCRIPTION_INFO = "subscription_info.jsp";
    public static final String PAGE_SUCCESS = "success.jsp";
    public static final String PAGE_USERS = "users.jsp";


    public static final String COMMAND_REASSIGN_AUTHOR = "controller?action=reassignAuthor";
    public static final String COMMAND_ADD_NEW_BOOK = "controller?action=addNewBook";
    public static final String COMMAND_CHANGE_BOOK_FIELD = "controller?action=changeBookField";
    public static final String COMMAND_CHANGE_USER_BLOCK_STATUS = "controller?action=changeUserBlockStatus";
    public static final String COMMAND_DELETE_BOOK = "controller?action=deleteBook";
    public static final String COMMAND_DELETE_LIBRARIAN = "controller?action=deleteLibrarian";
    public static final String COMMAND_GET_ALL_USERS = "controller?action=getAllUsers";
    public static final String COMMAND_GET_BOOK = "controller?action=getBook";
    public static final String COMMAND_CHANGE_USER_FIELD = "controller?action=changeUserField";
    public static final String COMMAND_GET_SUBSCRIPTION_INFO = "controller?action=getSubscriptionInfo";
    public static final String COMMAND_GET_USER = "controller?action=getUser";
    public static final String COMMAND_LOGOUT = "controller?action=logout";
    public static final String COMMAND_CONFIRM_SUBSCRIPTION = "controller?action=confirmSubscription";
    public static final String COMMAND_GET_ALL_READERS = "controller?action=getAllReaders";
    public static final String COMMAND_GET_ALL_SUBSCRIPTIONS = "controller?action=getAllSubscriptions";
    public static final String COMMAND_GET_READER_SUBSCRIPTIONS = "controller?action=getReaderSubscriptions";
    public static final String COMMAND_REJECT_SUBSCRIPTION = "controller?action=rejectSubscription";
    public static final String COMMAND_ADD_BOOK_TO_CART = "controller?action=addBookToCart";
    public static final String COMMAND_DELETE_BOOK_FROM_CART = "controller?action=deleteBookFromCart";
    public static final String COMMAND_GET_MY_SUBSCRIPTIONS = "controller?action=getMySubscriptions";
    public static final String COMMAND_ORDER_BOOKS = "controller?action=orderBooks";
    public static final String COMMAND_PAY_FINE = "controller?action=payFine";
    public static final String COMMAND_PRINT_PDF_SUBSCRIPTION_INFO = "controller?action=printPdfSubscriptionInfo";
    public static final String COMMAND_BOOK_SEARCH = "controller?action=bookSearch";
    public static final String COMMAND_CHANGE_LANGUAGE = "controller?action=changeLanguage";
    public static final String COMMAND_LOGIN = "controller?action=login";
    public static final String COMMAND_SIGN_UP = "controller?action=signUp";

    public static final String REDIRECT = "redirect";
}
