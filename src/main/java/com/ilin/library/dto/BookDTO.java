package com.ilin.library.dto;

import java.util.Objects;

/**
 * BookDTO class. Fields are similar to the entity class, except language which is String.
 * Standard constructors, getters and setters are used.
 *
 * @author Pavlo Ilin
 * @version 1.0
 */
public class BookDTO {
    private int id;
    private String title;
    private AuthorDTO author;
    private String publisherHouse;
    private int year;
    private String language;
    private int quantity;

    public BookDTO(int id, String title, AuthorDTO author, String publisherHouse, int year, String language, int quantity) {
        this.id = id;
        this.title = title;
        this.author = author;
        this.publisherHouse = publisherHouse;
        this.year = year;
        this.language = language;
        this.quantity = quantity;
    }

    public BookDTO() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public AuthorDTO getAuthor() {
        return author;
    }

    public void setAuthor(AuthorDTO author) {
        this.author = author;
    }

    public String getPublisherHouse() {
        return publisherHouse;
    }

    public void setPublisherHouse(String publisherHouse) {
        this.publisherHouse = publisherHouse;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BookDTO book = (BookDTO) o;
        return year == book.year && Objects.equals(title, book.title) && Objects.equals(author, book.author) &&
                Objects.equals(publisherHouse, book.publisherHouse) && Objects.equals(language, book.language);
    }

    @Override
    public int hashCode() {
        return Objects.hash(title, author, publisherHouse, year, language);
    }

    @Override
    public String toString() {
        return "Book{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", author=" + author +
                ", publisherHouse='" + publisherHouse + '\'' +
                ", year=" + year +
                ", language=" + language +
                ", quantity=" + quantity +
                '}';
    }
}
