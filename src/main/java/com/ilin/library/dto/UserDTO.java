package com.ilin.library.dto;

import java.sql.Timestamp;
import java.util.Objects;

/**
 * UserDTO class. Fields are similar to the entity class, except role which is String.
 * Standard constructors, getters and setters are used.
 *
 * @author Pavlo Ilin
 * @version 1.0
 */
public class UserDTO {
    private int id;
    private String login;
    private String email;
    private String password;
    private String phoneNumber;
    private String role;
    private String firstName;
    private String lastName;
    private Timestamp blockedAt;

    public UserDTO(int id, String login, String email, String password, String phoneNumber, String role, String firstName,
                   String lastName, Timestamp blockedAt) {
        this.id = id;
        this.login = login;
        this.email = email;
        this.password = password;
        this.phoneNumber = phoneNumber;
        this.role = role;
        this.firstName = firstName;
        this.lastName = lastName;
        this.blockedAt = blockedAt;
    }

    public UserDTO() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Timestamp getBlockedAt() {
        return blockedAt;
    }

    public void setBlockedAt(Timestamp blockedAt) {
        this.blockedAt = blockedAt;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserDTO user = (UserDTO) o;
        return Objects.equals(login, user.login) && Objects.equals(email, user.email) &&
                Objects.equals(password, user.password) && Objects.equals(phoneNumber, user.phoneNumber) &&
                Objects.equals(role, user.role) && Objects.equals(firstName, user.firstName) &&
                Objects.equals(lastName, user.lastName) && Objects.equals(blockedAt, user.blockedAt);
    }

    @Override
    public int hashCode() {
        return Objects.hash(login, email, password, phoneNumber, role, firstName, lastName, blockedAt);
    }
}
