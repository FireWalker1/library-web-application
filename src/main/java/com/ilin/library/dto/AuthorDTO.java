package com.ilin.library.dto;

import java.util.Objects;

/**
 * AuthorDTO class. Fields are similar to the entity class.
 * Standard constructors, getters and setters are used.
 *
 * @author Pavlo Ilin
 * @version 1.0
 */
public class AuthorDTO {
    private int id;
    private String nameEN;
    private String nameUA;

    public AuthorDTO(int id, String nameEN, String nameUA) {
        this.id = id;
        this.nameEN = nameEN;
        this.nameUA = nameUA;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNameEN() {
        return nameEN;
    }

    public void setNameEN(String nameEN) {
        this.nameEN = nameEN;
    }

    public String getNameUA() {
        return nameUA;
    }

    public void setNameUA(String nameUA) {
        this.nameUA = nameUA;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AuthorDTO author = (AuthorDTO) o;
        return Objects.equals(nameEN, author.nameEN) && Objects.equals(nameUA, author.nameUA);
    }

    @Override
    public int hashCode() {
        return Objects.hash(nameEN, nameUA);
    }

    @Override
    public String toString() {
        return "Author{" +
                "id=" + id +
                ", nameEN='" + nameEN + '\'' +
                ", nameUA='" + nameUA + '\'' +
                '}';
    }
}
