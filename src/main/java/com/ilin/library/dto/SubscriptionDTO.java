package com.ilin.library.dto;

import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;
import java.util.Objects;

/**
 * SubscriptionDTO class. Fields are similar to the entity class, except type and status which is String.
 * Standard constructors, getters and setters are used.
 *
 * @author Pavlo Ilin
 * @version 1.0
 */
public class SubscriptionDTO {
    private int id;
    private UserDTO user;
    private String type;
    private String status;
    private Timestamp createdAt;
    private Timestamp updatedAt;
    private Date bookReturnDeadline;
    private BigDecimal fine;
    private List<BookDTO> books;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public UserDTO getUser() {
        return user;
    }

    public void setUser(UserDTO user) {
        this.user = user;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Timestamp getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Timestamp createdAt) {
        this.createdAt = createdAt;
    }

    public Timestamp getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Timestamp updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Date getBookReturnDeadline() {
        return bookReturnDeadline;
    }

    public void setBookReturnDeadline(Date bookReturnDeadline) {
        this.bookReturnDeadline = bookReturnDeadline;
    }

    public BigDecimal getFine() {
        return fine;
    }

    public void setFine(BigDecimal fine) {
        this.fine = fine;
    }

    public List<BookDTO> getBooks() {
        return books;
    }

    public void setBooks(List<BookDTO> books) {
        this.books = books;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SubscriptionDTO that = (SubscriptionDTO) o;
        return Objects.equals(user, that.user) && Objects.equals(type, that.type) &&
                Objects.equals(status, that.status) && Objects.equals(createdAt, that.createdAt) &&
                Objects.equals(updatedAt, that.updatedAt) &&
                Objects.equals(bookReturnDeadline, that.bookReturnDeadline) && Objects.equals(fine, that.fine) &&
                Objects.equals(books, that.books);
    }

    @Override
    public int hashCode() {
        return Objects.hash(user, type, status, createdAt, updatedAt, bookReturnDeadline, fine, books);
    }
}
