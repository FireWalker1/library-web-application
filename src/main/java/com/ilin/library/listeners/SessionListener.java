package com.ilin.library.listeners;


import com.ilin.library.dto.BookDTO;
import com.ilin.library.services.implementation.BookServiceImpl;
import com.ilin.library.utils.Validator;
import jakarta.servlet.annotation.WebListener;
import jakarta.servlet.http.HttpSessionEvent;
import jakarta.servlet.http.HttpSessionListener;

import java.util.List;

/**
 * SessionListener class. Assisting BookService to clear cart with books in case user didn't finalize confirmation
 * of the order process.
 *
 * @author Pavlo Ilin
 * @version 1.0
 */
@WebListener
public class SessionListener implements HttpSessionListener {

    /**
     * Deleting all books from the book cart of user, if the session is closed
     *
     * @param se passed by application
     */
    @Override
    public void sessionDestroyed(HttpSessionEvent se) {
        List<BookDTO> books = Validator.validateBookListAttribute(se.getSession(), "bookCart");
        BookServiceImpl service = new BookServiceImpl();
        if (books.size() > 0) {
            books.forEach(service::deleteBookFromCart);
        }
    }
}
