package com.ilin.library.listeners;

import com.ilin.library.services.FineUpdater;
import jakarta.servlet.ServletContextEvent;
import jakarta.servlet.ServletContextListener;
import jakarta.servlet.annotation.WebListener;

import java.time.Duration;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * BackgroundJobManager class. Starting FineService's run method once a day at 1:00 AM automatically.
 *
 * @author Pavlo Ilin
 * @version 1.0
 */
@WebListener
public class BackgroundJobManager implements ServletContextListener {

    private ScheduledExecutorService scheduler;

    /**
     * Starting method in the indicating class according to the specified time schedule
     *
     * @param sce passed by application
     */
    @Override
    public void contextInitialized(ServletContextEvent sce) {
        ZonedDateTime now = ZonedDateTime.now(ZoneId.of("Africa/Cairo"));
        ZonedDateTime nextRun = now.withHour(1).withMinute(0).withSecond(0);
        if (now.compareTo(nextRun) > 0) {
            nextRun = nextRun.plusDays(1);
        }

        Duration duration = Duration.between(now, nextRun);
        long initialDelay = duration.getSeconds();

        scheduler = Executors.newSingleThreadScheduledExecutor();
        scheduler.scheduleAtFixedRate(new FineUpdater(), initialDelay, TimeUnit.DAYS.toSeconds(1), TimeUnit.SECONDS);
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        scheduler.shutdownNow();
    }
}
