package com.ilin.library.dao.executors;

import com.ilin.library.entities.Author;
import com.ilin.library.entities.Book;
import com.ilin.library.entities.Subscription;
import com.ilin.library.entities.User;
import com.ilin.library.entities.enums.Language;
import com.ilin.library.entities.enums.Role;
import com.ilin.library.entities.enums.Status;
import com.ilin.library.entities.enums.Type;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * EntityMapper class. Helps Executors to map entities correctly.
 *
 * @author Pavlo Ilin
 * @version 1.0
 */
public class EntityMapper {

    /**
     * Mapping provided Book entity with data from the Result Set
     *
     * @param resultSet Result Set from the database
     * @param book      empty Book entity for filling
     */
    static void mappingBook(ResultSet resultSet, Book book) throws SQLException {
        book.setId(resultSet.getInt("book_id"));
        book.setTitle(resultSet.getString("title"));
        Author author = new Author();
        author.setId(resultSet.getInt("author_id"));
        author.setNameUA(resultSet.getString("name_UA"));
        author.setNameEN(resultSet.getString("name_EN"));
        book.setAuthor(author);
        book.setPublisherHouse(resultSet.getString("publisher_house"));
        book.setYear(resultSet.getInt("year"));
        if (resultSet.getString("language").equals("UA")) {
            book.setLanguage(Language.UA);
        } else {
            book.setLanguage(Language.EN);
        }
        book.setQuantity(resultSet.getInt("quantity"));
    }

    /**
     * Mapping provided User entity with data from the Result Set
     *
     * @param resultSet Result Set from the database
     * @param user      empty User entity for filling
     */
    static void mappingUser(ResultSet resultSet, User user) throws SQLException {
        user.setId(resultSet.getInt("user_id"));
        user.setLogin(resultSet.getString("login"));
        user.setEmail(resultSet.getString("email"));
        user.setPassword(resultSet.getString("password"));
        user.setPhoneNumber(resultSet.getString("phone_number"));
        if (resultSet.getString("role").equals("reader")) {
            user.setRole(Role.READER);
        } else if (resultSet.getString("role").equals("librarian")) {
            user.setRole(Role.LIBRARIAN);
        } else {
            user.setRole(Role.ADMINISTRATOR);
        }
        user.setFirstName(resultSet.getString("first_name"));
        user.setLastName(resultSet.getString("last_name"));
        user.setBlockedAt(resultSet.getTimestamp("blocked_at"));
    }

    /**
     * Mapping provided Subscription entity with data from the Result Set
     *
     * @param resultSet    Result Set from the database
     * @param subscription empty Subscription entity for filling
     */
    static void mappingSubscription(ResultSet resultSet, Subscription subscription) throws SQLException {
        subscription.setId(resultSet.getInt("subscription_id"));
        User user = new User();
        mappingUser(resultSet, user);
        subscription.setUser(user);
        if (resultSet.getString("type").equals("reading room")) {
            subscription.setType(Type.READING_ROOM);
        } else {
            subscription.setType(Type.HOME);
        }
        if (resultSet.getString("status").equals("requested")) {
            subscription.setStatus(Status.REQUESTED);
        } else if (resultSet.getString("status").equals("confirmed")) {
            subscription.setStatus(Status.CONFIRMED);
        } else if (resultSet.getString("status").equals("ongoing")) {
            subscription.setStatus(Status.ONGOING);
        } else {
            subscription.setStatus(Status.CLOSED);
        }
        subscription.setCreatedAt(resultSet.getTimestamp("created_at"));
        subscription.setUpdatedAt(resultSet.getTimestamp("updated_at"));
        subscription.setBookReturnDeadline(resultSet.getDate("book_return_deadline"));
        subscription.setFine(resultSet.getBigDecimal("fine"));
    }
}
