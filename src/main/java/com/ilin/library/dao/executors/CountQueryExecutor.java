package com.ilin.library.dao.executors;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * CountQueryExecutor class. Extends QueryExecutor abstract class for Integer and "count" SQL command.
 * Overriding and implementing two abstract methods for proper work with Integer.
 *
 * @author Pavlo Ilin
 * @version 1.0
 */
public class CountQueryExecutor extends QueryExecutor<Integer> {

    /**
     * Fetches required information from Result Set. Creates and collects list of Integers.
     * Currently, not implemented because of lack of necessity.
     *
     * @param resultSet Result Set from the database
     * @return null
     */
    @Override
    List<Integer> getListOfResults(ResultSet resultSet) {
        return null;
    }

    /**
     * Fetches required information from Result Set. Creates and collects Integer with "count" value.
     *
     * @param resultSet Result Set from the database
     * @return Integer that was fetched from the database. If empty - no matches found.
     * If null - incorrect execution of the statement.
     */
    @Override
    Integer getResult(ResultSet resultSet) throws SQLException {
        Integer integer = null;
        while (resultSet.next()) {
            integer = resultSet.getInt("count");
        }
        return integer;
    }

}
