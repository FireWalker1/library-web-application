package com.ilin.library.dao.executors;

import com.ilin.library.entities.User;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * UserQueryExecutor class. Extends QueryExecutor abstract class for User entity.
 * Overriding and implementing two abstract methods for proper work with User entity.
 *
 * @author Pavlo Ilin
 * @version 1.0
 */
public class UserQueryExecutor extends QueryExecutor<User> {

    /**
     * Fetches required information from Result Set. Creates and collects list of users.
     *
     * @param resultSet Result Set from the database
     * @return list of users that were fetched from the database. If list is empty - no matches found.
     * If null - incorrect execution of the statement.
     */
    @Override
    List<User> getListOfResults(ResultSet resultSet) throws SQLException {
        List<User> users = new ArrayList<>();
        while (resultSet.next()) {
            User user = new User();
            EntityMapper.mappingUser(resultSet, user);
            users.add(user);
        }
        return users;
    }

    /**
     * Fetches required information from Result Set. Creates and collects user.
     *
     * @param resultSet Result Set from the database
     * @return user that was fetched from the database. If empty - no matches found.
     * If null - incorrect execution of the statement.
     */
    @Override
    User getResult(ResultSet resultSet) throws SQLException {
        User user = new User();
        while (resultSet.next()) {
            EntityMapper.mappingUser(resultSet, user);
        }
        return user;
    }

}
