package com.ilin.library.dao.executors;

import com.ilin.library.entities.Book;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * BookQueryExecutor class. Extends QueryExecutor abstract class for Book entity.
 * Overriding and implementing two abstract methods for proper work with Book entity.
 *
 * @author Pavlo Ilin
 * @version 1.0
 */
public class BookQueryExecutor extends QueryExecutor<Book> {

    /**
     * Fetches required information from Result Set. Creates and collects list of books.
     *
     * @param resultSet Result Set from the database
     * @return list of books that were fetched from the database. If list is empty - no matches found.
     * If null - incorrect execution of the statement.
     */
    @Override
    List<Book> getListOfResults(ResultSet resultSet) throws SQLException {
        List<Book> books = new ArrayList<>();
        while (resultSet.next()) {
            Book book = new Book();
            EntityMapper.mappingBook(resultSet, book);
            books.add(book);
        }
        return books;
    }

    /**
     * Fetches required information from Result Set. Creates and collects book.
     *
     * @param resultSet Result Set from the database
     * @return book that was fetched from the database. If empty - no matches found.
     * If null - incorrect execution of the statement.
     */
    @Override
    Book getResult(ResultSet resultSet) throws SQLException {
        Book book = new Book();
        while (resultSet.next()) {
            EntityMapper.mappingBook(resultSet, book);
        }
        return book;
    }

}
