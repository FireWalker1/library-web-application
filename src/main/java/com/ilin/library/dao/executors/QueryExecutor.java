package com.ilin.library.dao.executors;

import com.ilin.library.database.DBManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.util.List;

/**
 * QueryExecutor abstract class. Is called from DAO classes. Implementing Strategy pattern.
 * Setting arguments and executing query that was sent by DAO class in the database.
 * This class needs to be extended in case of adding new entities.
 *
 * @author Pavlo Ilin
 * @version 1.0
 */
public abstract class QueryExecutor<T> {

    private static final Logger logger = LogManager.getLogger(QueryExecutor.class);

    /**
     * Executes single query that do not returns any object.
     *
     * @param query SQL query
     * @param args  arguments for prepared statement
     * @return true or false depending on success of operation
     */
    public boolean executeQuery(String query, Object... args) {
        try (Connection connection = DBManager.getDataSource().getConnection();
             PreparedStatement statement = connection.prepareStatement(query)) {
            if (args.length > 0) {
                setArgumentsOfStatement(statement, args);
            }
            int i = statement.executeUpdate();
            return i > 0;
        } catch (SQLException e) {
            logger.error("Error catch in QueryExecutor - executeQuery()", e);
        }
        return false;
    }

    /**
     * Executes multiple queries that do not returns any object as a transaction.
     *
     * @param queries array of SQL queries
     * @param args    array of arguments for prepared statements
     * @return true or false depending on success of operation
     */
    public boolean executeTransaction(String[] queries, Object[]... args) {
        try (Connection connection = DBManager.getDataSource().getConnection()) {
            connection.setAutoCommit(false);
            Savepoint savepoint = connection.setSavepoint("Savepoint");
            for (int i = 0; i < queries.length; i++) {
                try (PreparedStatement statement = connection.prepareStatement(queries[i])) {
                    if (args[i].length > 0) {
                        setArgumentsOfStatement(statement, args[i]);
                    }
                    if (statement.executeUpdate() == 0) {
                        connection.rollback(savepoint);
                        return false;
                    }
                } catch (SQLException e) {
                    connection.rollback(savepoint);
                    throw new SQLException(e);
                }
            }
            connection.commit();
            return true;
        } catch (SQLException e) {
            logger.error("Error catch in QueryExecutor - executeTransaction()", e);
        }
        return false;
    }

    /**
     * Executes single query that returns list of indicated objects.
     *
     * @param query SQL query
     * @param args  arguments for prepared statement
     * @return list of objects that were fetched from the database. If list is empty - no matches found.
     * If null - incorrect execution of the statement.
     */
    public List<T> executeAndReturnValues(final String query, Object... args) {
        List<T> entities;
        try (Connection conn = DBManager.getDataSource().getConnection();
             PreparedStatement statement = conn.prepareStatement(query)) {
            if (args.length > 0) {
                setArgumentsOfStatement(statement, args);
            }
            try (ResultSet resultSet = statement.executeQuery()) {
                entities = getListOfResults(resultSet);
            }
        } catch (SQLException e) {
            logger.error("Error catch in QueryExecutor - executeAndReturnValues()", e);
            return null;
        }
        return entities;
    }

    /**
     * Executes single query that returns indicated object.
     *
     * @param query SQL query
     * @param args  arguments for prepared statement
     * @return object that was fetched from the database. If empty - no matches found.
     * If null - incorrect execution of the statement.
     */
    public T executeAndReturnValue(final String query, Object... args) {
        T entity;
        try (Connection conn = DBManager.getDataSource().getConnection();
             PreparedStatement statement = conn.prepareStatement(query)) {
            if (args.length > 0) {
                setArgumentsOfStatement(statement, args);
            }
            try (ResultSet resultSet = statement.executeQuery()) {
                entity = getResult(resultSet);
            }
        } catch (SQLException e) {
            logger.error("Error catch in QueryExecutor - executeAndReturnValue()", e);
            return null;
        }
        return entity;
    }

    /**
     * Fetches required information from Result Set. Creates and collects list of indicated objects.
     * Should be overrider for every entity
     *
     * @param rs Result Set from the database
     * @return list of objects that were fetched from the database. If list is empty - no matches found.
     * If null - incorrect execution of the statement.
     */
    abstract List<T> getListOfResults(ResultSet rs) throws SQLException;

    /**
     * Fetches required information from Result Set. Creates and collects indicated object.
     * Should be overrider for every entity
     *
     * @param rs Result Set from the database
     * @return object that was fetched from the database. If empty - no matches found.
     * If null - incorrect execution of the statement.
     */
    abstract T getResult(ResultSet rs) throws SQLException;

    private void setArgumentsOfStatement(PreparedStatement statement, Object... args)
            throws SQLException {
        for (int i = 0; i < args.length; i++) {
            statement.setObject(i + 1, args[i]);
        }
    }
}