package com.ilin.library.dao.executors;

import com.ilin.library.entities.Subscription;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * SubscriptionQueryExecutor class. Extends QueryExecutor abstract class for Subscription entity.
 * Overriding and implementing two abstract methods for proper work with Subscription entity.
 *
 * @author Pavlo Ilin
 * @version 1.0
 */
public class SubscriptionQueryExecutor extends QueryExecutor<Subscription> {

    /**
     * Fetches required information from Result Set. Creates and collects list of subscriptions.
     *
     * @param resultSet Result Set from the database
     * @return list of subscriptions that were fetched from the database. If list is empty - no matches found.
     * If null - incorrect execution of the statement.
     */
    @Override
    List<Subscription> getListOfResults(ResultSet resultSet) throws SQLException {
        List<Subscription> subscriptions = new ArrayList<>();
        while (resultSet.next()) {
            Subscription subscription = new Subscription();
            EntityMapper.mappingSubscription(resultSet, subscription);
            subscriptions.add(subscription);
        }
        return subscriptions;
    }

    /**
     * Fetches required information from Result Set. Creates and collects subscription.
     *
     * @param resultSet Result Set from the database
     * @return subscription that was fetched from the database. If empty - no matches found.
     * If null - incorrect execution of the statement.
     */
    @Override
    Subscription getResult(ResultSet resultSet) throws SQLException {
        Subscription subscription = new Subscription();
        while (resultSet.next()) {
            EntityMapper.mappingSubscription(resultSet, subscription);
        }
        return subscription;
    }

}
