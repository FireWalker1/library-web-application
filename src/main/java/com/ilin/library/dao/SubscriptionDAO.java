package com.ilin.library.dao;

import com.ilin.library.dao.executors.CountQueryExecutor;
import com.ilin.library.dao.executors.QueryExecutor;
import com.ilin.library.dao.executors.SubscriptionQueryExecutor;
import com.ilin.library.dao.executors.SumQueryExecutor;
import com.ilin.library.entities.Book;
import com.ilin.library.entities.Subscription;
import com.ilin.library.entities.enums.Status;
import com.ilin.library.entities.enums.Type;

import java.math.BigDecimal;
import java.sql.Date;
import java.util.Arrays;
import java.util.List;

/**
 * SubscriptionDAO class. Is called from service class.
 * Building correct SQL query (MySQL) and sending it to QueryExecutor.
 *
 * @author Pavlo Ilin
 * @version 1.0
 */
public class SubscriptionDAO {

    private final QueryExecutor<Subscription> executor = new SubscriptionQueryExecutor();

    /**
     * Gets all subscriptions from the database without ordering.
     *
     * @param pageNumber number of required page (fetching only 10 items per statement)
     * @return list of subscriptions that were fetched from the database. If list is empty - no matches found.
     * If null - incorrect execution of the statement.
     */
    public List<Subscription> getFullList(int pageNumber) {
        return executor.executeAndReturnValues(SQLQueries.SUBSCRIPTION_SELECT
                + SQLQueries.LIMIT, (pageNumber - 1) * 10, 10);
    }

    /**
     * Gets all subscriptions from the database with indicated ordering.
     *
     * @param orderBy    name of column in the database for ordering
     * @param desc       ascending or descending type ("true" or "false" possible)
     * @param pageNumber number of required page (fetching only 10 items per statement)
     * @return list of subscriptions that were fetched from the database. If list is empty - no matches found.
     * If null - incorrect execution of the statement.
     */
    public List<Subscription> getFullList(String orderBy, String desc, int pageNumber) {
        StringBuilder query = new StringBuilder(SQLQueries.SUBSCRIPTION_SELECT);
        if (orderBy != null) {
            if (orderBy.equals("id")) {
                query.append(SQLQueries.SUBSCRIPTION_ORDER_BY_ID);
            }
            if (orderBy.equals("userLogin")) {
                query.append(SQLQueries.SUBSCRIPTION_ORDER_BY_USER_LOGIN);
            }
            if (orderBy.equals("type")) {
                query.append(SQLQueries.SUBSCRIPTION_ORDER_BY_TYPE);
            }
            if (orderBy.equals("status")) {
                query.append(SQLQueries.SUBSCRIPTION_ORDER_BY_STATUS);
            }
            if (orderBy.equals("createdAt")) {
                query.append(SQLQueries.SUBSCRIPTION_ORDER_BY_CREATED_AT);
            }
            if (orderBy.equals("updatedAt")) {
                query.append(SQLQueries.SUBSCRIPTION_ORDER_BY_UPDATED_AT);
            }
            if (orderBy.equals("bookReturnDeadline")) {
                query.append(SQLQueries.SUBSCRIPTION_ORDER_BY_BOOK_RETURN_DEADLINE);
            }
            if (orderBy.equals("fine")) {
                query.append(SQLQueries.SUBSCRIPTION_ORDER_BY_FINE);
            }
        }
        if (desc != null && desc.equals("true")) {
            query.append(SQLQueries.DESC);
        }
        query.append(SQLQueries.LIMIT);
        return executor.executeAndReturnValues(query.toString(), (pageNumber - 1) * 10, 10);
    }

    /**
     * Inserting new subscription to the database.
     *
     * @param subscription Subscription entity that will be inserted
     * @param userLogin    login of user that will be attached to subscription
     * @return true or false depending on success of operation
     */
    public boolean insertSubscription(Subscription subscription, String userLogin) {
        List<Book> books = subscription.getBooks();
        StringBuilder query = new StringBuilder(SQLQueries.SUBSCRIPTION_HAS_BOOK_INSERT);
        Object[][] args = new Object[2][];
        String type = subscription.getType() == Type.HOME ? "home" : "reading room";
        args[0] = new Object[]{userLogin, type};
        args[1] = new Object[books.size() * 2];
        args[1][0] = userLogin;
        args[1][1] = books.get(0).getId();

        for (int i = 1; i < books.size(); i++) {
            query.append(SQLQueries.SUBSCRIPTION_HAS_BOOK_INSERT_ADDITIONAL_BOOK);
            args[1][i * 2] = userLogin;
            args[1][i * 2 + 1] = books.get(i).getId();
        }
        String[] queries = {SQLQueries.SUBSCRIPTION_INSERT, query.toString()};
        return executor.executeTransaction(queries, args);
    }

    /**
     * Gets all subscriptions from the database that related to certain user.
     *
     * @param userLogin login of required user
     * @return list of subscriptions that were fetched from the database. If list is empty - no matches found.
     * If null - incorrect execution of the statement.
     */
    public List<Subscription> getUserSubscriptions(String userLogin) {
        return executor.executeAndReturnValues(SQLQueries.SUBSCRIPTION_GET_USER_SUBSCRIPTIONS, userLogin);
    }

    /**
     * Gets all subscriptions from the database that have status "ongoing".
     *
     * @return list of subscriptions that were fetched from the database. If list is empty - no matches found.
     * If null - incorrect execution of the statement.
     */
    public List<Subscription> getOngoingSubscriptions() {
        return executor.executeAndReturnValues(SQLQueries.SUBSCRIPTION_GET_ONGOING_SUBSCRIPTIONS);
    }

    /**
     * Gets subscription from the database with indicated ID.
     *
     * @param id ID of required subscription
     * @return subscription that was fetched from the database. If empty - no matches found.
     * If null - incorrect execution of the statement.
     */
    public Subscription getSubscriptionByID(int id) {
        return executor.executeAndReturnValue(SQLQueries.SUBSCRIPTION_GET_BY_ID, id);
    }

    /**
     * Gets quantity of rows of the subscriptions from the database.
     *
     * @return Integer with quantity of rows. If empty - no matches found.
     * If null - incorrect execution of the statement.
     */
    public Integer getRowsQuantity() {
        QueryExecutor<Integer> intExecutor = new CountQueryExecutor();
        return intExecutor.executeAndReturnValue(SQLQueries.SUBSCRIPTION_GET_COUNT);
    }

    /**
     * Updates status of the subscription with indicated ID to "confirmed" in the database.
     *
     * @param id ID of required subscription
     * @return true or false depending on success of operation
     */
    public boolean updateStatusToConfirmed(int id) {
        return executor.executeQuery(SQLQueries.SUBSCRIPTION_UPDATE_STATUS, Status.CONFIRMED.toString().toLowerCase(), id);
    }

    /**
     * Updates status of the subscription with indicated ID to "ongoing" in the database.
     *
     * @param id   ID of required subscription
     * @param date deadline date for returning books back
     * @return true or false depending on success of operation
     */
    public boolean updateStatusToOngoing(int id, Date date) {
        String[] queries = {SQLQueries.SUBSCRIPTION_UPDATE_STATUS, SQLQueries.SUBSCRIPTION_UPDATE_BOOK_RETURN_DEADLINE};
        Object[] args1 = {Status.ONGOING.toString().toLowerCase(), id};
        Object[] args2 = {date, id};
        return executor.executeTransaction(queries, args1, args2);
    }

    /**
     * Updates status of the subscription with indicated ID to "closed" in the database.
     *
     * @param id    ID of required subscription
     * @param books list of books in subscription. Their quantity increases on 1 in the database
     * @return true or false depending on success of operation
     */
    public boolean updateStatusToClosed(int id, List<Book> books) {
        String[] queries = new String[books.size() + 1];
        Arrays.fill(queries, SQLQueries.BOOK_UPDATE_QTY);
        queries[0] = SQLQueries.SUBSCRIPTION_UPDATE_STATUS;

        Object[][] args = new Object[books.size() + 1][2];
        args[0][0] = Status.CLOSED.toString().toLowerCase();
        args[0][1] = id;
        for (int i = 1; i < books.size() + 1; i++) {
            args[i][0] = books.get(i - 1).getQuantity() + 1;
            args[i][1] = books.get(i - 1).getId();
        }
        return executor.executeTransaction(queries, args);
    }

    /**
     * Updates fine for the subscription with indicated ID in the database.
     *
     * @param id   ID of required subscription
     * @param fine new fine value
     * @return true or false depending on success of operation
     */
    public boolean updateFine(BigDecimal fine, int id) {
        return executor.executeQuery(SQLQueries.SUBSCRIPTION_UPDATE_FINE, fine, id);
    }

    /**
     * Gets total quantity of fines from the database.
     *
     * @param userLogin login of required user
     * @return Integer with quantity of rows. If empty - no matches found.
     * If null - incorrect execution of the statement.
     */
    public Integer getUserFines(String userLogin) {
        QueryExecutor<Integer> intExecutor = new SumQueryExecutor();
        return intExecutor.executeAndReturnValue(SQLQueries.SUBSCRIPTION_GET_COUNT_FINE_BY_LOGIN, userLogin);
    }
}
