package com.ilin.library.dao;

import com.ilin.library.dao.executors.BookQueryExecutor;
import com.ilin.library.dao.executors.CountQueryExecutor;
import com.ilin.library.dao.executors.QueryExecutor;
import com.ilin.library.entities.Author;
import com.ilin.library.entities.Book;
import com.ilin.library.entities.enums.Language;

import java.util.ArrayList;
import java.util.List;

/**
 * BookDAO class. Is called from service class. Building correct SQL query (MySQL) and sending it to QueryExecutor.
 *
 * @author Pavlo Ilin
 * @version 1.0
 */
public class BookDAO {

    private final QueryExecutor<Book> executor = new BookQueryExecutor();

    /**
     * Gets all books from the database without filtering.
     *
     * @return list of books that were fetched from the database. If list is empty - no matches found.
     * If null - incorrect execution of the statement.
     */
    public List<Book> getFullList() {

        return executor.executeAndReturnValues(SQLQueries.BOOK_SELECT + SQLQueries.LIMIT, 0, 10);
    }

    /**
     * Gets books from the database according to the indicated parameters.
     *
     * @param orderBy    name of column in the database for ordering
     * @param desc       ascending or descending type ("true" or "false" possible)
     * @param pageNumber number of required page (fetching only 10 items per statement)
     * @param words      array of Strings for searching among all results
     * @return list of books that were fetched from the database. If list is empty - no matches found.
     * If null - incorrect execution of the statement.
     */
    public List<Book> getSearchResultList(String orderBy, String desc, int pageNumber, String[] words) {
        if (words.length == 0) return getFullList();
        List<Object> arguments = new ArrayList<>();
        StringBuilder query = new StringBuilder(SQLQueries.BOOK_SELECT);
        buildQueryAndArguments(orderBy, desc, pageNumber, words, arguments, query);
        return executor.executeAndReturnValues(query.toString(), arguments.toArray());
    }

    /**
     * Gets book from the database according to the indicated ID.
     *
     * @param id ID of the required book
     * @return book that was fetched from the database. If empty - no matches found.
     * If null - incorrect execution of the statement.
     */
    public Book getByID(int id) {
        return executor.executeAndReturnValue(SQLQueries.BOOK_SELECT_BY_ID, id);
    }

    /**
     * Updating title of book with indicated ID in the database.
     *
     * @param id    ID of the required book
     * @param title new title
     * @return true or false depending on success of operation
     */
    public boolean updateTitle(int id, String title) {
        return executor.executeQuery(SQLQueries.BOOK_UPDATE_TITLE, title, id);
    }

    /**
     * Updating author name (UA) of book with indicated ID in the database.
     *
     * @param id           ID of the required book
     * @param authorNameUA new author name (UA)
     * @return true or false depending on success of operation
     */
    public boolean updateAuthorNameUA(int id, String authorNameUA) {
        return executor.executeQuery(SQLQueries.BOOK_UPDATE_AUTHOR_NAME_UA, authorNameUA, id);
    }

    /**
     * Updating author name (EN) of book with indicated ID in the database.
     *
     * @param id           ID of the required book
     * @param authorNameEN new author name (EN)
     * @return true or false depending on success of operation
     */
    public boolean updateAuthorNameEN(int id, String authorNameEN) {
        return executor.executeQuery(SQLQueries.BOOK_UPDATE_AUTHOR_NAME_EN, authorNameEN, id);
    }

    /**
     * Updating publisher house name of book with indicated ID in the database.
     *
     * @param id             ID of the required book
     * @param publisherHouse new publisher house name
     * @return true or false depending on success of operation
     */
    public boolean updatePublisherHouse(int id, String publisherHouse) {
        return executor.executeQuery(SQLQueries.BOOK_UPDATE_PUBLISHER_HOUSE, publisherHouse, id);
    }

    /**
     * Updating year of book with indicated ID in the database.
     *
     * @param id   ID of the required book
     * @param year new year
     * @return true or false depending on success of operation
     */
    public boolean updateYear(int id, int year) {
        return executor.executeQuery(SQLQueries.BOOK_UPDATE_YEAR, year, id);
    }

    /**
     * Updating language of book with indicated ID in the database.
     *
     * @param id   ID of the required book
     * @param lang new language
     * @return true or false depending on success of operation
     */
    public boolean updateLanguage(int id, Language lang) {
        return executor.executeQuery(SQLQueries.BOOK_UPDATE_LANGUAGE, lang.toString(), id);
    }

    /**
     * Updating quantity of book with indicated ID in the database.
     *
     * @param id       ID of the required book
     * @param quantity new quantity
     * @return true or false depending on success of operation
     */
    public boolean updateQuantity(int id, int quantity) {
        return executor.executeQuery(SQLQueries.BOOK_UPDATE_QTY, quantity, id);
    }

    /**
     * Gets books from the database according to the subscription ID.
     *
     * @param subscriptionID ID of the required subscription
     * @return list of books that were fetched from the database. If list is empty - no matches found.
     * If null - incorrect execution of the statement.
     */
    public List<Book> getBySubscriptionID(int subscriptionID) {
        return executor.executeAndReturnValues(SQLQueries.SUBSCRIPTION_HAS_BOOK_GET_BOOKS_BY_ID, subscriptionID);
    }

    /**
     * Gets quantity of rows of the books from the database according to the indicated parameters.
     *
     * @param words array of Strings for searching among all results
     * @return Integer with quantity of rows. If empty - no matches found.
     * If null - incorrect execution of the statement.
     */
    public Integer getRowsQuantity(String[] words) {
        QueryExecutor<Integer> intExecutor = new CountQueryExecutor();
        List<Object> arguments = new ArrayList<>();
        StringBuilder query = new StringBuilder(SQLQueries.BOOK_GET_COUNT);
        if (words != null && words.length > 0) {
            buildQueryAndArguments(null, null, 0, words, arguments, query);
            return intExecutor.executeAndReturnValue(query.toString(), arguments.toArray());
        }
        return intExecutor.executeAndReturnValue(query.toString());
    }

    /**
     * Gets exact copy of the provided book from the database (if exists).
     *
     * @param book filled Book entity
     * @return same Book entity if exists. If empty - such book doesn't exist.
     * If null - incorrect execution of the statement.
     */
    public Book getBook(Book book) {
        return executor.executeAndReturnValue(SQLQueries.BOOK_GET_BOOK_FULL, book.getTitle(), book.getAuthor().getNameEN(),
                book.getAuthor().getNameUA(), book.getPublisherHouse(), book.getYear(), book.getLanguage().toString());
    }

    /**
     * Inserting new book to the database.
     *
     * @param book Book entity that will be inserted
     * @return true or false depending on success of operation
     */
    public boolean insertBook(Book book) {
        return executor.executeQuery(SQLQueries.BOOK_INSERT, book.getTitle(), book.getAuthor().getNameEN(),
                book.getPublisherHouse(), book.getYear(), book.getLanguage().toString(), book.getQuantity());
    }

    /**
     * Inserting new author to the database.
     *
     * @param author Author entity that will be inserted
     */
    public void insertAuthor(Author author) {
        executor.executeQuery(SQLQueries.BOOK_AUTHOR_INSERT, author.getNameEN(), author.getNameUA());
    }

    /**
     * Deleting book with indicated ID from the database.
     *
     * @param id ID of the required book
     * @return true or false depending on success of operation
     */
    public boolean deleteBook(int id) {
        return executor.executeQuery(SQLQueries.BOOK_DELETE, id);
    }


    /**
     * Updating of the author for indicated book
     *
     * @param author author that will be updated
     * @param id     ID of the required book
     * @return true or false depending on success of operation
     */
    public boolean updateAuthor(Author author, int id) {
        return executor.executeQuery(SQLQueries.BOOK_UPDATE_AUTHOR, author.getNameEN(), id);
    }

    private void buildQueryAndArguments(String orderBy, String desc, int pageNumber, String[] words,
                                        List<Object> arguments, StringBuilder query) {
        query.append(SQLQueries.BOOK_SEARCH_FIRST_WORD);
        for (int j = 0; j < 5; j++) arguments.add("%" + words[0] + "%");

        for (int i = 1; i < words.length; i++) {
            query.append(SQLQueries.BOOK_SEARCH_ADDITIONAL_WORD);
            for (int j = 0; j < 5; j++) arguments.add("%" + words[i] + "%");
        }
        if (orderBy != null) {
            if (orderBy.equals("id")) {
                query.append(SQLQueries.BOOK_SEARCH_ORDER_BY_ID);
            }
            if (orderBy.equals("title")) {
                query.append(SQLQueries.BOOK_SEARCH_ORDER_BY_TITLE);
            }
            if (orderBy.equals("name_UA")) {
                query.append(SQLQueries.BOOK_SEARCH_ORDER_BY_AUTHOR_NAME_UA);
            }
            if (orderBy.equals("name_EN")) {
                query.append(SQLQueries.BOOK_SEARCH_ORDER_BY_AUTHOR_NAME_EN);
            }
            if (orderBy.equals("publisher_house")) {
                query.append(SQLQueries.BOOK_SEARCH_ORDER_BY_PUBLISHER_HOUSE);
            }
            if (orderBy.equals("year")) {
                query.append(SQLQueries.BOOK_SEARCH_ORDER_BY_YEAR);
            }
            if (orderBy.equals("language")) {
                query.append(SQLQueries.BOOK_SEARCH_ORDER_BY_LANGUAGE);
            }
            if (orderBy.equals("quantity")) {
                query.append(SQLQueries.BOOK_SEARCH_ORDER_BY_QUANTITY);
            }
        }
        if (desc != null && desc.equals("true")) {
            query.append(SQLQueries.DESC);
        }
        if (pageNumber != 0) {
            query.append(SQLQueries.LIMIT);
            arguments.add((pageNumber - 1) * 10);
            arguments.add(10);
        }
    }
}
