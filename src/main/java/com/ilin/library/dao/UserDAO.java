package com.ilin.library.dao;

import com.ilin.library.dao.executors.CountQueryExecutor;
import com.ilin.library.dao.executors.QueryExecutor;
import com.ilin.library.dao.executors.UserQueryExecutor;
import com.ilin.library.entities.User;
import com.ilin.library.entities.enums.Role;

import java.sql.Timestamp;
import java.util.List;

/**
 * UserDAO class. Is called from service class. Building correct SQL query (MySQL) and sending it to QueryExecutor.
 *
 * @author Pavlo Ilin
 * @version 1.0
 */
public class UserDAO {

    private final QueryExecutor<User> executor = new UserQueryExecutor();

    /**
     * Gets all users from the database without ordering.
     *
     * @param pageNumber number of required page (fetching only 10 items per statement)
     * @return list of users that were fetched from the database. If list is empty - no matches found.
     * If null - incorrect execution of the statement.
     */
    public List<User> getFullList(int pageNumber) {
        return executor.executeAndReturnValues(SQLQueries.USER_SELECT + SQLQueries.LIMIT, (pageNumber - 1) * 10, 10);
    }

    /**
     * Gets all users from the database with indicated ordering.
     *
     * @param orderBy    name of column in the database for ordering
     * @param desc       ascending or descending type ("true" or "false" possible)
     * @param pageNumber number of required page (fetching only 10 items per statement)
     * @return list of users that were fetched from the database. If list is empty - no matches found.
     * If null - incorrect execution of the statement.
     */
    public List<User> getFullList(String orderBy, String desc, int pageNumber) {
        StringBuilder query = new StringBuilder(SQLQueries.USER_SELECT);
        buildQuery(orderBy, desc, query);
        return executor.executeAndReturnValues(query.toString(), (pageNumber - 1) * 10, 10);
    }

    /**
     * Gets all users with "reader" role from the database without ordering.
     *
     * @param pageNumber number of required page (fetching only 10 items per statement)
     * @return list of users that were fetched from the database. If list is empty - no matches found.
     * If null - incorrect execution of the statement.
     */
    public List<User> getFullReadersList(int pageNumber) {
        return executor.executeAndReturnValues(SQLQueries.USER_GET_BY_ROLE + SQLQueries.LIMIT,
                Role.READER.toString().toLowerCase(), (pageNumber - 1) * 10, 10);
    }

    /**
     * Gets all users with "reader" role from the database with indicated ordering.
     *
     * @param orderBy    name of column in the database for ordering
     * @param desc       ascending or descending type ("true" or "false" possible)
     * @param pageNumber number of required page (fetching only 10 items per statement)
     * @return list of users that were fetched from the database. If list is empty - no matches found.
     * If null - incorrect execution of the statement.
     */
    public List<User> getFullReadersList(String orderBy, String desc, int pageNumber) {
        StringBuilder query = new StringBuilder(SQLQueries.USER_GET_BY_ROLE);
        buildQuery(orderBy, desc, query);
        return executor.executeAndReturnValues(query.toString(),
                Role.READER.toString().toLowerCase(), (pageNumber - 1) * 10, 10);
    }

    /**
     * Gets user from the database according to the indicated login.
     *
     * @param login login of the required user
     * @return user that was fetched from the database. If empty - no matches found.
     * If null - incorrect execution of the statement.
     */
    public User getByLogin(String login) {
        return executor.executeAndReturnValue(SQLQueries.USER_GET_BY_LOGIN, login);
    }

    /**
     * Gets user from the database according to the indicated email.
     *
     * @param email email of the required user
     * @return user that was fetched from the database. If empty - no matches found.
     * If null - incorrect execution of the statement.
     */
    public User getByEmail(String email) {
        return executor.executeAndReturnValue(SQLQueries.USER_GET_BY_EMAIL, email);
    }

    /**
     * Gets user from the database according to the indicated phone number.
     *
     * @param phoneNumber phone number of the required user
     * @return user that was fetched from the database. If empty - no matches found.
     * If null - incorrect execution of the statement.
     */
    public User getByPhoneNumber(String phoneNumber) {
        return executor.executeAndReturnValue(SQLQueries.USER_GET_BY_PHONE_NUMBER, phoneNumber);
    }

    /**
     * Inserting new user to the database.
     *
     * @param user User entity that will be inserted
     * @return true or false depending on success of operation
     */
    public boolean insertUser(User user) {
        return executor.executeQuery(SQLQueries.USER_INSERT, user.getLogin(), user.getEmail(), user.getPassword(),
                user.getPhoneNumber(), user.getRole().toString().toLowerCase(), user.getFirstName(), user.getLastName());
    }

    /**
     * Updating login of user with indicated login in the database.
     *
     * @param oldLogin login of the required user
     * @param newLogin new login
     * @return true or false depending on success of operation
     */
    public boolean updateLogin(String newLogin, String oldLogin) {
        return executor.executeQuery(SQLQueries.USER_UPDATE_LOGIN, newLogin, oldLogin);
    }

    /**
     * Updating email of user with indicated login in the database.
     *
     * @param login login of the required user
     * @param email new email
     * @return true or false depending on success of operation
     */
    public boolean updateEmail(String email, String login) {
        return executor.executeQuery(SQLQueries.USER_UPDATE_EMAIL, email, login);
    }

    /**
     * Updating password of user with indicated login in the database.
     *
     * @param login    login of the required user
     * @param password new password
     * @return true or false depending on success of operation
     */
    public boolean updatePassword(String password, String login) {
        return executor.executeQuery(SQLQueries.USER_UPDATE_PASSWORD, password, login);
    }

    /**
     * Updating phone number of user with indicated login in the database.
     *
     * @param login       login of the required user
     * @param phoneNumber new phone number
     * @return true or false depending on success of operation
     */
    public boolean updatePhoneNumber(String phoneNumber, String login) {
        return executor.executeQuery(SQLQueries.USER_UPDATE_PHONE_NUMBER, phoneNumber, login);
    }

    /**
     * Updating first name of user with indicated login in the database.
     *
     * @param login     login of the required user
     * @param firstName new first name
     * @return true or false depending on success of operation
     */
    public boolean updateFirstName(String firstName, String login) {
        return executor.executeQuery(SQLQueries.USER_UPDATE_FIRST_NAME, firstName, login);
    }

    /**
     * Updating last name of user with indicated login in the database.
     *
     * @param login    login of the required user
     * @param lastName new last name
     * @return true or false depending on success of operation
     */
    public boolean updateLastName(String lastName, String login) {
        return executor.executeQuery(SQLQueries.USER_UPDATE_LAST_NAME, lastName, login);
    }

    /**
     * Updating "blocked_at" of user with indicated login in the database.
     *
     * @param login     login of the required user
     * @param timestamp new timestamp
     * @return true or false depending on success of operation
     */
    public boolean updateBlockedAt(Timestamp timestamp, String login) {
        return executor.executeQuery(SQLQueries.USER_UPDATE_BLOCKED_AT, timestamp, login);
    }

    /**
     * Gets quantity of rows of the users from the database according to the indicated parameters.
     *
     * @return Integer with quantity of rows. If empty - no matches found.
     * If null - incorrect execution of the statement.
     */
    public Integer getRowsQuantityUsers() {
        QueryExecutor<Integer> intExecutor = new CountQueryExecutor();
        return intExecutor.executeAndReturnValue(SQLQueries.USER_GET_COUNT);
    }

    /**
     * Gets quantity of rows of the users with "readers" role from the database according to the indicated parameters.
     *
     * @return Integer with quantity of rows. If empty - no matches found.
     * If null - incorrect execution of the statement.
     */
    public Integer getRowsQuantityReaders() {
        QueryExecutor<Integer> intExecutor = new CountQueryExecutor();
        return intExecutor.executeAndReturnValue(SQLQueries.USER_READERS_GET_COUNT);
    }

    /**
     * Deleting user with indicated login from the database.
     *
     * @param login login of the required user
     * @return true or false depending on success of operation
     */
    public boolean deleteUser(String login) {
        return executor.executeQuery(SQLQueries.USER_DELETE, login);
    }

    private void buildQuery(String orderBy, String desc, StringBuilder query) {
        if (orderBy != null) {
            if (orderBy.equals("id")) {
                query.append(SQLQueries.USER_ORDER_BY_ID);
            }
            if (orderBy.equals("login")) {
                query.append(SQLQueries.USER_ORDER_BY_LOGIN);
            }
            if (orderBy.equals("email")) {
                query.append(SQLQueries.USER_ORDER_BY_EMAIL);
            }
            if (orderBy.equals("phoneNumber")) {
                query.append(SQLQueries.USER_ORDER_BY_PHONE_NUMBER);
            }
            if (orderBy.equals("role")) {
                query.append(SQLQueries.USER_ORDER_BY_ROLE);
            }
            if (orderBy.equals("firstName")) {
                query.append(SQLQueries.USER_ORDER_BY_FIRST_NAME);
            }
            if (orderBy.equals("lastName")) {
                query.append(SQLQueries.USER_ORDER_BY_LAST_NAME);
            }
            if (orderBy.equals("blockedAt")) {
                query.append(SQLQueries.USER_ORDER_BY_BLOCKED_AT);
            }
        }
        if (desc != null && desc.equals("true")) {
            query.append(SQLQueries.DESC);
        }
        query.append(SQLQueries.LIMIT);
    }
}
