package com.ilin.library.dao;

/**
 * SQLQueries class. Contains all SQL queries (MySQL) for proper work of the application.
 *
 * @author Pavlo Ilin
 * @version 1.0
 */
public class SQLQueries {

    // book queries
    public static final String BOOK_SELECT = "SELECT book.id AS book_id, book.title, author.id AS author_id, author.name_UA, " +
            "author.name_EN, book.publisher_house, book.year, book.language, book.quantity " +
            "FROM book JOIN author ON book.author_id = author.id";
    public static final String BOOK_SEARCH_FIRST_WORD = " WHERE title LIKE ? OR name_UA LIKE ? OR name_EN LIKE ? " +
            "OR publisher_house LIKE ? OR year LIKE ?";
    public static final String BOOK_SEARCH_ADDITIONAL_WORD = " OR title LIKE ? OR name_UA LIKE ? OR name_EN LIKE ? " +
            "OR publisher_house LIKE ? OR year LIKE ?";
    public static final String BOOK_SEARCH_ORDER_BY_ID = " ORDER BY book_id";
    public static final String BOOK_SEARCH_ORDER_BY_TITLE = " ORDER BY title";
    public static final String BOOK_SEARCH_ORDER_BY_AUTHOR_NAME_UA = " ORDER BY author.name_UA";
    public static final String BOOK_SEARCH_ORDER_BY_AUTHOR_NAME_EN = " ORDER BY author.name_EN";
    public static final String BOOK_SEARCH_ORDER_BY_PUBLISHER_HOUSE = " ORDER BY publisher_house";
    public static final String BOOK_SEARCH_ORDER_BY_YEAR = " ORDER BY year";
    public static final String BOOK_SEARCH_ORDER_BY_LANGUAGE = " ORDER BY language";
    public static final String BOOK_SEARCH_ORDER_BY_QUANTITY = " ORDER BY quantity";
    public static final String BOOK_GET_COUNT = "SELECT COUNT(*) AS count FROM book JOIN author ON book.author_id = author.id";
    public static final String BOOK_SELECT_BY_ID = BOOK_SELECT + " WHERE book.id = ?";
    public static final String BOOK_GET_BOOK_FULL = BOOK_SELECT + " WHERE book.title = ? AND author.name_EN = ? AND " +
            "author.name_UA = ? AND book.publisher_house = ? AND book.year = ? AND book.language = ?";
    public static final String BOOK_UPDATE_AUTHOR = "UPDATE book SET author_id = " +
            "(SELECT id FROM author WHERE name_EN = ?) WHERE id = ?";
    public static final String BOOK_AUTHOR_INSERT = "INSERT INTO author (name_EN, name_UA) VALUES (?, ?)";
    public static final String BOOK_INSERT = "INSERT INTO book (title, author_id, publisher_house, `year`, `language`, quantity) " +
            "VALUES (?, (SELECT id FROM author WHERE name_EN = ?), ?, ?, ?, ?)";
    public static final String BOOK_UPDATE_TITLE = "UPDATE book SET title = ? WHERE id = ?";
    public static final String BOOK_UPDATE_AUTHOR_NAME_UA = "UPDATE author SET name_UA = ? " +
            "WHERE id = (SELECT author_id FROM book WHERE id = ?)";
    public static final String BOOK_UPDATE_AUTHOR_NAME_EN = "UPDATE author SET name_EN = ? " +
            "WHERE id = (SELECT author_id FROM book WHERE id = ?)";
    public static final String BOOK_UPDATE_PUBLISHER_HOUSE = "UPDATE book SET publisher_house = ? WHERE id = ?";
    public static final String BOOK_UPDATE_YEAR = "UPDATE book SET year = ? WHERE id = ?";
    public static final String BOOK_UPDATE_LANGUAGE = "UPDATE book SET language = ? WHERE id = ?";
    public static final String BOOK_UPDATE_QTY = "UPDATE book SET quantity = ? WHERE id = ?";
    public static final String BOOK_DELETE = "DELETE FROM book WHERE id = ?";
    public static final String SUBSCRIPTION_HAS_BOOK_GET_BOOKS_BY_ID = "SELECT book.id AS book_id, book.title, " +
            "author.id AS author_id, author.name_UA, author.name_EN, book.publisher_house, book.year, book.language, " +
            "book.quantity FROM (subscription_has_book JOIN book ON book.id = subscription_has_book.book_id) " +
            "JOIN author ON book.author_id = author.id WHERE subscription_has_book.subscription_id = ?";


    // user queries

    public static final String USER_SELECT = "SELECT id AS user_id, login, email, password, phone_number, role, " +
            "first_name, last_name, blocked_at FROM user";
    public static final String USER_GET_BY_LOGIN = USER_SELECT + " WHERE login = ?";
    public static final String USER_GET_BY_EMAIL = USER_SELECT + " WHERE email = ?";
    public static final String USER_GET_BY_PHONE_NUMBER = USER_SELECT + " WHERE phone_number = ?";
    public static final String USER_GET_BY_ROLE = USER_SELECT + " WHERE role = ?";
    public static final String USER_INSERT = "INSERT INTO user " +
            "(login, email, `password`, phone_number, `role`, first_name, last_name) VALUES (?, ?, ?, ?, ?, ?, ?)";
    public static final String USER_UPDATE_LOGIN = "UPDATE user SET login = ? WHERE login = ?";
    public static final String USER_UPDATE_EMAIL = "UPDATE user SET email = ? WHERE login = ?";
    public static final String USER_UPDATE_PASSWORD = "UPDATE user SET password = ? WHERE login = ?";
    public static final String USER_UPDATE_PHONE_NUMBER = "UPDATE user SET phone_number = ? WHERE login = ?";
    public static final String USER_UPDATE_FIRST_NAME = "UPDATE user SET first_name = ? WHERE login = ?";
    public static final String USER_UPDATE_LAST_NAME = "UPDATE user SET last_name = ? WHERE login = ?";
    public static final String USER_UPDATE_BLOCKED_AT = "UPDATE user SET blocked_at = ? WHERE login = ?";
    public static final String USER_ORDER_BY_ID = " ORDER BY user_id";
    public static final String USER_ORDER_BY_LOGIN = " ORDER BY login";
    public static final String USER_ORDER_BY_EMAIL = " ORDER BY email";
    public static final String USER_ORDER_BY_PHONE_NUMBER = " ORDER BY phone_number";
    public static final String USER_ORDER_BY_ROLE = " ORDER BY role";
    public static final String USER_ORDER_BY_FIRST_NAME = " ORDER BY first_name";
    public static final String USER_ORDER_BY_LAST_NAME = " ORDER BY last_name";
    public static final String USER_ORDER_BY_BLOCKED_AT = " ORDER BY blocked_at";
    public static final String USER_GET_COUNT = "SELECT COUNT(*) AS count FROM user";
    public static final String USER_READERS_GET_COUNT = USER_GET_COUNT + " WHERE role = 'reader'";
    public static final String USER_DELETE = "DELETE FROM user WHERE login = ?";


    // subscription queries

    public static final String SUBSCRIPTION_SELECT = "SELECT subscription.id AS subscription_id, subscription.type, subscription.status, subscription.created_at, subscription.updated_at, " +
            "subscription.book_return_deadline, subscription.fine, user.id AS user_id, user.login, user.email, user.password, user.phone_number, " +
            "user.role, user.first_name, user.last_name, user.blocked_at " +
            "FROM subscription JOIN user ON subscription.user_id = user.id";
    public static final String SUBSCRIPTION_INSERT = "INSERT INTO subscription (user_id, type) " +
            "VALUES ((SELECT id FROM user WHERE login = ?), ?)";
    public static final String SUBSCRIPTION_HAS_BOOK_INSERT = "INSERT INTO subscription_has_book (subscription_id, book_id) VALUES " +
            "((SELECT MAX(id) FROM subscription WHERE user_id = (SELECT id FROM user WHERE login = ?)), ?)";
    public static final String SUBSCRIPTION_HAS_BOOK_INSERT_ADDITIONAL_BOOK =
            ", ((SELECT MAX(id) FROM subscription WHERE user_id = (SELECT id FROM user WHERE login = ?)), ?)";
    public static final String SUBSCRIPTION_GET_USER_SUBSCRIPTIONS = SUBSCRIPTION_SELECT +
            " WHERE user_id = (SELECT id FROM user WHERE login = ?)";
    public static final String SUBSCRIPTION_GET_ONGOING_SUBSCRIPTIONS = SUBSCRIPTION_SELECT + " WHERE status = 'ongoing'";
    public static final String SUBSCRIPTION_GET_BY_ID = SUBSCRIPTION_SELECT +
            " WHERE subscription.id = ?";
    public static final String SUBSCRIPTION_ORDER_BY_ID = " ORDER BY subscription_id";
    public static final String SUBSCRIPTION_ORDER_BY_USER_LOGIN = " ORDER BY user.login";
    public static final String SUBSCRIPTION_ORDER_BY_TYPE = " ORDER BY type";
    public static final String SUBSCRIPTION_ORDER_BY_STATUS = " ORDER BY status";
    public static final String SUBSCRIPTION_ORDER_BY_CREATED_AT = " ORDER BY created_at";
    public static final String SUBSCRIPTION_ORDER_BY_UPDATED_AT = " ORDER BY updated_at";
    public static final String SUBSCRIPTION_ORDER_BY_BOOK_RETURN_DEADLINE = " ORDER BY book_return_deadline";
    public static final String SUBSCRIPTION_ORDER_BY_FINE = " ORDER BY fine";
    public static final String SUBSCRIPTION_GET_COUNT = "SELECT COUNT(*) AS count FROM subscription";
    public static final String SUBSCRIPTION_GET_COUNT_FINE_BY_LOGIN = "SELECT SUM(fine) AS sum FROM subscription " +
            "JOIN user ON subscription.user_id=user.id WHERE fine > 0 && user.login = ?";
    public static final String SUBSCRIPTION_UPDATE_STATUS = "UPDATE subscription SET status = ? WHERE id = ?";
    public static final String SUBSCRIPTION_UPDATE_BOOK_RETURN_DEADLINE = "UPDATE subscription SET book_return_deadline = ? WHERE id = ?";
    public static final String SUBSCRIPTION_UPDATE_FINE = "UPDATE subscription SET fine = ? WHERE id = ?";


    // common queries

    public static final String DESC = " DESC";
    public static final String LIMIT = " LIMIT ?,?";
}
