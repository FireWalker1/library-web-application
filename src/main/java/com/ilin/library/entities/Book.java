package com.ilin.library.entities;

import com.ilin.library.entities.enums.Language;

import java.util.Objects;

/**
 * Book class. Entity class that matches "book" table in the database.
 * Standard constructors, getters and setters are used.
 *
 * @author Pavlo Ilin
 * @version 1.0
 */
public class Book {
    private int id;
    private String title;
    private Author author;
    private String publisherHouse;
    private int year;
    private Language language;
    private int quantity;

    public Book(int id, String title, Author author, String publisherHouse, int year, Language language, int quantity) {
        this.id = id;
        this.title = title;
        this.author = author;
        this.publisherHouse = publisherHouse;
        this.year = year;
        this.language = language;
        this.quantity = quantity;
    }

    public Book() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

    public String getPublisherHouse() {
        return publisherHouse;
    }

    public void setPublisherHouse(String publisherHouse) {
        this.publisherHouse = publisherHouse;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public Language getLanguage() {
        return language;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Book book = (Book) o;
        return year == book.year && Objects.equals(title, book.title) && Objects.equals(author, book.author) && Objects.equals(publisherHouse, book.publisherHouse) && language == book.language;
    }

    @Override
    public int hashCode() {
        return Objects.hash(title, author, publisherHouse, year, language);
    }

    @Override
    public String toString() {
        return "Book{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", author=" + author +
                ", publisherHouse='" + publisherHouse + '\'' +
                ", year=" + year +
                ", language=" + language +
                ", quantity=" + quantity +
                '}';
    }
}
