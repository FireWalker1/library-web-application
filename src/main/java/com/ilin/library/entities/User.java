package com.ilin.library.entities;

import com.ilin.library.entities.enums.Role;

import java.sql.Timestamp;
import java.util.Objects;

/**
 * User class. Entity class that matches "user" table in the database.
 * Standard constructors, getters and setters are used.
 *
 * @author Pavlo Ilin
 * @version 1.0
 */
public class User {
    private int id;
    private String login;
    private String email;
    private String password;
    private String phoneNumber;
    private Role role;
    private String firstName;
    private String lastName;
    private Timestamp blockedAt;

    public User(int id, String login, String email, String password, String phoneNumber, Role role, String firstName,
                String lastName, Timestamp blockedAt) {
        this.id = id;
        this.login = login;
        this.email = email;
        this.password = password;
        this.phoneNumber = phoneNumber;
        this.role = role;
        this.firstName = firstName;
        this.lastName = lastName;
        this.blockedAt = blockedAt;
    }

    public User() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Timestamp getBlockedAt() {
        return blockedAt;
    }

    public void setBlockedAt(Timestamp blockedAt) {
        this.blockedAt = blockedAt;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return Objects.equals(login, user.login) && Objects.equals(email, user.email) &&
                Objects.equals(password, user.password) && Objects.equals(phoneNumber, user.phoneNumber) &&
                role == user.role && Objects.equals(firstName, user.firstName) &&
                Objects.equals(lastName, user.lastName) && Objects.equals(blockedAt, user.blockedAt);
    }

    @Override
    public int hashCode() {
        return Objects.hash(login, email, password, phoneNumber, role, firstName, lastName, blockedAt);
    }
}
