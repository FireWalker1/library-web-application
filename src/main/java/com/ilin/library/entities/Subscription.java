package com.ilin.library.entities;

import com.ilin.library.entities.enums.Status;
import com.ilin.library.entities.enums.Type;

import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;
import java.util.Objects;

/**
 * Subscription class. Entity class that matches "subscription" table in the database.
 * Standard constructors, getters and setters are used.
 *
 * @author Pavlo Ilin
 * @version 1.0
 */
public class Subscription {
    private int id;
    private User user;
    private Type type;
    private Status status;
    private Timestamp createdAt;
    private Timestamp updatedAt;
    private Date bookReturnDeadline;
    private BigDecimal fine;
    private List<Book> books;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Timestamp getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Timestamp createdAt) {
        this.createdAt = createdAt;
    }

    public Timestamp getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Timestamp updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Date getBookReturnDeadline() {
        return bookReturnDeadline;
    }

    public void setBookReturnDeadline(Date bookReturnDeadline) {
        this.bookReturnDeadline = bookReturnDeadline;
    }

    public BigDecimal getFine() {
        return fine;
    }

    public void setFine(BigDecimal fine) {
        this.fine = fine;
    }

    public List<Book> getBooks() {
        return books;
    }

    public void setBooks(List<Book> books) {
        this.books = books;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Subscription that = (Subscription) o;
        return Objects.equals(user, that.user) && type == that.type && status == that.status &&
                Objects.equals(createdAt, that.createdAt) && Objects.equals(updatedAt, that.updatedAt) &&
                Objects.equals(bookReturnDeadline, that.bookReturnDeadline) && Objects.equals(fine, that.fine) &&
                Objects.equals(books, that.books);
    }

    @Override
    public int hashCode() {
        return Objects.hash(user, type, status, createdAt, updatedAt, bookReturnDeadline, fine, books);
    }
}
