package com.ilin.library.entities.enums;

/**
 * Role enum. Enum matches "user.role" column in the database.
 *
 * @author Pavlo Ilin
 * @version 1.0
 */
public enum Role {
    READER,
    LIBRARIAN,
    ADMINISTRATOR
}
