package com.ilin.library.entities.enums;

/**
 * Type enum. Enum matches "subscription.type" column in the database.
 *
 * @author Pavlo Ilin
 * @version 1.0
 */
public enum Type {
    READING_ROOM,
    HOME
}
