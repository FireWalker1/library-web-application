package com.ilin.library.entities.enums;

/**
 * Language enum. Enum matches "book.language" column in the database.
 *
 * @author Pavlo Ilin
 * @version 1.0
 */
public enum Language {
    UA,
    EN
}
