package com.ilin.library.entities.enums;

/**
 * Status enum. Enum matches "subscription.status" column in the database.
 *
 * @author Pavlo Ilin
 * @version 1.0
 */
public enum Status {
    REQUESTED,
    CONFIRMED,
    ONGOING,
    CLOSED
}
