package com.ilin.library.filters;

import jakarta.servlet.*;
import jakarta.servlet.annotation.WebFilter;

import java.io.IOException;

/**
 * EncodingFilter class. Setting UTF-8 encoding to all requests and responses.
 *
 * @author Pavlo Ilin
 * @version 1.0
 */
@WebFilter("/*")
public class EncodingFilter implements Filter {

    /**
     * Sets UTF-8 encoding for any values from user.
     *
     * @param request  passed by application
     * @param response passed by application
     * @param chain    passed by application
     */
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {
        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");
        chain.doFilter(request, response);
    }
}