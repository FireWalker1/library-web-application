package com.ilin.library.filters.security;

import com.ilin.library.controller.Path;

import java.util.ArrayList;
import java.util.List;

/**
 * Access class. Contains lists with different access layers for each Role.
 * While adding new Command or .jsp page, it should be placed here as well.
 *
 * @author Pavlo Ilin
 * @version 1.0
 */
public class Access {

    public static final List<String> unrestrictedAddressList = new ArrayList<>();
    public static final List<String> commonAddressList = new ArrayList<>();
    public static final List<String> readerAddressList = new ArrayList<>();
    public static final List<String> librarianAddressList = new ArrayList<>();
    public static final List<String> administratorAddressList = new ArrayList<>();

    static {
        unrestrictedAddressList.add(Path.PAGE_404);
        unrestrictedAddressList.add(Path.PAGE_ACCESS_DENIED);
        unrestrictedAddressList.add(Path.PAGE_CONTACTS);
        unrestrictedAddressList.add(Path.PAGE_ERROR);
        unrestrictedAddressList.add(Path.PAGE_LOGIN);
        unrestrictedAddressList.add(Path.PAGE_INDEX);
        unrestrictedAddressList.add(Path.PAGE_BOOKS);
        unrestrictedAddressList.add(Path.PAGE_SIGN_UP);
        unrestrictedAddressList.add(Path.PAGE_SUCCESS);
        unrestrictedAddressList.add(Path.COMMAND_BOOK_SEARCH);
        unrestrictedAddressList.add(Path.COMMAND_CHANGE_LANGUAGE);
        unrestrictedAddressList.add(Path.COMMAND_LOGIN);
        unrestrictedAddressList.add(Path.COMMAND_SIGN_UP);


        commonAddressList.add(Path.PAGE_PROFILE);
        commonAddressList.add(Path.PAGE_SUBSCRIPTION_INFO);
        commonAddressList.add(Path.COMMAND_CHANGE_USER_FIELD);
        commonAddressList.add(Path.COMMAND_GET_SUBSCRIPTION_INFO);
        commonAddressList.add(Path.COMMAND_GET_USER);
        commonAddressList.add(Path.COMMAND_LOGOUT);
        commonAddressList.add(Path.COMMAND_PRINT_PDF_SUBSCRIPTION_INFO);


        readerAddressList.add(Path.PAGE_CART);
        readerAddressList.add(Path.PAGE_MY_SUBSCRIPTIONS);
        readerAddressList.add(Path.COMMAND_ADD_BOOK_TO_CART);
        readerAddressList.add(Path.COMMAND_DELETE_BOOK_FROM_CART);
        readerAddressList.add(Path.COMMAND_GET_MY_SUBSCRIPTIONS);
        readerAddressList.add(Path.COMMAND_ORDER_BOOKS);
        readerAddressList.add(Path.COMMAND_PAY_FINE);


        librarianAddressList.add(Path.PAGE_ALL_SUBSCRIPTIONS);
        librarianAddressList.add(Path.PAGE_USERS);
        librarianAddressList.add(Path.COMMAND_CONFIRM_SUBSCRIPTION);
        librarianAddressList.add(Path.COMMAND_GET_ALL_READERS);
        librarianAddressList.add(Path.COMMAND_GET_ALL_SUBSCRIPTIONS);
        librarianAddressList.add(Path.COMMAND_GET_READER_SUBSCRIPTIONS);
        librarianAddressList.add(Path.COMMAND_REJECT_SUBSCRIPTION);


        administratorAddressList.add(Path.PAGE_ADD_NEW_BOOK);
        administratorAddressList.add(Path.PAGE_EDIT_BOOK);
        administratorAddressList.add(Path.PAGE_USERS);
        administratorAddressList.add(Path.COMMAND_ADD_NEW_BOOK);
        administratorAddressList.add(Path.COMMAND_CHANGE_BOOK_FIELD);
        administratorAddressList.add(Path.COMMAND_CHANGE_USER_BLOCK_STATUS);
        administratorAddressList.add(Path.COMMAND_DELETE_BOOK);
        administratorAddressList.add(Path.COMMAND_DELETE_LIBRARIAN);
        administratorAddressList.add(Path.COMMAND_GET_ALL_USERS);
        administratorAddressList.add(Path.COMMAND_GET_BOOK);
        administratorAddressList.add(Path.COMMAND_REASSIGN_AUTHOR);
    }
}
