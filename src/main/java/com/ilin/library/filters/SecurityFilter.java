package com.ilin.library.filters;

import com.ilin.library.controller.Path;
import com.ilin.library.filters.security.Access;
import jakarta.servlet.*;
import jakarta.servlet.annotation.WebFilter;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpSession;

import java.io.IOException;
import java.util.Objects;

/**
 * SecurityFilter class. Checking whether access for page or action is granted to the certain user.
 * Specific lists with access information are located in Access class.
 *
 * @author Pavlo Ilin
 * @version 1.0
 */
@WebFilter(urlPatterns = "*.jsp", servletNames = "Controller")
public class SecurityFilter implements Filter {

    /**
     * Getting user's role and checking whether access is granted for further actions.
     *
     * @param request  passed by application
     * @param response passed by application
     * @param chain    passed by application
     */
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest httpRequest = (HttpServletRequest) request;
        HttpSession session = httpRequest.getSession();
        String role = (String) session.getAttribute("role");
        String servletPath = httpRequest.getServletPath().substring(1);
        String action = httpRequest.getParameter("action");
        boolean isApproved;

        isApproved = action == null ?
                isAccessGranted(role, servletPath) : isAccessGranted(role, servletPath + "?action=" + action);
        if (isApproved) {
            chain.doFilter(request, response);
        } else {
            request.getRequestDispatcher(Path.PAGE_ACCESS_DENIED).forward(request, response);
        }
    }

    private boolean isAccessGranted(String role, String servletPath) {
        if (Access.unrestrictedAddressList.contains(servletPath)) {
            return true;
        }
        if (role == null && Access.commonAddressList.contains(servletPath)) {
            return false;
        }
        if (!Objects.equals(role, "role.reader") && Access.readerAddressList.contains(servletPath)) {
            return false;
        }
        if (!Objects.equals(role, "role.librarian") && Access.librarianAddressList.contains(servletPath)) {
            return false;
        }
        return Objects.equals(role, "role.administrator") || !Access.administratorAddressList.contains(servletPath);
    }
}
