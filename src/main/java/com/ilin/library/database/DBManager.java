package com.ilin.library.database;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

import javax.sql.DataSource;

/**
 * DBManager class. Configures and obtains DataSource from Hikari Connection Pool.
 *
 * @author Pavlo Ilin
 * @version 1.0
 */
public class DBManager {
    private static DataSource ds;

    private DBManager() {
    }

    /**
     * Configuring DataSource from db.properties file. Implements Singleton pattern.
     *
     * @return DataSource instance
     */
    public static DataSource getDataSource() {
        if (ds == null) {
            HikariConfig config = new HikariConfig("/db.properties");
            config.addDataSourceProperty("cachePrepStmts", "true");
            config.addDataSourceProperty("prepStmtCacheSize", "250");
            config.addDataSourceProperty("prepStmtCacheSqlLimit", "2048");
            config.setDriverClassName("com.mysql.cj.jdbc.Driver");
            ds = new HikariDataSource(config);
        }
        return ds;
    }
}
