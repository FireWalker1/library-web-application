package com.ilin.library.utils;

import com.ilin.library.dto.BookDTO;
import jakarta.servlet.http.HttpSession;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

/**
 * Validator class. Is called from Command classes.
 * Includes collection of static classes for validating data from user for fetched from request or session
 *
 * @author Pavlo Ilin
 * @version 1.0
 */
public class Validator {

    public static final String FIRST_NAME_ERROR = "sign_up.error_not_valid_first_name";
    public static final String LAST_NAME_ERROR = "sign_up.error_not_valid_last_name";
    public static final String LOGIN_ERROR = "sign_up.error_not_valid_login";
    public static final String PASSWORD_ERROR = "sign_up.error_not_valid_password";
    public static final String EMAIL_ERROR = "sign_up.error_not_valid_email";
    public static final String PHONE_NUMBER_ERROR = "sign_up.error_not_valid_phone_number";
    public static final String TITLE_ERROR = "add_new_book.error_not_valid_title";
    public static final String AUTHOR_NAME_ERROR = "add_new_book.error_not_valid_author_name";
    public static final String PUBLISHER_HOUSE_ERROR = "add_new_book.error_not_valid_publisher_house";
    public static final String YEAR_ERROR = "add_new_book.error_not_valid_year";
    public static final String QUANTITY_ERROR = "add_new_book.error_not_valid_quantity";

    public static final String NAME_REGEX = "^[a-zA-ZЄ-ЯҐа-їґ']{2,45}$";
    public static final String LOGIN_REGEX = "^[a-zA-ZЄ-ЯҐа-їґ\\d_-]{3,45}$";
    public static final String PASSWORD_REGEX = "^.{5,45}$";
    public static final String EMAIL_REGEX = "^[a-zA-Z\\d][a-zA-Z\\d.!#$%&'*+-/=?^_`{|}~]*?[a-zA-Z\\d._\\-]?@[a-zA-Z\\d][a-zA-Z\\d._\\-]*?[a-zA-Z\\d]?\\.[a-zA-Z]{2,63}$";
    public static final String PHONE_NUMBER_REGEX = "^\\d{9}$";
    public static final String TITLE_REGEX = "^.{2,100}$";
    public static final String AUTHOR_NAME_REGEX = "^.{2,100}$";
    public static final String PUBLISHER_HOUSE_REGEX = "^.{2,45}$";

    /**
     * Validating user's first name
     *
     * @param firstName first name for validation
     * @return null if name is correct, error String if it isn't
     */
    public static String checkFirstName(String firstName) {
        return checkString(firstName, NAME_REGEX, FIRST_NAME_ERROR);
    }

    /**
     * Validating user's last name
     *
     * @param lastName last name for validation
     * @return null if name is correct, error String if it isn't
     */
    public static String checkLastName(String lastName) {
        return checkString(lastName, NAME_REGEX, LAST_NAME_ERROR);
    }

    /**
     * Validating user's login
     *
     * @param login login for validation
     * @return null if login is correct, error String if it isn't
     */
    public static String checkLogin(String login) {
        return checkString(login, LOGIN_REGEX, LOGIN_ERROR);
    }

    /**
     * Validating user's password
     *
     * @param password password for validation
     * @return null if password is correct, error String if it isn't
     */
    public static String checkPassword(String password) {
        return checkString(password, PASSWORD_REGEX, PASSWORD_ERROR);
    }

    /**
     * Validating user's email
     *
     * @param email email for validation
     * @return null if email is correct, error String if it isn't
     */
    public static String checkEmail(String email) {
        return checkString(email, EMAIL_REGEX, EMAIL_ERROR);
    }

    /**
     * Validating user's phone number
     *
     * @param phoneNumber phone number for validation
     * @return null if phone number is correct, error String if it isn't
     */
    public static String checkPhoneNumber(String phoneNumber) {
        if (phoneNumber == null || phoneNumber.isBlank()) {
            return null;
        }
        return checkString(phoneNumber, PHONE_NUMBER_REGEX, PHONE_NUMBER_ERROR);
    }

    /**
     * Validating user's field
     *
     * @param fieldName  name of the field
     * @param fieldValue value of the field
     * @return null if field is correct, error String if it isn't
     */
    public static String checkUserFieldValue(String fieldName, String fieldValue) {
        if (fieldName.equals("login")) {
            return checkLogin(fieldValue);
        }
        if (fieldName.equals("email")) {
            return checkEmail(fieldValue);
        }
        if (fieldName.equals("password")) {
            return checkPassword(fieldValue);
        }
        if (fieldName.equals("phoneNumber")) {
            return checkPhoneNumber(fieldValue);
        }
        if (fieldName.equals("firstName")) {
            return checkFirstName(fieldValue);
        } else {
            return checkLastName(fieldValue);
        }
    }

    /**
     * Validating book's field
     *
     * @param fieldName  name of the field
     * @param fieldValue value of the field
     * @return null if field is correct, error String if it isn't
     */
    public static String checkBookFieldValue(String fieldName, String fieldValue) {
        if (fieldName.equals("title")) {
            return checkTitle(fieldValue);
        }
        if (fieldName.equals("authorNameUA")) {
            return checkAuthorNameUA(fieldValue);
        }
        if (fieldName.equals("authorNameEN")) {
            return checkAuthorNameEN(fieldValue);
        }
        if (fieldName.equals("publisherHouse")) {
            return checkPublisherHouse(fieldValue);
        }
        if (fieldName.equals("year")) {
            return checkYear(validateInt(fieldValue));
        } else {
            return checkQuantity(validateInt(fieldValue));
        }
    }

    /**
     * Validating book's title
     *
     * @param title title for validation
     * @return null if title is correct, error String if it isn't
     */
    public static String checkTitle(String title) {
        return checkString(title, TITLE_REGEX, TITLE_ERROR);
    }

    /**
     * Validating author's name UA
     *
     * @param authorNameUA name UA for validation
     * @return null if name is correct, error String if it isn't
     */
    public static String checkAuthorNameUA(String authorNameUA) {
        return checkString(authorNameUA, AUTHOR_NAME_REGEX, AUTHOR_NAME_ERROR);
    }

    /**
     * Validating author's name EN
     *
     * @param authorNameEN name EN for validation
     * @return null if name is correct, error String if it isn't
     */
    public static String checkAuthorNameEN(String authorNameEN) {
        return checkString(authorNameEN, AUTHOR_NAME_REGEX, AUTHOR_NAME_ERROR);
    }

    /**
     * Validating book's publisher house
     *
     * @param publisherHouse publisher house for validation
     * @return null if publisher house is correct, error String if it isn't
     */
    public static String checkPublisherHouse(String publisherHouse) {
        return checkString(publisherHouse, PUBLISHER_HOUSE_REGEX, PUBLISHER_HOUSE_ERROR);
    }

    /**
     * Validating book's year
     *
     * @param year year for validation
     * @return null if year is correct, error String if it isn't
     */
    public static String checkYear(int year) {
        if (year > 1900 && year <= LocalDate.now().getYear()) {
            return null;
        } else {
            return YEAR_ERROR;
        }
    }

    /**
     * Validating book's quantity
     *
     * @param quantity quantity for validation
     * @return null if quantity is correct, error String if it isn't
     */
    public static String checkQuantity(int quantity) {
        if (quantity >= 0) {
            return null;
        } else {
            return QUANTITY_ERROR;
        }
    }

    /**
     * Validating integer while parsing from String
     *
     * @param number String integer for validation
     * @return parsed integer if number is correct, 0 if not
     */
    public static int validateInt(String number) {
        try {
            return Integer.parseInt(number);
        } catch (NumberFormatException | NullPointerException e) {
            return -1;
        }
    }

    /**
     * Validating BookTO list while fetching from session
     *
     * @param session       provided by Command
     * @param attributeName name of the attribute
     * @return if success - filled BookDTO list, if not - new list
     */
    public static List<BookDTO> validateBookListAttribute(HttpSession session, String attributeName) {
        Object attributeObj = session.getAttribute(attributeName);
        if (attributeObj instanceof ArrayList) {
            return (ArrayList<BookDTO>) attributeObj;
        } else {
            return new ArrayList<>();
        }
    }

    private static String checkString(String string, String regex, String errorText) {
        if (string == null || string.isEmpty()) {
            return errorText;
        }
        if (Pattern.matches(regex, string)) {
            return null;
        } else {
            return errorText;
        }
    }
}
