package com.ilin.library.utils;

import com.ilin.library.dto.AuthorDTO;
import com.ilin.library.dto.BookDTO;
import com.ilin.library.dto.SubscriptionDTO;
import com.ilin.library.dto.UserDTO;
import com.ilin.library.entities.Author;
import com.ilin.library.entities.Book;
import com.ilin.library.entities.Subscription;
import com.ilin.library.entities.User;
import com.ilin.library.entities.enums.Language;
import com.ilin.library.entities.enums.Role;
import com.ilin.library.entities.enums.Status;
import com.ilin.library.entities.enums.Type;

import java.util.ArrayList;
import java.util.List;

/**
 * Converter class. Is called from service classes.
 * Includes collection of static classes for conversion Entities classes to DTO and vice versa.
 *
 * @author Pavlo Ilin
 * @version 1.0
 */
public class Converter {

    /**
     * Converting Book entity to BookDTO
     *
     * @param book Book entity for conversion
     * @return BookDTO after conversion
     */
    public static BookDTO convertBookToDTO(Book book) {
        BookDTO dto = new BookDTO();
        dto.setId(book.getId());
        dto.setTitle(book.getTitle());
        dto.setAuthor(convertAuthorToDTO(book.getAuthor()));
        dto.setPublisherHouse(book.getPublisherHouse());
        dto.setYear(book.getYear());
        dto.setLanguage(book.getLanguage().toString());
        dto.setQuantity(book.getQuantity());
        return dto;
    }

    /**
     * Converting BookDTO to Book entity
     *
     * @param dto BookDTO for conversion
     * @return Book entity after conversion
     */
    public static Book convertDTOToBook(BookDTO dto) {
        Book book = new Book();
        book.setId(dto.getId());
        book.setTitle(dto.getTitle());
        book.setAuthor(convertDTOToAuthor(dto.getAuthor()));
        book.setPublisherHouse(dto.getPublisherHouse());
        book.setYear(dto.getYear());
        book.setLanguage(dto.getLanguage().equals("EN") ? Language.EN : Language.UA);
        book.setQuantity(dto.getQuantity());
        return book;
    }

    /**
     * Converting list of Book entities to BookDTO list
     *
     * @param books list of Book entities for conversion
     * @return list of BookDTOs after conversion
     */
    public static List<BookDTO> convertBookListToDTOList(List<Book> books) {
        List<BookDTO> dtoList = new ArrayList<>();
        if (books != null) {
            books.forEach(x -> dtoList.add(convertBookToDTO(x)));
        }
        return dtoList;
    }

    /**
     * Converting list of BookDTO to Book entities list
     *
     * @param dtoList list of BookDTOs for conversion
     * @return list of Book entities after conversion
     */
    public static List<Book> convertDTOListToBookList(List<BookDTO> dtoList) {
        List<Book> books = new ArrayList<>();
        if (dtoList != null) {
            dtoList.forEach(x -> books.add(convertDTOToBook(x)));
        }
        return books;
    }

    /**
     * Converting Author entity to AuthorDTO
     *
     * @param author Author entity for conversion
     * @return AuthorDTO after conversion
     */
    public static AuthorDTO convertAuthorToDTO(Author author) {
        return new AuthorDTO(author.getId(), author.getNameEN(), author.getNameUA());
    }

    /**
     * Converting AuthorDTO to Author entity
     *
     * @param dto AuthorDTO for conversion
     * @return Author entity after conversion
     */
    public static Author convertDTOToAuthor(AuthorDTO dto) {
        return new Author(dto.getId(), dto.getNameEN(), dto.getNameUA());
    }

    /**
     * Converting User entity to UserDTO
     *
     * @param user User entity for conversion
     * @return UserDTO after conversion
     */
    public static UserDTO convertUserToDTO(User user) {
        UserDTO dto = new UserDTO();
        if (user == null) {
            user = new User();
        }
        dto.setId(user.getId());
        dto.setLogin(user.getLogin());
        dto.setEmail(user.getEmail());
        dto.setPassword(user.getPassword());
        dto.setPhoneNumber(user.getPhoneNumber());
        Role role = user.getRole();
        if (role == null) {
            dto.setRole(null);
        } else if (role == Role.READER) {
            dto.setRole("role.reader");
        } else if (role == Role.LIBRARIAN) {
            dto.setRole("role.librarian");
        } else if (role == Role.ADMINISTRATOR) {
            dto.setRole("role.administrator");
        } else {
            dto.setRole(null);
        }
        dto.setFirstName(user.getFirstName());
        dto.setLastName(user.getLastName());
        dto.setBlockedAt(user.getBlockedAt());
        return dto;
    }

    /**
     * Converting UserDTO to User entity
     *
     * @param dto UserDTO for conversion
     * @return User entity after conversion
     */
    public static User convertDTOToUser(UserDTO dto) {
        User user = new User();
        if (dto == null) {
            dto = new UserDTO();
        }
        user.setId(dto.getId());
        user.setLogin(dto.getLogin());
        user.setEmail(dto.getEmail());
        user.setPassword(dto.getPassword());
        user.setPhoneNumber(dto.getPhoneNumber());
        String role = dto.getRole();
        if (role == null) {
            user.setRole(null);
        } else if (role.equals("role.reader")) {
            user.setRole(Role.READER);
        } else if (role.equals("role.librarian")) {
            user.setRole(Role.LIBRARIAN);
        } else if (role.equals("role.administrator")) {
            user.setRole(Role.ADMINISTRATOR);
        } else {
            user.setRole(null);
        }
        user.setFirstName(dto.getFirstName());
        user.setLastName(dto.getLastName());
        user.setBlockedAt(dto.getBlockedAt());
        return user;
    }

    /**
     * Converting list of User entities to UserDTO list
     *
     * @param users list of User entities for conversion
     * @return list of UserDTOs after conversion
     */
    public static List<UserDTO> convertUserListToDTOList(List<User> users) {
        List<UserDTO> dtoList = new ArrayList<>();
        if (users != null) {
            users.forEach(x -> dtoList.add(convertUserToDTO(x)));
        }
        return dtoList;
    }

    /**
     * Converting Subscription entity to SubscriptionDTO
     *
     * @param subscription Subscription entity for conversion
     * @return SubscriptionDTO after conversion
     */
    public static SubscriptionDTO convertSubscriptionToDTO(Subscription subscription) {
        SubscriptionDTO dto = new SubscriptionDTO();
        dto.setId(subscription.getId());
        dto.setUser(convertUserToDTO(subscription.getUser()));
        dto.setType(subscription.getType().equals(Type.HOME) ? "type.home" : "type.reading_room");
        Status status = subscription.getStatus();
        if (status == null) {
            dto.setStatus(null);
        } else if (status.equals(Status.REQUESTED)) {
            dto.setStatus("status.requested");
        } else if (status.equals(Status.CONFIRMED)) {
            dto.setStatus("status.confirmed");
        } else if (status.equals(Status.ONGOING)) {
            dto.setStatus("status.ongoing");
        } else if (status.equals(Status.CLOSED)) {
            dto.setStatus("status.closed");
        } else {
            dto.setStatus(null);
        }
        dto.setCreatedAt(subscription.getCreatedAt());
        dto.setUpdatedAt(subscription.getUpdatedAt());
        dto.setBookReturnDeadline(subscription.getBookReturnDeadline());
        dto.setFine(subscription.getFine());
        dto.setBooks(convertBookListToDTOList(subscription.getBooks()));
        return dto;
    }

    /**
     * Converting SubscriptionDTO to Subscription entity
     *
     * @param dto SubscriptionDTO for conversion
     * @return Subscription entity after conversion
     */
    public static Subscription convertDTOToSubscription(SubscriptionDTO dto) {
        Subscription subscription = new Subscription();
        subscription.setId(dto.getId());
        subscription.setUser(convertDTOToUser(dto.getUser()));
        subscription.setType(dto.getType().equals("type.home") ? Type.HOME : Type.READING_ROOM);
        String status = dto.getStatus();
        if (status == null) {
            subscription.setStatus(null);
        } else if (status.equals("status.requested")) {
            subscription.setStatus(Status.REQUESTED);
        } else if (status.equals("status.confirmed")) {
            subscription.setStatus(Status.CONFIRMED);
        } else if (status.equals("status.ongoing")) {
            subscription.setStatus(Status.ONGOING);
        } else if (status.equals("status.closed")) {
            subscription.setStatus(Status.CLOSED);
        } else {
            subscription.setStatus(null);
        }
        subscription.setCreatedAt(dto.getCreatedAt());
        subscription.setUpdatedAt(dto.getUpdatedAt());
        subscription.setBookReturnDeadline(dto.getBookReturnDeadline());
        subscription.setFine(dto.getFine());
        subscription.setBooks(convertDTOListToBookList(dto.getBooks()));
        return subscription;
    }

    /**
     * Converting list of Subscription entities to SubscriptionDTO list
     *
     * @param subscriptions list of Subscription entities for conversion
     * @return list of SubscriptionDTOs after conversion
     */
    public static List<SubscriptionDTO> convertSubscriptionListToDTOList(List<Subscription> subscriptions) {
        List<SubscriptionDTO> dtoList = new ArrayList<>();
        if (subscriptions != null) {
            subscriptions.forEach(x -> dtoList.add(convertSubscriptionToDTO(x)));
        }
        return dtoList;
    }
}
