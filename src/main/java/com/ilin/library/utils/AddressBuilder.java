package com.ilin.library.utils;

import com.ilin.library.controller.Path;
import jakarta.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.util.Map;

/**
 * Utils class. Is called from Command classes.
 * Includes collection of static classes for proper building of redirection address and its parameters.
 *
 * @author Pavlo Ilin
 * @version 1.0
 */
public class AddressBuilder {
    private static final Logger logger = LogManager.getLogger(AddressBuilder.class);

    /**
     * Forming URL address properties for Command
     *
     * @param properties properties from original Request
     * @param info       Map with set of information and/or error text that should be added to address
     * @return formed URL address properties part
     */
    public static String formCommandProperties(String properties, Map<String, String> info) {
        StringBuilder line = new StringBuilder();
        if (properties != null && properties.indexOf('&') != -1) {
            line.append(properties.substring(properties.indexOf('&')));
        }
        if (info == null || info.isEmpty()) {
            return line.toString();
        }
        info.forEach((x, y) -> line.append("&").append(x).append("=").append(y));
        return line.toString();
    }

    /**
     * Forming URL address properties for pages
     *
     * @param properties properties from original Request
     * @param info       Map with set of information and/or error text that should be added to address
     * @return formed URL address properties part
     */
    public static String formPageProperties(String properties, Map<String, String> info) {
        StringBuilder line = new StringBuilder();
        if (properties != null && properties.indexOf('&') != -1) {
            line.append("?").append(properties.substring(properties.indexOf('&') + 1)).append("&");
        } else {
            line.append("?");
        }
        if (info == null || info.isEmpty()) {
            return line.toString();
        }
        info.forEach((x, y) -> line.append(x).append("=").append(y).append("&"));
        line.deleteCharAt(line.length() - 1);
        return line.toString();
    }

    /**
     * Logging necessary error information and redirecting according to PRG pattern
     *
     * @param response provided by application
     * @param message  error message to log
     * @return Redirect String
     */
    public static String logAndRedirectToErrorPage(String message, HttpServletResponse response) throws IOException {
        response.sendRedirect(Path.PAGE_ERROR);
        logger.error(message);
        return Path.REDIRECT;
    }
}
