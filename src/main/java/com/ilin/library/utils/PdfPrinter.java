package com.ilin.library.utils;

import com.ilin.library.dto.BookDTO;
import com.ilin.library.dto.SubscriptionDTO;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.stream.Stream;

/**
 * PdfPrinter class. Is called from Command classes.
 * Includes collection of static classes for creating pdf file with indicated information from the database.
 *
 * @author Pavlo Ilin
 * @version 1.0
 */
public class PdfPrinter {

    /**
     * Creating pdf file with information about certain subscription
     *
     * @param dto      subscription dto with all necessary information
     * @param language locale name
     * @return stream with pdf file
     */
    public static ByteArrayOutputStream printSubscriptionInfo(SubscriptionDTO dto, String language)
            throws DocumentException, IOException {
        ResourceBundle bundle = ResourceBundle.getBundle("resources", Locale.forLanguageTag(language));
        ByteArrayOutputStream output = new ByteArrayOutputStream();
        Document document = new Document(PageSize.A4.rotate());
        PdfWriter.getInstance(document, output);

        document.open();
        fillingDocument(dto, language, bundle, document);
        document.close();
        return output;
    }

    private static void fillingDocument(SubscriptionDTO dto, String language, ResourceBundle bundle, Document document)
            throws DocumentException, IOException {
        BaseFont baseFontBold =
                BaseFont.createFont("fonts/HelveticaNeueCyr-Bold.ttf", "Cp1251", BaseFont.EMBEDDED);
        BaseFont baseFontMedium =
                BaseFont.createFont("fonts/HelveticaNeueCyr-Medium.ttf", "Cp1251", BaseFont.EMBEDDED);
        Font fontBold = new Font(baseFontBold, 16);
        Font fontMedium = new Font(baseFontMedium, 10);

        Paragraph paragraph = new Paragraph(bundle.getString("title.subscription_info") + " #" +
                dto.getId(), fontBold);
        document.add(paragraph);
        document.add(new Paragraph("\n\n\n"));

        String mainInfo = getMainInfo(dto, bundle);
        paragraph = new Paragraph(mainInfo, fontMedium);
        document.add(paragraph);
        document.add(new Paragraph("\n\n\n\n"));

        PdfPTable table = new PdfPTable(6);
        table.setWidthPercentage(100);
        addTableHeader(table, fontMedium, bundle);
        addRows(table, fontMedium, dto.getBooks(), language);

        document.add(table);
    }

    private static String getMainInfo(SubscriptionDTO dto, ResourceBundle bundle) {
        StringBuilder builder = new StringBuilder();
        Date dateCreatedAt = new Date(dto.getCreatedAt().getTime());
        Date dateUpdatedAt = new Date(dto.getUpdatedAt().getTime());
        builder.append(bundle.getString("my_subscriptions.id")).append(" - ").append(dto.getId()).append("\n")
                .append(bundle.getString("my_subscriptions.type"))
                .append(" - ").append(bundle.getString(dto.getType())).append("\n")
                .append(bundle.getString("my_subscriptions.status"))
                .append(" - ").append(bundle.getString(dto.getStatus())).append("\n")
                .append(bundle.getString("my_subscriptions.created")).append(" - ")
                .append(new SimpleDateFormat("HH:mm dd.MM.yy").format(dateCreatedAt)).append("\n")
                .append(bundle.getString("my_subscriptions.updated")).append(" - ")
                .append(new SimpleDateFormat("HH:mm dd.MM.yy").format(dateUpdatedAt)).append("\n")
                .append(bundle.getString("my_subscriptions.return_due")).append(" - ");

        if (dto.getBookReturnDeadline() == null) {
            builder.append("N/A").append("\n");
        } else {
            builder.append(dto.getBookReturnDeadline()).append("\n");
        }
        builder.append(bundle.getString("my_subscriptions.fine")).append(" - ")
                .append(dto.getFine()).append(" UAH\n");
        return builder.toString();
    }

    private static void addTableHeader(PdfPTable table, Font fontMedium, ResourceBundle bundle) {
        Stream.of(bundle.getString("books.book_id"),
                        bundle.getString("books.title"),
                        bundle.getString("books.author"),
                        bundle.getString("books.publisher"),
                        bundle.getString("books.year"),
                        bundle.getString("books.language"))
                .forEach(columnTitle -> {
                    PdfPCell header = new PdfPCell();
                    header.setBackgroundColor(BaseColor.LIGHT_GRAY);
                    header.setBorderWidth(2);
                    header.setPhrase(new Phrase(columnTitle, fontMedium));
                    table.addCell(header);
                });
    }

    private static void addRows(PdfPTable table, Font fontMedium, List<BookDTO> books, String language) {
        books.forEach(book -> {
            PdfPCell cell = new PdfPCell();
            cell.setBorderWidth(2);
            cell.setPhrase(new Phrase(String.valueOf(book.getId()), fontMedium));
            table.addCell(cell);
            cell.setPhrase(new Phrase(book.getTitle(), fontMedium));
            table.addCell(cell);
            if (language != null && language.equals("en")) {
                cell.setPhrase(new Phrase(book.getAuthor().getNameEN(), fontMedium));
            } else {
                cell.setPhrase(new Phrase(book.getAuthor().getNameUA(), fontMedium));
            }
            table.addCell(cell);
            cell.setPhrase(new Phrase(book.getPublisherHouse(), fontMedium));
            table.addCell(cell);
            cell.setPhrase(new Phrase(String.valueOf(book.getYear()), fontMedium));
            table.addCell(cell);
            cell.setPhrase(new Phrase(book.getLanguage(), fontMedium));
            table.addCell(cell);
        });
    }
}
