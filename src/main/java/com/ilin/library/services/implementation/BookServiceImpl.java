package com.ilin.library.services.implementation;

import com.ilin.library.dao.BookDAO;
import com.ilin.library.dto.AuthorDTO;
import com.ilin.library.dto.BookDTO;
import com.ilin.library.entities.Author;
import com.ilin.library.entities.Book;
import com.ilin.library.entities.enums.Language;
import com.ilin.library.services.BookService;
import com.ilin.library.utils.Converter;

import java.util.List;

/**
 * BookService class. Is called from Command classes.
 * Doing specific business logic and sending it to appropriate DAO class.
 *
 * @author Pavlo Ilin
 * @version 1.0
 */
public class BookServiceImpl implements BookService {
    private final BookDAO bookDAO;

    /**
     * Constructor to use in all cases except testing
     */
    public BookServiceImpl() {
        bookDAO = new BookDAO();
    }

    /**
     * Constructor to use during testing
     *
     * @param bookDAO is mocked DAO
     */
    public BookServiceImpl(BookDAO bookDAO) {
        this.bookDAO = bookDAO;
    }

    /**
     * Gets books from the database according to the indicated parameters.
     *
     * @param orderBy     name of column in the database for ordering
     * @param desc        ascending or descending type ("true" or "false" possible)
     * @param pageNumber  number of required page (fetching only 10 items per statement)
     * @param searchWords array of Strings for searching among all results
     * @return list of books that were fetched from the database. If list is empty - no matches found.
     * If null - incorrect execution of the statement.
     */
    @Override
    public List<BookDTO> getSearchResultList(String[] searchWords, String orderBy, String desc, int pageNumber) {
        List<Book> books = bookDAO.getSearchResultList(orderBy, desc, pageNumber, searchWords);
        return Converter.convertBookListToDTOList(books);
    }

    /**
     * Gets all books from the database without filtering.
     *
     * @return list of books that were fetched from the database. If list is empty - no matches found.
     * If null - incorrect execution of the statement.
     */
    @Override
    public List<BookDTO> getFullList() {
        List<Book> books = bookDAO.getFullList();
        return Converter.convertBookListToDTOList(books);
    }

    /**
     * Adding indicated book to the user's book cart list.
     *
     * @param bookID ID of the required book
     * @return book that was fetched from the database. If null - no matches found.
     */
    @Override
    public BookDTO addBookToCart(int bookID) {
        Book book = bookDAO.getByID(bookID);
        if (book.getTitle() == null) {
            return null;
        }
        int quantity = book.getQuantity();
        if (quantity > 0 && setBookQuantity(bookID, --quantity)) {
            book.setQuantity(quantity);
            return Converter.convertBookToDTO(book);
        } else {
            return null;
        }
    }

    /**
     * Gets book from the database according to the indicated ID.
     *
     * @param bookID ID of the required book
     * @return book that was fetched from the database. If null - no matches found.
     */
    @Override
    public BookDTO getByID(int bookID) {
        Book book = bookDAO.getByID(bookID);
        if (book.getTitle() == null) {
            return null;
        } else {
            return Converter.convertBookToDTO(book);
        }
    }

    /**
     * Deleting indicated book to the user's book cart list.
     *
     * @param dto book that will be deleted
     * @return true or false depending on success of operation
     */
    @Override
    public boolean deleteBookFromCart(BookDTO dto) {
        Book book = Converter.convertDTOToBook(dto);
        int quantity = book.getId() == 0 ? -1 : book.getQuantity();
        if (quantity != -1 && setBookQuantity(book.getId(), ++quantity)) {
            book.setQuantity(quantity);
            return true;
        } else {
            return false;
        }
    }

    /**
     * Gets quantity of pages of the books from the database without parameters.
     *
     * @return Integer with quantity of pages. If empty - no matches found.
     * If null - incorrect execution of the statement.
     */
    @Override
    public int getPageQuantity() {
        return getPageQuantity(new String[0]);
    }

    /**
     * Gets quantity of pages of the books from the database according to the indicated parameters.
     *
     * @param split array of Strings for searching among all results
     * @return Integer with quantity of pages. If empty - no matches found.
     * If null - incorrect execution of the statement.
     */
    @Override
    public int getPageQuantity(String[] split) {
        return (int) Math.ceil(bookDAO.getRowsQuantity(split) / 10.0);
    }

    /**
     * Creating BookDTO from the parameters that were put in the method and returning it.
     *
     * @return book that was created from the parameters
     */
    @Override
    public BookDTO createBook(String title, String authorNameUA, String authorNameEN, String publisherHouse, int year,
                              String language, int quantity) {
        AuthorDTO author = new AuthorDTO(0, authorNameEN, authorNameUA);
        return new BookDTO(0, title, author, publisherHouse, year, language, quantity);
    }

    /**
     * Checking whether indicated book exists in the database.
     *
     * @param dto book that will be checked
     * @return true or false depending on success of operation
     */
    @Override
    public boolean isSuchBookExists(BookDTO dto) {
        Book book = Converter.convertDTOToBook(dto);
        return bookDAO.getBook(book).getTitle() != null;
    }

    /**
     * Checking whether indicated book with new indicated parameter exists in the database.
     *
     * @param dto        book that will be checked
     * @param fieldName  name of the book field
     * @param fieldValue value of the book field
     * @return true or false depending on success of operation
     */
    @Override
    public boolean isSuchBookExists(BookDTO dto, String fieldName, String fieldValue) {
        if (fieldName.equals("title")) {
            dto.setTitle(fieldValue);
        }
        if (fieldName.equals("authorNameUA")) {
            dto.getAuthor().setNameUA(fieldValue);
        }
        if (fieldName.equals("authorNameEN")) {
            dto.getAuthor().setNameEN(fieldValue);
        }
        if (fieldName.equals("publisherHouse")) {
            dto.setPublisherHouse(fieldValue);
        }
        if (fieldName.equals("year")) {
            dto.setYear(Integer.parseInt(fieldValue));
        }
        if (fieldName.equals("language")) {
            dto.setLanguage(fieldValue);
        }
        if (fieldName.equals("quantity")) {
            dto.setQuantity(Integer.parseInt(fieldValue));
        }
        return isSuchBookExists(dto);
    }

    /**
     * Inserting indicated book to the database.
     *
     * @param dto book that will be inserted
     * @return true or false depending on success of operation
     */
    @Override
    public boolean insertBook(BookDTO dto) {
        Book book = Converter.convertDTOToBook(dto);
        bookDAO.insertAuthor(book.getAuthor());
        return bookDAO.insertBook(book);
    }

    /**
     * Updating field of indicated book in the database.
     *
     * @param id         ID of the book that will be updated
     * @param fieldName  name of the book field
     * @param fieldValue value of the book field
     * @return true or false depending on success of operation
     */
    @Override
    public boolean updateBook(int id, String fieldName, String fieldValue) {
        if (fieldName.equals("title")) {
            return bookDAO.updateTitle(id, fieldValue);
        }
        if (fieldName.equals("authorNameUA")) {
            return bookDAO.updateAuthorNameUA(id, fieldValue);
        }
        if (fieldName.equals("authorNameEN")) {
            return bookDAO.updateAuthorNameEN(id, fieldValue);
        }
        if (fieldName.equals("publisherHouse")) {
            return bookDAO.updatePublisherHouse(id, fieldValue);
        }
        if (fieldName.equals("year")) {
            return bookDAO.updateYear(id, Integer.parseInt(fieldValue));
        }
        if (fieldName.equals("language")) {
            Language lang = fieldValue.equals("EN") ? Language.EN : Language.UA;
            return bookDAO.updateLanguage(id, lang);
        } else {
            return bookDAO.updateQuantity(id, Integer.parseInt(fieldValue));
        }
    }

    /**
     * Deleting indicated book from the database.
     *
     * @param id ID of the book that will be deleted
     * @return true or false depending on success of operation
     */
    @Override
    public boolean deleteBook(int id) {
        if (id == 0) {
            return false;
        }
        return bookDAO.deleteBook(id);
    }

    /**
     * Updating quantity of indicated book
     *
     * @param id       ID of the book that will be updated
     * @param quantity new quantity
     * @return true or false depending on success of operation
     */
    private boolean setBookQuantity(int id, int quantity) {
        return bookDAO.updateQuantity(id, quantity);
    }

    /**
     * Reassigning author for the existing book
     *
     * @param dto author that will be altered
     * @param id  ID of the book that will be updated
     * @return true or false depending on success of operation
     */
    @Override
    public boolean reassignAuthor(AuthorDTO dto, int id) {
        Author author = Converter.convertDTOToAuthor(dto);
        bookDAO.insertAuthor(author);
        return bookDAO.updateAuthor(author, id);
    }
}