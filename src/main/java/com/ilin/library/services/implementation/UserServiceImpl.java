package com.ilin.library.services.implementation;

import com.ilin.library.dao.UserDAO;
import com.ilin.library.dto.UserDTO;
import com.ilin.library.entities.User;
import com.ilin.library.entities.enums.Role;
import com.ilin.library.services.UserService;
import com.ilin.library.utils.Converter;
import org.mindrot.jbcrypt.BCrypt;

import java.sql.Timestamp;
import java.util.List;

/**
 * UserService class. Is called from Command classes.
 * Doing specific business logic and sending it to appropriate DAO class.
 *
 * @author Pavlo Ilin
 * @version 1.0
 */
public class UserServiceImpl implements UserService {

    private final UserDAO userDAO;

    /**
     * Constructor to use in all cases except testing
     */
    public UserServiceImpl() {
        this.userDAO = new UserDAO();
    }

    /**
     * Constructor to use during testing
     *
     * @param userDAO is mocked DAO
     */
    public UserServiceImpl(UserDAO userDAO) {
        this.userDAO = userDAO;
    }

    /**
     * Gets all users from the database with indicated ordering.
     *
     * @param orderBy    name of column in the database for ordering
     * @param desc       ascending or descending type ("true" or "false" possible)
     * @param pageNumber number of required page (fetching only 10 items per statement)
     * @return list of users that were fetched from the database. If list is empty - no matches found.
     * If null - incorrect execution of the statement.
     */
    @Override
    public List<UserDTO> getFullList(String orderBy, String desc, int pageNumber) {
        if (pageNumber < 1) {
            pageNumber = 1;
        }
        if (orderBy == null || orderBy.isBlank()) {
            List<User> users = userDAO.getFullList(pageNumber);
            return Converter.convertUserListToDTOList(users);
        }
        List<User> users = userDAO.getFullList(orderBy, desc, pageNumber);
        return Converter.convertUserListToDTOList(users);
    }

    /**
     * Gets all users with "reader" role from the database with indicated ordering.
     *
     * @param orderBy    name of column in the database for ordering
     * @param desc       ascending or descending type ("true" or "false" possible)
     * @param pageNumber number of required page (fetching only 10 items per statement)
     * @return list of users that were fetched from the database. If list is empty - no matches found.
     * If null - incorrect execution of the statement.
     */
    @Override
    public List<UserDTO> getFullReadersList(String orderBy, String desc, int pageNumber) {
        if (pageNumber < 1) {
            pageNumber = 1;
        }
        if (orderBy == null || orderBy.isBlank()) {
            List<User> users = userDAO.getFullReadersList(pageNumber);
            return Converter.convertUserListToDTOList(users);
        }
        List<User> users = userDAO.getFullReadersList(orderBy, desc, pageNumber);
        return Converter.convertUserListToDTOList(users);
    }

    /**
     * Gets user from the database according to the indicated login and password.
     *
     * @param login    login of the required user
     * @param password password of the required user
     * @return user that was fetched from the database. If null - no matches found.
     */
    @Override
    public UserDTO getByLoginAndPassword(String login, String password) {
        User user = userDAO.getByLogin(login);
        if (user.getPassword() != null && BCrypt.checkpw(password, user.getPassword())) {
            return Converter.convertUserToDTO(user);
        } else {
            return null;
        }
    }

    /**
     * Gets user from the database according to the indicated login.
     *
     * @param login login of the required user
     * @return user that was fetched from the database. If empty - no matches found.
     * If null - incorrect execution of the statement.
     */
    @Override
    public UserDTO getByLogin(String login) {
        User user = userDAO.getByLogin(login);
        return Converter.convertUserToDTO(user);
    }

    /**
     * Checking whether user with such login exists in the database.
     *
     * @param login login of the required user
     * @return true or false depending on success of operation
     */
    @Override
    public boolean isSuchLoginExists(String login) {
        User user = userDAO.getByLogin(login);
        return user.getLogin() != null;
    }

    /**
     * Checking whether user with such email exists in the database.
     *
     * @param email email of the required user
     * @return true or false depending on success of operation
     */
    @Override
    public boolean isSuchEmailExists(String email) {
        User user = userDAO.getByEmail(email);
        return user.getEmail() != null;
    }

    /**
     * Checking whether user with such phone number exists in the database.
     *
     * @param phoneNumber phone number of the required user
     * @return true or false depending on success of operation
     */
    @Override
    public boolean isSuchPhoneNumberExists(String phoneNumber) {
        User user = userDAO.getByPhoneNumber(phoneNumber);
        return user.getPhoneNumber() != null;
    }

    /**
     * Inserting new user to the database.
     *
     * @param dto user that will be inserted
     * @return true or false depending on success of operation
     */
    @Override
    public boolean insertUser(UserDTO dto) {
        dto.setPassword(BCrypt.hashpw(dto.getPassword(), BCrypt.gensalt()));
        User user = Converter.convertDTOToUser(dto);
        return userDAO.insertUser(user);
    }

    /**
     * Updating field of indicated user in the database.
     *
     * @param userLogin  login of the user that will be updated
     * @param fieldName  name of the user field
     * @param fieldValue value of the user field
     * @return true or false depending on success of operation
     */
    @Override
    public boolean updateUser(String fieldValue, String fieldName, String userLogin) {
        if (fieldName.equals("login")) {
            return userDAO.updateLogin(fieldValue, userLogin);
        }
        if (fieldName.equals("email")) {
            return userDAO.updateEmail(fieldValue, userLogin);
        }
        if (fieldName.equals("password")) {
            fieldValue = BCrypt.hashpw(fieldValue, BCrypt.gensalt());
            return userDAO.updatePassword(fieldValue, userLogin);
        }
        if (fieldName.equals("phoneNumber")) {
            return userDAO.updatePhoneNumber(fieldValue, userLogin);
        }
        if (fieldName.equals("firstName")) {
            return userDAO.updateFirstName(fieldValue, userLogin);
        } else {
            return userDAO.updateLastName(fieldValue, userLogin);
        }
    }

    /**
     * Gets quantity of pages of the users with "readers" role from the database according to the indicated parameters.
     *
     * @return Integer with quantity of pages. If 0 - no matches found.
     */
    @Override
    public int getPageQuantityReaders() {
        return (int) Math.ceil(userDAO.getRowsQuantityReaders() / 10.0);
    }

    /**
     * Gets quantity of pages of the users from the database according to the indicated parameters.
     *
     * @return Integer with quantity of pages. If 0 - no matches found.
     */
    @Override
    public int getPageQuantityUsers() {
        return (int) Math.ceil(userDAO.getRowsQuantityUsers() / 10.0);
    }

    /**
     * Deleting user with indicated login and "librarian" role from the database.
     *
     * @param login login of the required user
     * @return true or false depending on success of operation
     */
    @Override
    public boolean deleteLibrarian(String login) {
        User user = userDAO.getByLogin(login);
        if (user.getRole() == Role.LIBRARIAN) {
            return userDAO.deleteUser(login);
        }
        return false;
    }

    /**
     * Updating block status of user with indicated login on the opposite in the database.
     *
     * @param login login of the required user
     * @return true or false depending on success of operation
     */
    @Override
    public boolean changeBlockStatus(String login) {
        User user = userDAO.getByLogin(login);
        if (user.getBlockedAt() == null) {
            return userDAO.updateBlockedAt(new Timestamp(System.currentTimeMillis()), login);
        } else {
            return userDAO.updateBlockedAt(null, login);
        }
    }
}
