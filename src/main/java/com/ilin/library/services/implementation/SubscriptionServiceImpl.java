package com.ilin.library.services.implementation;

import com.ilin.library.dao.BookDAO;
import com.ilin.library.dao.SubscriptionDAO;
import com.ilin.library.dto.SubscriptionDTO;
import com.ilin.library.entities.Book;
import com.ilin.library.entities.Subscription;
import com.ilin.library.entities.enums.Status;
import com.ilin.library.services.SubscriptionService;
import com.ilin.library.utils.Converter;

import java.math.BigDecimal;
import java.sql.Date;
import java.util.List;

/**
 * SubscriptionService class. Is called from Command classes.
 * Doing specific business logic and sending it to appropriate DAO class.
 *
 * @author Pavlo Ilin
 * @version 1.0
 */
public class SubscriptionServiceImpl implements SubscriptionService {
    private final SubscriptionDAO subscriptionDao;
    private final BookDAO bookDao;

    /**
     * Constructor to use in all cases except testing
     */
    public SubscriptionServiceImpl() {
        bookDao = new BookDAO();
        subscriptionDao = new SubscriptionDAO();
    }

    /**
     * Constructor to use during testing
     *
     * @param subscriptionDao is mocked DAO
     */
    public SubscriptionServiceImpl(SubscriptionDAO subscriptionDao, BookDAO bookDao) {
        this.subscriptionDao = subscriptionDao;
        this.bookDao = bookDao;
    }

    /**
     * Inserting new subscription to the database.
     *
     * @param dto       subscription that will be inserted
     * @param userLogin login of user that will be attached to subscription
     * @return true or false depending on success of operation
     */
    @Override
    public boolean insertSubscription(SubscriptionDTO dto, String userLogin) {
        Subscription subscription = Converter.convertDTOToSubscription(dto);
        return subscriptionDao.insertSubscription(subscription, userLogin);
    }

    /**
     * Gets all subscriptions from the database that related to certain user.
     *
     * @param userLogin login of required user
     * @return list of subscriptions that were fetched from the database. If list is empty - no matches found.
     * If null - incorrect execution of the statement.
     */
    @Override
    public List<SubscriptionDTO> getUserSubscriptions(String userLogin) {
        List<Subscription> subscriptions = subscriptionDao.getUserSubscriptions(userLogin);
        return Converter.convertSubscriptionListToDTOList(subscriptions);
    }

    /**
     * Gets subscription from the database with indicated ID.
     *
     * @param subscriptionID ID of required subscription
     * @return subscription that was fetched from the database. If empty - no matches found.
     * If null - incorrect execution of the statement.
     */
    @Override
    public SubscriptionDTO getSubscriptionByID(int subscriptionID) {
        Subscription subscription = subscriptionDao.getSubscriptionByID(subscriptionID);
        List<Book> books = bookDao.getBySubscriptionID(subscriptionID);
        subscription.setBooks(books);
        return Converter.convertSubscriptionToDTO(subscription);
    }

    /**
     * Gets all subscriptions from the database with indicated ordering.
     *
     * @param orderBy    name of column in the database for ordering
     * @param desc       ascending or descending type ("true" or "false" possible)
     * @param pageNumber number of required page (fetching only 10 items per statement)
     * @return list of subscriptions that were fetched from the database. If list is empty - no matches found.
     * If null - incorrect execution of the statement.
     */
    @Override
    public List<SubscriptionDTO> getFullList(String orderBy, String desc, int pageNumber) {
        if (pageNumber < 1) {
            pageNumber = 1;
        }
        if (orderBy == null || orderBy.isBlank()) {
            List<Subscription> subscriptions = subscriptionDao.getFullList(pageNumber);
            return Converter.convertSubscriptionListToDTOList(subscriptions);
        }
        List<Subscription> subscriptions = subscriptionDao.getFullList(orderBy, desc, pageNumber);
        return Converter.convertSubscriptionListToDTOList(subscriptions);
    }

    /**
     * Gets quantity of pages of the subscriptions from the database.
     *
     * @return Integer with quantity of pages. If empty - no matches found.
     * If null - incorrect execution of the statement.
     */
    @Override
    public int getPageQuantity() {
        return (int) Math.ceil(subscriptionDao.getRowsQuantity() / 10.0);
    }

    /**
     * Progressing status of the subscription with indicated ID in the database.
     *
     * @param subscriptionID   ID of required subscription
     * @param returnDateOption if status should be changed on "Ongoing" then indicates return date option, if not - null
     * @return true or false depending on success of operation
     */
    @Override
    public boolean progressStatus(int subscriptionID, String returnDateOption) {
        Subscription subscription = subscriptionDao.getSubscriptionByID(subscriptionID);
        if (subscription == null || subscription.getStatus() == null) {
            return false;
        }
        Status status = subscription.getStatus();

        if (status.equals(Status.REQUESTED)) {
            return subscriptionDao.updateStatusToConfirmed(subscriptionID);
        }
        if (status.equals(Status.CONFIRMED)) {
            Date date;
            if (returnDateOption == null) {
                return false;
            }
            switch (returnDateOption) {
                case "1":
                    date = new Date(System.currentTimeMillis());
                    break;
                case "2":
                    date = new Date(System.currentTimeMillis() + 3 * 86400000);
                    break;
                case "3":
                    date = new Date(System.currentTimeMillis() + 7 * 86400000);
                    break;
                case "4":
                    date = new Date(System.currentTimeMillis() + 14 * 86400000);
                    break;
                default:
                    return false;
            }
            return subscriptionDao.updateStatusToOngoing(subscriptionID, date);
        }
        if (status.equals(Status.ONGOING)) {
            List<Book> books = bookDao.getBySubscriptionID(subscriptionID);
            return subscriptionDao.updateStatusToClosed(subscriptionID, books);
        }
        return false;
    }

    /**
     * Updating status of the subscription with indicated ID in the database on "Closed".
     *
     * @param subscriptionID ID of required subscription
     * @return true or false depending on success of operation
     */
    @Override
    public boolean rejectSubscription(int subscriptionID) {
        Subscription subscription = subscriptionDao.getSubscriptionByID(subscriptionID);
        if (subscription.getStatus().equals(Status.CLOSED)) {
            return false;
        }
        List<Book> books = bookDao.getBySubscriptionID(subscriptionID);
        return subscriptionDao.updateStatusToClosed(subscriptionID, books);
    }

    /**
     * Updating fine to 0 for the subscription with indicated ID in the database.
     *
     * @param subscriptionID ID of required subscription
     * @return true or false depending on success of operation
     */
    @Override
    public boolean payFine(int subscriptionID) {
        if (subscriptionID == 0) {
            return false;
        }
        return subscriptionDao.updateFine(BigDecimal.ZERO, subscriptionID);
    }

    /**
     * Getting sum of all user fines from all subscriptions from the database.
     *
     * @param userLogin login of required user
     * @return int sum of all user fines
     */
    @Override
    public int getUserFines(String userLogin) {
        return subscriptionDao.getUserFines(userLogin);
    }
}
