package com.ilin.library.services;

import com.ilin.library.dao.SubscriptionDAO;
import com.ilin.library.entities.Subscription;

import java.math.BigDecimal;
import java.sql.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * FineUpdater class. Is called from BackgroundJobManager class.
 * Doing specific business logic and sending it to appropriate DAO class.
 *
 * @author Pavlo Ilin
 * @version 1.0
 */
public class FineUpdater implements Runnable {
    private final SubscriptionDAO subscriptionDAO;

    /**
     * Constructor to use in all cases except testing
     */
    public FineUpdater() {
        subscriptionDAO = new SubscriptionDAO();
    }

    /**
     * Constructor to use during testing
     *
     * @param subscriptionDAO is mocked DAO
     */
    public FineUpdater(SubscriptionDAO subscriptionDAO) {
        this.subscriptionDAO = subscriptionDAO;
    }

    /**
     * Running increasing of the fine for all subscriptions with status "ongoing" and passed book return deadline
     * on 5 units
     */
    @Override
    public void run() {
        Date date = new Date(System.currentTimeMillis());
        List<Subscription> subscriptions = subscriptionDAO.getOngoingSubscriptions();
        subscriptions = subscriptions.stream()
                .filter(x -> x.getBookReturnDeadline()
                        .compareTo(date) < 0).collect(Collectors.toList());
        subscriptions.forEach(x -> {
            BigDecimal fine = x.getFine();
            if (fine == null) {
                fine = BigDecimal.ZERO;
            }
            subscriptionDAO.updateFine(fine.add(BigDecimal.valueOf(5)), x.getId());
        });
    }
}
