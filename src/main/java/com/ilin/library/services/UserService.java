package com.ilin.library.services;

import com.ilin.library.dto.UserDTO;

import java.util.List;

public interface UserService {
    /**
     * Gets all users from the database with indicated ordering.
     *
     * @param orderBy    name of column in the database for ordering
     * @param desc       ascending or descending type ("true" or "false" possible)
     * @param pageNumber number of required page (fetching only 10 items per statement)
     * @return list of users that were fetched from the database. If list is empty - no matches found.
     * If null - incorrect execution of the statement.
     */
    List<UserDTO> getFullList(String orderBy, String desc, int pageNumber);

    /**
     * Gets all users with "reader" role from the database with indicated ordering.
     *
     * @param orderBy    name of column in the database for ordering
     * @param desc       ascending or descending type ("true" or "false" possible)
     * @param pageNumber number of required page (fetching only 10 items per statement)
     * @return list of users that were fetched from the database. If list is empty - no matches found.
     * If null - incorrect execution of the statement.
     */
    List<UserDTO> getFullReadersList(String orderBy, String desc, int pageNumber);

    /**
     * Gets user from the database according to the indicated login and password.
     *
     * @param login    login of the required user
     * @param password password of the required user
     * @return user that was fetched from the database. If null - no matches found.
     */
    UserDTO getByLoginAndPassword(String login, String password);

    /**
     * Gets user from the database according to the indicated login.
     *
     * @param login login of the required user
     * @return user that was fetched from the database. If empty - no matches found.
     * If null - incorrect execution of the statement.
     */
    UserDTO getByLogin(String login);

    /**
     * Checking whether user with such login exists in the database.
     *
     * @param login login of the required user
     * @return true or false depending on success of operation
     */
    boolean isSuchLoginExists(String login);

    /**
     * Checking whether user with such email exists in the database.
     *
     * @param email email of the required user
     * @return true or false depending on success of operation
     */
    boolean isSuchEmailExists(String email);

    /**
     * Checking whether user with such phone number exists in the database.
     *
     * @param phoneNumber phone number of the required user
     * @return true or false depending on success of operation
     */
    boolean isSuchPhoneNumberExists(String phoneNumber);

    /**
     * Inserting new user to the database.
     *
     * @param dto user that will be inserted
     * @return true or false depending on success of operation
     */
    boolean insertUser(UserDTO dto);

    /**
     * Updating field of indicated user in the database.
     *
     * @param userLogin  login of the user that will be updated
     * @param fieldName  name of the user field
     * @param fieldValue value of the user field
     * @return true or false depending on success of operation
     */
    boolean updateUser(String fieldValue, String fieldName, String userLogin);

    /**
     * Gets quantity of pages of the users with "readers" role from the database according to the indicated parameters.
     *
     * @return Integer with quantity of pages. If 0 - no matches found.
     */
    int getPageQuantityReaders();

    /**
     * Gets quantity of pages of the users from the database according to the indicated parameters.
     *
     * @return Integer with quantity of pages. If 0 - no matches found.
     */
    int getPageQuantityUsers();

    /**
     * Deleting user with indicated login and "librarian" role from the database.
     *
     * @param login login of the required user
     * @return true or false depending on success of operation
     */
    boolean deleteLibrarian(String login);

    /**
     * Updating block status of user with indicated login on the opposite in the database.
     *
     * @param login login of the required user
     * @return true or false depending on success of operation
     */
    boolean changeBlockStatus(String login);
}
