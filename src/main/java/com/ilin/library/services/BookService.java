package com.ilin.library.services;

import com.ilin.library.dto.AuthorDTO;
import com.ilin.library.dto.BookDTO;

import java.util.List;

public interface BookService {
    /**
     * Gets books from the database according to the indicated parameters.
     *
     * @param orderBy     name of column in the database for ordering
     * @param desc        ascending or descending type ("true" or "false" possible)
     * @param pageNumber  number of required page (fetching only 10 items per statement)
     * @param searchWords array of Strings for searching among all results
     * @return list of books that were fetched from the database. If list is empty - no matches found.
     * If null - incorrect execution of the statement.
     */
    List<BookDTO> getSearchResultList(String[] searchWords, String orderBy, String desc, int pageNumber);

    /**
     * Gets all books from the database without filtering.
     *
     * @return list of books that were fetched from the database. If list is empty - no matches found.
     * If null - incorrect execution of the statement.
     */
    List<BookDTO> getFullList();

    /**
     * Adding indicated book to the user's book cart list.
     *
     * @param bookID ID of the required book
     * @return book that was fetched from the database. If null - no matches found.
     */
    BookDTO addBookToCart(int bookID);

    /**
     * Gets book from the database according to the indicated ID.
     *
     * @param bookID ID of the required book
     * @return book that was fetched from the database. If null - no matches found.
     */
    BookDTO getByID(int bookID);

    /**
     * Deleting indicated book to the user's book cart list.
     *
     * @param dto book that will be deleted
     * @return true or false depending on success of operation
     */
    boolean deleteBookFromCart(BookDTO dto);

    /**
     * Gets quantity of pages of the books from the database without parameters.
     *
     * @return Integer with quantity of pages. If empty - no matches found.
     * If null - incorrect execution of the statement.
     */
    int getPageQuantity();

    /**
     * Gets quantity of pages of the books from the database according to the indicated parameters.
     *
     * @param split array of Strings for searching among all results
     * @return Integer with quantity of pages. If empty - no matches found.
     * If null - incorrect execution of the statement.
     */
    int getPageQuantity(String[] split);

    /**
     * Creating BookDTO from the parameters that were put in the method and returning it.
     *
     * @return book that was created from the parameters
     */
    BookDTO createBook(String title, String authorNameUA, String authorNameEN, String publisherHouse, int year,
                       String language, int quantity);

    /**
     * Checking whether indicated book exists in the database.
     *
     * @param dto book that will be checked
     * @return true or false depending on success of operation
     */
    boolean isSuchBookExists(BookDTO dto);

    /**
     * Checking whether indicated book with new indicated parameter exists in the database.
     *
     * @param dto        book that will be checked
     * @param fieldName  name of the book field
     * @param fieldValue value of the book field
     * @return true or false depending on success of operation
     */
    boolean isSuchBookExists(BookDTO dto, String fieldName, String fieldValue);

    /**
     * Inserting indicated book to the database.
     *
     * @param dto book that will be inserted
     * @return true or false depending on success of operation
     */
    boolean insertBook(BookDTO dto);

    /**
     * Updating field of indicated book in the database.
     *
     * @param id         ID of the book that will be updated
     * @param fieldName  name of the book field
     * @param fieldValue value of the book field
     * @return true or false depending on success of operation
     */
    boolean updateBook(int id, String fieldName, String fieldValue);

    /**
     * Deleting indicated book from the database.
     *
     * @param id ID of the book that will be deleted
     * @return true or false depending on success of operation
     */
    boolean deleteBook(int id);

    /**
     * Reassigning author for the existing book
     *
     * @param dto author that will be altered
     * @param id  ID of the book that will be updated
     * @return true or false depending on success of operation
     */
    boolean reassignAuthor(AuthorDTO dto, int id);
}
