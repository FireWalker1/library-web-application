package com.ilin.library.services;

import com.ilin.library.dto.SubscriptionDTO;

import java.util.List;

public interface SubscriptionService {
    /**
     * Inserting new subscription to the database.
     *
     * @param dto       subscription that will be inserted
     * @param userLogin login of user that will be attached to subscription
     * @return true or false depending on success of operation
     */
    boolean insertSubscription(SubscriptionDTO dto, String userLogin);

    /**
     * Gets all subscriptions from the database that related to certain user.
     *
     * @param userLogin login of required user
     * @return list of subscriptions that were fetched from the database. If list is empty - no matches found.
     * If null - incorrect execution of the statement.
     */
    List<SubscriptionDTO> getUserSubscriptions(String userLogin);

    /**
     * Gets subscription from the database with indicated ID.
     *
     * @param subscriptionID ID of required subscription
     * @return subscription that was fetched from the database. If empty - no matches found.
     * If null - incorrect execution of the statement.
     */
    SubscriptionDTO getSubscriptionByID(int subscriptionID);

    /**
     * Gets all subscriptions from the database with indicated ordering.
     *
     * @param orderBy    name of column in the database for ordering
     * @param desc       ascending or descending type ("true" or "false" possible)
     * @param pageNumber number of required page (fetching only 10 items per statement)
     * @return list of subscriptions that were fetched from the database. If list is empty - no matches found.
     * If null - incorrect execution of the statement.
     */
    List<SubscriptionDTO> getFullList(String orderBy, String desc, int pageNumber);

    /**
     * Gets quantity of pages of the subscriptions from the database.
     *
     * @return Integer with quantity of pages. If empty - no matches found.
     * If null - incorrect execution of the statement.
     */
    int getPageQuantity();

    /**
     * Progressing status of the subscription with indicated ID in the database.
     *
     * @param subscriptionID   ID of required subscription
     * @param returnDateOption if status should be changed on "Ongoing" then indicates return date option, if not - null
     * @return true or false depending on success of operation
     */
    boolean progressStatus(int subscriptionID, String returnDateOption);

    /**
     * Updating status of the subscription with indicated ID in the database on "Closed".
     *
     * @param subscriptionID ID of required subscription
     * @return true or false depending on success of operation
     */
    boolean rejectSubscription(int subscriptionID);

    /**
     * Updating fine to 0 for the subscription with indicated ID in the database.
     *
     * @param subscriptionID ID of required subscription
     * @return true or false depending on success of operation
     */
    boolean payFine(int subscriptionID);

    /**
     * Getting sum of all user fines from all subscriptions from the database.
     *
     * @param userLogin login of required user
     * @return int sum of all user fines
     */
    int getUserFines(String userLogin);
}
