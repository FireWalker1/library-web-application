package com.ilin.library.controller.commands.common;

import com.ilin.library.controller.Path;
import com.ilin.library.controller.commands.Command;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

class LogoutCommandTest {
    private final HttpServletResponse response = mock(HttpServletResponse.class);
    private final HttpServletRequest request = mock(HttpServletRequest.class);
    private final HttpSession session = mock(HttpSession.class);
    private final Command command = new LogoutCommand();

    @Test
    void testExecuteCorrectBehavior() throws IOException {
        when(request.getSession()).thenReturn(session);
        when(request.getCookies()).thenReturn(new Cookie[] {new Cookie("language", "en")});
        assertEquals(Path.REDIRECT, command.execute(request, response));
        verify(response).sendRedirect(Path.PAGE_INDEX);
    }
}