package com.ilin.library.controller.commands.administrator;

import com.ilin.library.TestHelper;
import com.ilin.library.controller.Path;
import com.ilin.library.controller.commands.Command;
import com.ilin.library.dto.BookDTO;
import com.ilin.library.services.implementation.BookServiceImpl;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

class GetBookCommandTest {
    private final HttpServletResponse response = mock(HttpServletResponse.class);
    private final HttpServletRequest request = mock(HttpServletRequest.class);
    private final BookServiceImpl service = mock(BookServiceImpl.class);
    private final Command command = new GetBookCommand(service);

    @Test
    void testExecuteCorrectBehavior() throws IOException {
        when(request.getParameter("id")).thenReturn("1");
        BookDTO book = TestHelper.getTestBookDTO();
        when(service.getByID(1)).thenReturn(book);
        assertEquals(Path.PAGE_EDIT_BOOK, command.execute(request, response));
        verify(request).setAttribute("book", book);
    }

    @Test
    void testExecuteIncorrectBehavior() throws IOException {
        when(request.getParameter("id")).thenReturn("1");
        when(service.getByID(1)).thenReturn(null);
        assertEquals(Path.REDIRECT, command.execute(request, response));
        verify(response).sendRedirect(Path.PAGE_ERROR);
    }
}