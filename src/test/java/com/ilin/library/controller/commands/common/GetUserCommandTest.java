package com.ilin.library.controller.commands.common;

import com.ilin.library.TestHelper;
import com.ilin.library.controller.Path;
import com.ilin.library.controller.commands.Command;
import com.ilin.library.services.UserService;
import com.ilin.library.services.implementation.UserServiceImpl;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

class GetUserCommandTest {
    private final HttpServletResponse response = mock(HttpServletResponse.class);
    private final HttpServletRequest request = mock(HttpServletRequest.class);
    private final UserService service = mock(UserServiceImpl.class);
    private final HttpSession session = mock(HttpSession.class);
    private final Command command = new GetUserCommand(service);

    @Test
    void testExecuteCorrectBehavior() throws IOException {
        when(request.getSession()).thenReturn(session);
        when(session.getAttribute("login")).thenReturn("Igor");
        when(service.getByLogin("Igor")).thenReturn(TestHelper.getTestUserDTO());
        assertEquals(Path.PAGE_PROFILE, command.execute(request, response));
        verify(request).setAttribute("user", TestHelper.getTestUserDTO());
    }

    @Test
    void testExecuteIncorrectBehavior() throws IOException {
        when(request.getSession()).thenReturn(session);
        when(session.getAttribute("login")).thenReturn("Igor");
        when(service.getByLogin("Igor")).thenReturn(null);
        assertEquals(Path.REDIRECT, command.execute(request, response));
        verify(response).sendRedirect(Path.PAGE_ERROR);
    }
}