package com.ilin.library.controller.commands.reader;

import com.ilin.library.TestHelper;
import com.ilin.library.controller.Path;
import com.ilin.library.controller.commands.Command;
import com.ilin.library.dto.BookDTO;
import com.ilin.library.services.implementation.BookServiceImpl;
import com.ilin.library.utils.AddressBuilder;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class DeleteBookFromCartCommandTest {
    private final HttpServletResponse response = mock(HttpServletResponse.class);
    private final HttpServletRequest request = mock(HttpServletRequest.class);
    private final BookServiceImpl service = mock(BookServiceImpl.class);
    private final Command command = new DeleteBookFromCartCommand(service);
    private final HttpSession session = mock(HttpSession.class);
    @Test
    void testExecuteCorrectBehavior() throws IOException {
        when(request.getSession()).thenReturn(session);
        when(request.getParameter("id")).thenReturn("1");
        List<BookDTO> books = TestHelper.getTestBookDTOList();
        when(session.getAttribute("bookCart")).thenReturn(books);
        Map<String, String> info = new HashMap<>();
        BookDTO book = TestHelper.getTestBookDTO();
        when(service.deleteBookFromCart(book)).thenReturn(true);
        info.put("infoText", "cart.info_success");
        assertEquals(Path.REDIRECT, command.execute(request, response));
        verify(response).sendRedirect(Path.PAGE_CART + AddressBuilder.formPageProperties("", info));
    }
    @Test
    void testExecuteIncorrectBehavior() throws IOException {
        when(request.getSession()).thenReturn(session);
        when(request.getParameter("id")).thenReturn("1");
        List<BookDTO> books = TestHelper.getTestBookDTOList();
        when(session.getAttribute("bookCart")).thenReturn(books);
        Map<String, String> info = new HashMap<>();
        BookDTO book = TestHelper.getTestBookDTO();
        when(service.deleteBookFromCart(book)).thenReturn(false);
        info.put("errorText", "cart.error_incorrect_id");
        assertEquals(Path.REDIRECT, command.execute(request, response));
        verify(response).sendRedirect(Path.PAGE_CART + AddressBuilder.formPageProperties("", info));
    }
}