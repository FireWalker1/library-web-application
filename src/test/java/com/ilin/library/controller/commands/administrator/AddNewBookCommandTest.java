package com.ilin.library.controller.commands.administrator;

import com.ilin.library.TestHelper;
import com.ilin.library.controller.Path;
import com.ilin.library.controller.commands.Command;
import com.ilin.library.dto.AuthorDTO;
import com.ilin.library.dto.BookDTO;
import com.ilin.library.services.implementation.BookServiceImpl;
import com.ilin.library.utils.AddressBuilder;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

class AddNewBookCommandTest {
    private final HttpServletResponse response = mock(HttpServletResponse.class);
    private final HttpServletRequest request = mock(HttpServletRequest.class);
    private final BookServiceImpl service = mock(BookServiceImpl.class);
    private final Command command = new AddNewBookCommand(service);

    @Test
    void testExecuteStandardValues() throws IOException {
        when(request.getParameter("title")).thenReturn("Title");
        when(request.getParameter("authorNameUA")).thenReturn("Name");
        when(request.getParameter("authorNameEN")).thenReturn("Name");
        when(request.getParameter("publisherHouse")).thenReturn("House");
        when(request.getParameter("year")).thenReturn("1998");
        when(request.getParameter("language")).thenReturn("UA");
        when(request.getParameter("quantity")).thenReturn("3");
        BookDTO book = TestHelper.getTestBookDTO();
        when(service.createBook("Title", "Name", "Name", "House", 1998,
                "UA", 3)).thenReturn(book);
        when(service.isSuchBookExists(book)).thenReturn(false);
        when(service.insertBook(book)).thenReturn(true);
        assertEquals(Path.REDIRECT, command.execute(request, response));
        verify(response).sendRedirect(Path.PAGE_SUCCESS);
    }

    @Test
    void testExecuteExistingValues() throws IOException {
        when(request.getParameter("title")).thenReturn("Title");
        when(request.getParameter("authorNameUA")).thenReturn("Name");
        when(request.getParameter("authorNameEN")).thenReturn("Name");
        when(request.getParameter("publisherHouse")).thenReturn("House");
        when(request.getParameter("year")).thenReturn("1998");
        when(request.getParameter("language")).thenReturn("UA");
        when(request.getParameter("quantity")).thenReturn("3");
        BookDTO book = TestHelper.getTestBookDTO();
        when(service.createBook("Title", "Name", "Name", "House", 1998,
                "UA", 3)).thenReturn(book);
        when(service.isSuchBookExists(book)).thenReturn(true);
        Map<String, String> info = new HashMap<>();
        info.put("duplicateError", "add_new_book.error_already_exists");
        assertEquals(Path.REDIRECT, command.execute(request, response));
        verify(response).sendRedirect(Path.PAGE_ADD_NEW_BOOK + AddressBuilder.formPageProperties("", info));
    }

    @Test
    void testExecuteIncorrectValues() throws IOException {
        when(request.getParameter("title")).thenReturn("a");
        when(request.getParameter("authorNameUA")).thenReturn("b");
        when(request.getParameter("authorNameEN")).thenReturn("c");
        when(request.getParameter("publisherHouse")).thenReturn("d");
        when(request.getParameter("year")).thenReturn("er");
        when(request.getParameter("language")).thenReturn("ZN");
        when(request.getParameter("quantity")).thenReturn("-5");
        when(service.createBook("a", "b", "c", "d", -1,
                "ZN", -5)).thenReturn(new BookDTO(0, "a", new AuthorDTO(0, "b", "c"),
                "d", -1, "ZN", -5));
        Map<String, String> info = new HashMap<>();
        info.put("titleError", "add_new_book.error_not_valid_title");
        info.put("authorNameUAError", "add_new_book.error_not_valid_author_name");
        info.put("authorNameENError", "add_new_book.error_not_valid_author_name");
        info.put("publisherHouseError", "add_new_book.error_not_valid_publisher_house");
        info.put("yearError", "add_new_book.error_not_valid_year");
        info.put("quantityError", "add_new_book.error_not_valid_quantity");
        assertEquals(Path.REDIRECT, command.execute(request, response));
        verify(response).sendRedirect(Path.PAGE_ADD_NEW_BOOK + AddressBuilder.formPageProperties("", info));
    }
}