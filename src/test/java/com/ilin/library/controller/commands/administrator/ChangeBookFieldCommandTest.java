package com.ilin.library.controller.commands.administrator;

import com.ilin.library.TestHelper;
import com.ilin.library.controller.Path;
import com.ilin.library.controller.commands.Command;
import com.ilin.library.dto.BookDTO;
import com.ilin.library.services.implementation.BookServiceImpl;
import com.ilin.library.utils.AddressBuilder;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

class ChangeBookFieldCommandTest {
    private final HttpServletResponse response = mock(HttpServletResponse.class);
    private final HttpServletRequest request = mock(HttpServletRequest.class);
    private final BookServiceImpl service = mock(BookServiceImpl.class);
    private final Command command = new ChangeBookFieldCommand(service);

    @Test
    void testExecuteStandardValue() throws IOException {
        when(request.getParameter("fieldName")).thenReturn("title");
        when(request.getParameter("fieldValue")).thenReturn("New Title");
        when(request.getParameter("id")).thenReturn("1");
        BookDTO book = TestHelper.getTestBookDTO();
        Map<String, String> info = new HashMap<>();
        info.put("infoText", "edit_book.info_successful_update");
        info.put("id", "1");
        when(service.getByID(1)).thenReturn(book);
        when(service.isSuchBookExists(book, "title", "New Title")).thenReturn(false);
        when(service.updateBook(1, "title", "New Title")).thenReturn(true);
        assertEquals(Path.REDIRECT, command.execute(request, response));
        verify(response).sendRedirect(Path.COMMAND_GET_BOOK + AddressBuilder.formCommandProperties("", info));
    }

    @Test
    void testExecuteExisingValue() throws IOException {
        when(request.getParameter("fieldName")).thenReturn("title");
        when(request.getParameter("fieldValue")).thenReturn("New Title");
        when(request.getParameter("id")).thenReturn("1");
        BookDTO book = TestHelper.getTestBookDTO();
        Map<String, String> info = new HashMap<>();
        info.put("errorText", "add_new_book.error_already_exists");
        info.put("id", "1");
        when(service.getByID(1)).thenReturn(book);
        when(service.isSuchBookExists(book, "title", "New Title")).thenReturn(true);
        assertEquals(Path.REDIRECT, command.execute(request, response));
        verify(response).sendRedirect(Path.COMMAND_GET_BOOK + AddressBuilder.formCommandProperties("", info));
    }

    @Test
    void testExecuteIncorrectValue() throws IOException {
        when(request.getParameter("fieldName")).thenReturn("title");
        when(request.getParameter("fieldValue")).thenReturn("5");
        when(request.getParameter("id")).thenReturn("1");
        BookDTO book = TestHelper.getTestBookDTO();
        Map<String, String> info = new HashMap<>();
        info.put("errorText", "add_new_book.error_not_valid_title");
        info.put("id", "1");
        when(service.getByID(1)).thenReturn(book);
        when(service.isSuchBookExists(book, "title", "New Title")).thenReturn(false);
        assertEquals(Path.REDIRECT, command.execute(request, response));
        verify(response).sendRedirect(Path.COMMAND_GET_BOOK + AddressBuilder.formCommandProperties("", info));
    }
}