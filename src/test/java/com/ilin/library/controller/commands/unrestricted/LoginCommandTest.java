package com.ilin.library.controller.commands.unrestricted;

import com.ilin.library.TestHelper;
import com.ilin.library.controller.Path;
import com.ilin.library.controller.commands.Command;
import com.ilin.library.dto.UserDTO;
import com.ilin.library.services.UserService;
import com.ilin.library.services.implementation.UserServiceImpl;
import com.ilin.library.utils.AddressBuilder;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

class LoginCommandTest {
    private final HttpServletResponse response = mock(HttpServletResponse.class);
    private final HttpServletRequest request = mock(HttpServletRequest.class);
    private final UserService service = mock(UserServiceImpl.class);
    private final HttpSession session = mock(HttpSession.class);
    private final Command command = new LoginCommand(service);

    @Test
    void testExecuteStandardValues() throws IOException {
        when(request.getParameter("login")).thenReturn("Igor");
        when(request.getParameter("password")).thenReturn("password");
        when(request.getSession()).thenReturn(session);
        when(service.getByLoginAndPassword("Igor", "password")).thenReturn(TestHelper.getTestUserDTO());
        assertEquals(Path.REDIRECT, command.execute(request, response));
        verify(session).setAttribute("login", "Igor");
        verify(session).setAttribute("role", "role.reader");
    }

    @Test
    void testExecuteEmptyUserDTO() throws IOException {
        when(request.getParameter("login")).thenReturn("Igor");
        when(request.getParameter("password")).thenReturn("password");
        when(request.getSession()).thenReturn(session);
        Map<String, String> map = new HashMap<>();
        map.put("errorText", "login.error_incorrect_fields");
        when(service.getByLoginAndPassword("Igor", "password")).thenReturn(null);
        assertEquals(Path.REDIRECT, command.execute(request, response));
        verify(response).sendRedirect(Path.PAGE_LOGIN + AddressBuilder.formPageProperties("", map));
    }

    @Test
    void testExecuteBlockedUserDTO() throws IOException {
        when(request.getParameter("login")).thenReturn("Igor");
        when(request.getParameter("password")).thenReturn("password");
        when(request.getSession()).thenReturn(session);
        Map<String, String> map = new HashMap<>();
        map.put("errorText", "login.error_user_is_blocked");
        UserDTO dto = TestHelper.getTestUserDTO();
        dto.setBlockedAt(new Timestamp(123));
        when(service.getByLoginAndPassword("Igor", "password")).thenReturn(dto);
        assertEquals(Path.REDIRECT, command.execute(request, response));
        verify(response).sendRedirect(Path.PAGE_LOGIN + AddressBuilder.formPageProperties("", map));
    }

    @Test
    void testExecuteNullValues() throws IOException {
        when(request.getParameter("login")).thenReturn(null);
        when(request.getParameter("password")).thenReturn(null);
        when(request.getSession()).thenReturn(session);
        Map<String, String> map = new HashMap<>();
        map.put("errorText", "login.error_empty_fields");
        assertEquals(Path.REDIRECT, command.execute(request, response));
        verify(response).sendRedirect(Path.PAGE_LOGIN + AddressBuilder.formPageProperties("", map));
    }
}