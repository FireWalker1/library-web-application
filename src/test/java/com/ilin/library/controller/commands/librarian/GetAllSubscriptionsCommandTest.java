package com.ilin.library.controller.commands.librarian;

import com.ilin.library.TestHelper;
import com.ilin.library.controller.Path;
import com.ilin.library.controller.commands.Command;
import com.ilin.library.dto.SubscriptionDTO;
import com.ilin.library.services.SubscriptionService;
import com.ilin.library.services.implementation.SubscriptionServiceImpl;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

class GetAllSubscriptionsCommandTest {
    private final HttpServletResponse response = mock(HttpServletResponse.class);
    private final HttpServletRequest request = mock(HttpServletRequest.class);
    private final SubscriptionService service = mock(SubscriptionServiceImpl.class);
    private final Command command = new GetAllSubscriptionsCommand(service);

    @Test
    void testExecuteCorrectBehavior() throws IOException {
        when(request.getParameter("orderBy")).thenReturn("id");
        when(request.getParameter("desc")).thenReturn("true");
        when(request.getParameter("pageNumber")).thenReturn("1");
        List<SubscriptionDTO> subscriptions = TestHelper.getTestSubscriptionDTOList();
        when(service.getFullList("id", "true", 1)).thenReturn(subscriptions);
        when(service.getPageQuantity()).thenReturn(5);
        assertEquals(Path.PAGE_ALL_SUBSCRIPTIONS, command.execute(request, response));
        verify(request).setAttribute("subscriptions", subscriptions);
        verify(request).setAttribute("pageQuantity", 5);
    }
}