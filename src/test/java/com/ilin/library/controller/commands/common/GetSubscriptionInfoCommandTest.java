package com.ilin.library.controller.commands.common;

import com.ilin.library.TestHelper;
import com.ilin.library.controller.Path;
import com.ilin.library.controller.commands.Command;
import com.ilin.library.dto.SubscriptionDTO;
import com.ilin.library.services.SubscriptionService;
import com.ilin.library.services.implementation.SubscriptionServiceImpl;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

class GetSubscriptionInfoCommandTest {
    private final HttpServletResponse response = mock(HttpServletResponse.class);
    private final HttpServletRequest request = mock(HttpServletRequest.class);
    private final SubscriptionService service = mock(SubscriptionServiceImpl.class);
    private final Command command = new GetSubscriptionInfoCommand(service);

    @Test
    void testExecuteCorrectSubscription() throws IOException {
        when(request.getParameter("id")).thenReturn("1");
        SubscriptionDTO dto = TestHelper.getTestSubscriptionDTO();
        when(service.getSubscriptionByID(1)).thenReturn(dto);
        assertEquals(Path.PAGE_SUBSCRIPTION_INFO, command.execute(request, response));
        verify(request).setAttribute("subscription", dto);
    }

    @Test
    void testExecuteIncorrectSubscription() throws IOException {
        when(request.getParameter("id")).thenReturn("2");
        SubscriptionDTO dto = TestHelper.getTestSubscriptionDTO();
        dto.setBooks(new ArrayList<>());
        when(service.getSubscriptionByID(2)).thenReturn(dto);
        assertEquals(Path.REDIRECT, command.execute(request, response));
    }
}