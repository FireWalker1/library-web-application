package com.ilin.library.controller.commands.unrestricted;

import com.ilin.library.controller.Path;
import com.ilin.library.controller.commands.Command;
import com.ilin.library.dto.UserDTO;
import com.ilin.library.services.UserService;
import com.ilin.library.services.implementation.UserServiceImpl;
import com.ilin.library.utils.AddressBuilder;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

class SignUpCommandTest {
    private final HttpServletResponse response = mock(HttpServletResponse.class);
    private final HttpServletRequest request = mock(HttpServletRequest.class);
    private final UserService service = mock(UserServiceImpl.class);
    private final HttpSession session = mock(HttpSession.class);
    private final Command command = new SignUpCommand(service);

    @Test
    void testExecuteStandardValues() throws IOException {
        when(request.getParameter("login")).thenReturn("Igor");
        when(request.getParameter("firstName")).thenReturn("Igor");
        when(request.getParameter("lastName")).thenReturn("Igorenko");
        when(request.getParameter("password")).thenReturn("password");
        when(request.getParameter("email")).thenReturn("email@site.com");
        when(request.getParameter("phoneNumber")).thenReturn("639940558");
        when(request.getSession()).thenReturn(session);
        when(session.getAttribute("role")).thenReturn(null);
        when(service.isSuchLoginExists("Igor")).thenReturn(false);
        when(service.isSuchEmailExists("email@site.com")).thenReturn(false);
        when(service.isSuchPhoneNumberExists("639940558")).thenReturn(false);
        when(service.insertUser(any(UserDTO.class))).thenReturn(true);
        assertEquals(Path.REDIRECT, command.execute(request, response));
        verify(response).sendRedirect(Path.PAGE_SUCCESS);
    }

    @Test
    void testExecuteExistingValues() throws IOException {
        when(request.getParameter("login")).thenReturn("Igor");
        when(request.getParameter("firstName")).thenReturn("Igor");
        when(request.getParameter("lastName")).thenReturn("Igorenko");
        when(request.getParameter("password")).thenReturn("password");
        when(request.getParameter("email")).thenReturn("email@site.com");
        when(request.getParameter("phoneNumber")).thenReturn("639940558");
        when(request.getSession()).thenReturn(session);
        when(session.getAttribute("role")).thenReturn(null);
        when(service.isSuchLoginExists("Igor")).thenReturn(true);
        when(service.isSuchEmailExists("email@site.com")).thenReturn(true);
        when(service.isSuchPhoneNumberExists("639940558")).thenReturn(true);
        Map<String, String> map = new HashMap<>();
        map.put("loginError", "sign_up.error_login_already_exists");
        map.put("emailError", "sign_up.error_email_already_exists");
        map.put("phoneNumberError", "sign_up.error_phone_number_already_exists");
        assertEquals(Path.REDIRECT, command.execute(request, response));
        verify(response).sendRedirect(Path.PAGE_SIGN_UP + AddressBuilder.formPageProperties("", map));
    }

    @Test
    void testExecuteIncorrectValues() throws IOException {
        when(request.getParameter("login")).thenReturn("a");
        when(request.getParameter("firstName")).thenReturn("b");
        when(request.getParameter("lastName")).thenReturn("c");
        when(request.getParameter("password")).thenReturn("2");
        when(request.getParameter("email")).thenReturn("incorrect");
        when(request.getParameter("phoneNumber")).thenReturn("35232");
        when(request.getSession()).thenReturn(session);
        when(session.getAttribute("role")).thenReturn(null);
        when(service.isSuchLoginExists("Igor")).thenReturn(false);
        when(service.isSuchEmailExists("email@site.com")).thenReturn(false);
        when(service.isSuchPhoneNumberExists("639940558")).thenReturn(false);
        Map<String, String> map = new HashMap<>();
        map.put("firstNameError", "sign_up.error_not_valid_first_name");
        map.put("lastNameError", "sign_up.error_not_valid_last_name");
        map.put("loginError", "sign_up.error_not_valid_login");
        map.put("passwordError", "sign_up.error_not_valid_password");
        map.put("emailError", "sign_up.error_not_valid_email");
        map.put("phoneNumberError", "sign_up.error_not_valid_phone_number");
        assertEquals(Path.REDIRECT, command.execute(request, response));
        verify(response).sendRedirect(Path.PAGE_SIGN_UP + AddressBuilder.formPageProperties("", map));
    }
}