package com.ilin.library.controller.commands.librarian;

import com.ilin.library.TestHelper;
import com.ilin.library.controller.Path;
import com.ilin.library.controller.commands.Command;
import com.ilin.library.dto.SubscriptionDTO;
import com.ilin.library.services.SubscriptionService;
import com.ilin.library.services.implementation.SubscriptionServiceImpl;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

class GetReaderSubscriptionsCommandTest {
    private final HttpServletResponse response = mock(HttpServletResponse.class);
    private final HttpServletRequest request = mock(HttpServletRequest.class);
    private final SubscriptionService service = mock(SubscriptionServiceImpl.class);
    private final Command command = new GetReaderSubscriptionsCommand(service);

    @Test
    void testExecuteCorrectBehavior() throws IOException {
        when(request.getParameter("login")).thenReturn("Igor");
        List<SubscriptionDTO> subscriptions = TestHelper.getTestSubscriptionDTOList();
        when(service.getUserSubscriptions("Igor")).thenReturn(subscriptions);
        assertEquals(Path.PAGE_MY_SUBSCRIPTIONS, command.execute(request, response));
        verify(request).setAttribute("subscriptions", subscriptions);
        verify(request).setAttribute("login", "Igor");
    }

    @Test
    void testExecuteIncorrectBehavior() throws IOException {
        when(request.getParameter("login")).thenReturn("Igor");
        List<SubscriptionDTO> subscriptions = new ArrayList<>();
        when(service.getUserSubscriptions("Igor")).thenReturn(subscriptions);
        assertEquals(Path.PAGE_MY_SUBSCRIPTIONS, command.execute(request, response));
        verify(request).setAttribute("errorText", "my_subscriptions.error_empty");
    }
}