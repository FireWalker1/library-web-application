package com.ilin.library.controller.commands.common;

import com.ilin.library.controller.Path;
import com.ilin.library.controller.commands.Command;
import com.ilin.library.services.UserService;
import com.ilin.library.services.implementation.UserServiceImpl;
import com.ilin.library.utils.AddressBuilder;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

class ChangeUserFieldCommandTest {
    private final HttpServletResponse response = mock(HttpServletResponse.class);
    private final HttpServletRequest request = mock(HttpServletRequest.class);
    private final UserService service = mock(UserServiceImpl.class);
    private final HttpSession session = mock(HttpSession.class);
    private final Command command = new ChangeUserFieldCommand(service);

    @Test
    void testExecuteCorrectLogin() throws IOException {
        when(request.getParameter("fieldName")).thenReturn("login");
        when(request.getParameter("fieldValue")).thenReturn("Igor2");
        when(request.getSession()).thenReturn(session);
        when(session.getAttribute("login")).thenReturn("Igor");
        when(service.isSuchLoginExists("Igor2")).thenReturn(false);
        when(service.updateUser("Igor2", "login", "Igor")).thenReturn(true);
        Map<String, String> map = new HashMap<>();
        map.put("infoText", "profile.info_successful_update");
        assertEquals(Path.REDIRECT, command.execute(request, response));
        verify(response).sendRedirect(Path.COMMAND_GET_USER + AddressBuilder.formCommandProperties("", map));
    }

    @Test
    void testExecuteCorrectEmail() throws IOException {
        when(request.getParameter("fieldName")).thenReturn("email");
        when(request.getParameter("fieldValue")).thenReturn("newmail@site.com");
        when(request.getSession()).thenReturn(session);
        when(session.getAttribute("login")).thenReturn("Igor");
        when(service.isSuchEmailExists("newmail@site.com")).thenReturn(false);
        when(service.updateUser("newmail@site.com", "email", "Igor")).thenReturn(true);
        Map<String, String> map = new HashMap<>();
        map.put("infoText", "profile.info_successful_update");
        assertEquals(Path.REDIRECT, command.execute(request, response));
        verify(response).sendRedirect(Path.COMMAND_GET_USER + AddressBuilder.formCommandProperties("", map));
    }

    @Test
    void testExecuteIncorrectValue() throws IOException {
        when(request.getParameter("fieldName")).thenReturn("login");
        when(request.getParameter("fieldValue")).thenReturn("a");
        when(request.getSession()).thenReturn(session);
        when(session.getAttribute("login")).thenReturn("Igor");
        Map<String, String> map = new HashMap<>();
        map.put("errorText", "sign_up.error_not_valid_login");
        assertEquals(Path.REDIRECT, command.execute(request, response));
        verify(response).sendRedirect(Path.COMMAND_GET_USER + AddressBuilder.formCommandProperties("", map));
    }

    @Test
    void testExecuteExistingLogin() throws IOException {
        when(request.getParameter("fieldName")).thenReturn("login");
        when(request.getParameter("fieldValue")).thenReturn("Igor3");
        when(request.getSession()).thenReturn(session);
        when(session.getAttribute("login")).thenReturn("Igor");
        when(service.isSuchLoginExists("Igor3")).thenReturn(true);
        Map<String, String> map = new HashMap<>();
        map.put("errorText", "sign_up.error_login_already_exists");
        assertEquals(Path.REDIRECT, command.execute(request, response));
        verify(response).sendRedirect(Path.COMMAND_GET_USER + AddressBuilder.formCommandProperties("", map));
    }

    @Test
    void testExecuteExistingEmail() throws IOException {
        when(request.getParameter("fieldName")).thenReturn("email");
        when(request.getParameter("fieldValue")).thenReturn("email@site.com");
        when(request.getSession()).thenReturn(session);
        when(session.getAttribute("login")).thenReturn("Igor");
        when(service.isSuchEmailExists("email@site.com")).thenReturn(true);
        Map<String, String> map = new HashMap<>();
        map.put("errorText", "sign_up.error_email_already_exists");
        assertEquals(Path.REDIRECT, command.execute(request, response));
        verify(response).sendRedirect(Path.COMMAND_GET_USER + AddressBuilder.formCommandProperties("", map));
    }

    @Test
    void testExecuteExistingPhoneNumber() throws IOException {
        when(request.getParameter("fieldName")).thenReturn("phoneNumber");
        when(request.getParameter("fieldValue")).thenReturn("636667788");
        when(request.getSession()).thenReturn(session);
        when(session.getAttribute("login")).thenReturn("Igor");
        when(service.isSuchPhoneNumberExists("636667788")).thenReturn(true);
        Map<String, String> map = new HashMap<>();
        map.put("errorText", "sign_up.error_phone_number_already_exists");
        assertEquals(Path.REDIRECT, command.execute(request, response));
        verify(response).sendRedirect(Path.COMMAND_GET_USER + AddressBuilder.formCommandProperties("", map));
    }
}