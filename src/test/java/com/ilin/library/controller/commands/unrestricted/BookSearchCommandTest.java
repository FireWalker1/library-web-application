package com.ilin.library.controller.commands.unrestricted;

import com.ilin.library.controller.Path;
import com.ilin.library.controller.commands.Command;
import com.ilin.library.services.implementation.BookServiceImpl;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class BookSearchCommandTest {
    private final HttpServletResponse response = mock(HttpServletResponse.class);
    private final HttpServletRequest request = mock(HttpServletRequest.class);
    private final BookServiceImpl service = mock(BookServiceImpl.class);
    private final Command command = new BookSearchCommand(service);

    @Test
    void testExecuteCorrectBehaviorWithParameters() throws IOException {
        when(request.getParameter("searchRequest")).thenReturn("Franko Lesya");
        when(request.getParameter("orderBy")).thenReturn("id");
        when(request.getParameter("pageNumber")).thenReturn("1");
        assertEquals(Path.PAGE_BOOKS, command.execute(request, response));
    }

    @Test
    void testExecuteCorrectBehaviorWithoutParameters() throws IOException {
        when(request.getParameter("searchRequest")).thenReturn(null);
        when(request.getParameter("orderBy")).thenReturn(null);
        when(request.getParameter("pageNumber")).thenReturn(null);
        assertEquals(Path.PAGE_BOOKS, command.execute(request, response));
    }
}