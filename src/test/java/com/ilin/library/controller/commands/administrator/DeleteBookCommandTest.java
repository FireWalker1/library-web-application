package com.ilin.library.controller.commands.administrator;

import com.ilin.library.controller.Path;
import com.ilin.library.controller.commands.Command;
import com.ilin.library.services.implementation.BookServiceImpl;
import com.ilin.library.utils.AddressBuilder;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

class DeleteBookCommandTest {
    private final HttpServletResponse response = mock(HttpServletResponse.class);
    private final HttpServletRequest request = mock(HttpServletRequest.class);
    private final BookServiceImpl service = mock(BookServiceImpl.class);
    private final Command command = new DeleteBookCommand(service);

    @Test
    void testExecuteCorrectBehavior() throws IOException {
        when(request.getParameter("id")).thenReturn("1");
        when(service.deleteBook(1)).thenReturn(true);
        Map<String, String> info = new HashMap<>();
        info.put("infoText", "books.info_successful_delete");
        assertEquals(Path.REDIRECT, command.execute(request, response));
        verify(response).sendRedirect(Path.COMMAND_BOOK_SEARCH + AddressBuilder.formCommandProperties(null, info) +
                "&searchRequest=&orderBy=id&desc=false&pageNumber=1");
    }

    @Test
    void testExecuteIncorrectBehavior() throws IOException {
        when(request.getParameter("id")).thenReturn("1");
        when(service.deleteBook(1)).thenReturn(false);
        Map<String, String> info = new HashMap<>();
        info.put("errorText", "books.error_unsuccessful_delete");
        assertEquals(Path.REDIRECT, command.execute(request, response));
        verify(response).sendRedirect(Path.COMMAND_BOOK_SEARCH + AddressBuilder.formCommandProperties(null, info) +
                "&searchRequest=&orderBy=id&desc=false&pageNumber=1");
    }
}