package com.ilin.library.controller.commands.librarian;

import com.ilin.library.controller.Path;
import com.ilin.library.controller.commands.Command;
import com.ilin.library.services.SubscriptionService;
import com.ilin.library.services.implementation.SubscriptionServiceImpl;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

class ConfirmSubscriptionCommandTest {
    private final HttpServletResponse response = mock(HttpServletResponse.class);
    private final HttpServletRequest request = mock(HttpServletRequest.class);
    private final SubscriptionService service = mock(SubscriptionServiceImpl.class);
    private final Command command = new ConfirmSubscriptionCommand(service);

    @Test
    void testExecuteCorrectBehavior() throws IOException {
        when(request.getParameter("id")).thenReturn("1");
        when(request.getParameter("date")).thenReturn("2");
        when(service.progressStatus(1, "2")).thenReturn(true);
        assertEquals(Path.REDIRECT, command.execute(request, response));
        verify(response).sendRedirect(Path.PAGE_SUCCESS);
    }

    @Test
    void testExecuteIncorrectBehavior() throws IOException {
        when(request.getParameter("id")).thenReturn("1");
        when(request.getParameter("date")).thenReturn("2");
        when(service.progressStatus(1, "2")).thenReturn(false);
        assertEquals(Path.REDIRECT, command.execute(request, response));
        verify(response).sendRedirect(Path.PAGE_ERROR);
    }
}