package com.ilin.library.controller.commands.administrator;

import com.ilin.library.controller.Path;
import com.ilin.library.controller.commands.Command;
import com.ilin.library.services.UserService;
import com.ilin.library.services.implementation.UserServiceImpl;
import com.ilin.library.utils.AddressBuilder;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

class ChangeUserBlockStatusCommandTest {
    private final HttpServletResponse response = mock(HttpServletResponse.class);
    private final HttpServletRequest request = mock(HttpServletRequest.class);
    private final UserService service = mock(UserServiceImpl.class);
    private final Command command = new ChangeUserBlockStatusCommand(service);

    @Test
    void testExecuteCorrectBehavior() throws IOException {
        when(request.getParameter("login")).thenReturn("Igor");
        when(service.changeBlockStatus("Igor")).thenReturn(true);
        Map<String, String> info = new HashMap<>();
        info.put("infoText", "users.info_successful_block");
        assertEquals(Path.REDIRECT, command.execute(request, response));
        verify(response).sendRedirect(Path.COMMAND_GET_ALL_USERS + AddressBuilder.formCommandProperties(null, info));
    }

    @Test
    void testExecuteIncorrectBehavior() throws IOException {
        when(request.getParameter("login")).thenReturn("Igor");
        when(service.changeBlockStatus("Igor")).thenReturn(false);
        Map<String, String> info = new HashMap<>();
        info.put("errorText", "users.error_unsuccessful_block");
        assertEquals(Path.REDIRECT, command.execute(request, response));
        verify(response).sendRedirect(Path.COMMAND_GET_ALL_USERS + AddressBuilder.formCommandProperties(null, info));
    }
}