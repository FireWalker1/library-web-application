package com.ilin.library.controller.commands.librarian;

import com.ilin.library.TestHelper;
import com.ilin.library.controller.Path;
import com.ilin.library.controller.commands.Command;
import com.ilin.library.dto.UserDTO;
import com.ilin.library.services.UserService;
import com.ilin.library.services.implementation.UserServiceImpl;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

class GetAllReadersCommandTest {
    private final HttpServletResponse response = mock(HttpServletResponse.class);
    private final HttpServletRequest request = mock(HttpServletRequest.class);
    private final UserService service = mock(UserServiceImpl.class);
    private final Command command = new GetAllReadersCommand(service);

    @Test
    void testExecuteCorrectBehavior() throws IOException {
        when(request.getParameter("orderBy")).thenReturn("id");
        when(request.getParameter("desc")).thenReturn("true");
        when(request.getParameter("pageNumber")).thenReturn("1");
        List<UserDTO> users = TestHelper.getTestUserDTOList();
        when(service.getFullReadersList("id", "true", 1)).thenReturn(users);
        when(service.getPageQuantityReaders()).thenReturn(5);
        assertEquals(Path.PAGE_USERS, command.execute(request, response));
        verify(request).setAttribute("users", users);
        verify(request).setAttribute("pageQuantity", 5);
    }
}