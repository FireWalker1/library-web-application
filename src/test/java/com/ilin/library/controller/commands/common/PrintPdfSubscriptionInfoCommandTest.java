package com.ilin.library.controller.commands.common;

import com.ilin.library.TestHelper;
import com.ilin.library.controller.Path;
import com.ilin.library.controller.commands.Command;
import com.ilin.library.dto.SubscriptionDTO;
import com.ilin.library.services.SubscriptionService;
import com.ilin.library.services.implementation.SubscriptionServiceImpl;
import jakarta.servlet.ServletOutputStream;
import jakarta.servlet.WriteListener;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class PrintPdfSubscriptionInfoCommandTest {

    private final HttpServletResponse response = mock(HttpServletResponse.class);
    private final HttpServletRequest request = mock(HttpServletRequest.class);
    private final SubscriptionService service = mock(SubscriptionServiceImpl.class);
    private final HttpSession session = mock(HttpSession.class);
    private final Command command = new PrintPdfSubscriptionInfoCommand(service);

    @Test
    void testExecuteCorrectBehavior() throws IOException {
        when(request.getSession()).thenReturn(session);
        when(request.getParameter("id")).thenReturn("1");
        when(session.getAttribute("language")).thenReturn("en");
        when(response.getOutputStream()).thenReturn(new ServletOutputStream() {
            @Override
            public boolean isReady() {
                return true;
            }

            @Override
            public void setWriteListener(WriteListener writeListener) {}

            @Override
            public void write(int b) {}
        });
        SubscriptionDTO dto = TestHelper.getTestSubscriptionDTO();
        when(service.getSubscriptionByID(1)).thenReturn(dto);
        assertEquals(Path.REDIRECT, command.execute(request, response));
    }
}