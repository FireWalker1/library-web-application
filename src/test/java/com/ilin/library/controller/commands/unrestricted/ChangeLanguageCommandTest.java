package com.ilin.library.controller.commands.unrestricted;

import com.ilin.library.controller.Path;
import com.ilin.library.controller.commands.Command;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class ChangeLanguageCommandTest {
    private final HttpServletResponse response = mock(HttpServletResponse.class);
    private final HttpServletRequest request = mock(HttpServletRequest.class);
    private final HttpSession session = mock(HttpSession.class);
    private final Command command = new ChangeLanguageCommand();

    @Test
    void testExecuteCorrectBehavior() throws IOException {
        when(request.getParameter("language")).thenReturn("EN");
        when(request.getSession()).thenReturn(session);
        assertEquals(Path.REDIRECT, command.execute(request, response));
    }
}