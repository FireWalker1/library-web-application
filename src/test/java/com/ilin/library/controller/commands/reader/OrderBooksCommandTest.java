package com.ilin.library.controller.commands.reader;

import com.ilin.library.TestHelper;
import com.ilin.library.controller.Path;
import com.ilin.library.controller.commands.Command;
import com.ilin.library.dto.BookDTO;
import com.ilin.library.dto.SubscriptionDTO;
import com.ilin.library.services.SubscriptionService;
import com.ilin.library.services.implementation.SubscriptionServiceImpl;
import com.ilin.library.utils.AddressBuilder;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class OrderBooksCommandTest {
    private final HttpServletResponse response = mock(HttpServletResponse.class);
    private final HttpServletRequest request = mock(HttpServletRequest.class);
    private final SubscriptionService service = mock(SubscriptionServiceImpl.class);
    private final Command command = new OrderBooksCommand(service);
    private final HttpSession session = mock(HttpSession.class);
    @Test
    void testExecuteCorrectBehavior() throws IOException {
        when(request.getSession()).thenReturn(session);
        when(session.getAttribute("login")).thenReturn("Igor");
        List<BookDTO> books = TestHelper.getTestBookDTOList();
        when(session.getAttribute("bookCart")).thenReturn(books);
        when(request.getParameter("type")).thenReturn("home");
        Map<String, String> info = new HashMap<>();
        when(service.getUserFines("Igor")).thenReturn(0);
        SubscriptionDTO dto = new SubscriptionDTO();
        dto.setBooks(books);
        dto.setType("home");
        when(service.insertSubscription(dto, "Igor")).thenReturn(true);
        assertEquals(Path.REDIRECT, command.execute(request, response));
        verify(session).setAttribute("bookCart", new ArrayList<BookDTO>());
        verify(response).sendRedirect(Path.PAGE_SUCCESS);
    }
    @Test
    void testExecuteIncorrectBehavior() throws IOException {
        when(request.getSession()).thenReturn(session);
        when(session.getAttribute("login")).thenReturn("Igor");
        List<BookDTO> books = TestHelper.getTestBookDTOList();
        when(session.getAttribute("bookCart")).thenReturn(books);
        when(request.getParameter("type")).thenReturn("home");
        Map<String, String> info = new HashMap<>();
        when(service.getUserFines("Igor")).thenReturn(0);
        SubscriptionDTO dto = new SubscriptionDTO();
        dto.setBooks(books);
        dto.setType("home");
        when(service.insertSubscription(dto, "Igor")).thenReturn(false);
        assertEquals(Path.REDIRECT, command.execute(request, response));
        verify(response).sendRedirect(Path.PAGE_ERROR);
    }
    @Test
    void testExecuteEmptyCart() throws IOException {
        when(request.getSession()).thenReturn(session);
        when(session.getAttribute("login")).thenReturn("Igor");
        List<BookDTO> books = new ArrayList<>();
        when(session.getAttribute("bookCart")).thenReturn(books);
        when(request.getParameter("type")).thenReturn("home");
        Map<String, String> info = new HashMap<>();
        info.put("errorText", "cart.error_no_books");
        when(service.getUserFines("Igor")).thenReturn(0);
        assertEquals(Path.REDIRECT, command.execute(request, response));
        verify(response).sendRedirect(Path.PAGE_CART + AddressBuilder.formPageProperties("", info));
    }
    @Test
    void testExecuteTooManyBooks() throws IOException {
        when(request.getSession()).thenReturn(session);
        when(session.getAttribute("login")).thenReturn("Igor");
        List<BookDTO> books = TestHelper.getTestBookDTOList();
        books.add(new BookDTO());
        books.add(new BookDTO());
        books.add(new BookDTO());
        books.add(new BookDTO());
        when(session.getAttribute("bookCart")).thenReturn(books);
        when(request.getParameter("type")).thenReturn("home");
        Map<String, String> info = new HashMap<>();
        info.put("errorText", "cart.error_too_much_books");
        when(service.getUserFines("Igor")).thenReturn(0);
        assertEquals(Path.REDIRECT, command.execute(request, response));
        verify(response).sendRedirect(Path.PAGE_CART + AddressBuilder.formPageProperties("", info));
    }
    @Test
    void testExecuteUnpaidFine() throws IOException {
        when(request.getSession()).thenReturn(session);
        when(session.getAttribute("login")).thenReturn("Igor");
        List<BookDTO> books = TestHelper.getTestBookDTOList();
        when(session.getAttribute("bookCart")).thenReturn(books);
        when(request.getParameter("type")).thenReturn("home");
        Map<String, String> info = new HashMap<>();
        info.put("errorText", "cart.error_have_unpaid_fine");
        when(service.getUserFines("Igor")).thenReturn(20);
        assertEquals(Path.REDIRECT, command.execute(request, response));
        verify(response).sendRedirect(Path.PAGE_CART + AddressBuilder.formPageProperties("", info));
    }
}