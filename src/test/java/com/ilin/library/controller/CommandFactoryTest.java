package com.ilin.library.controller;

import com.ilin.library.controller.commands.Command;
import com.ilin.library.controller.commands.CommandFactory;
import com.ilin.library.controller.commands.unrestricted.BookSearchCommand;
import jakarta.servlet.http.HttpServletRequest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class CommandFactoryTest {
    private final CommandFactory factory = CommandFactory.commandFactory();
    private final HttpServletRequest request = mock(HttpServletRequest.class);


    @Test
    void testGetCommandCorrectInput() {
        when(request.getParameter("action")).thenReturn("bookSearch");
        Command command = factory.getCommand(request);
        Assertions.assertInstanceOf(BookSearchCommand.class, command);
    }

    @Test
    void testGetCommandIncorrectInput() {
        when(request.getParameter("action")).thenReturn("incorrect");
        Command command = factory.getCommand(request);
        Assertions.assertNull(command);
    }

    @Test
    void testGetCommandNullInput() {
        when(request.getParameter("action")).thenReturn(null);
        Command command = factory.getCommand(request);
        Assertions.assertNull(command);
    }
}
