package com.ilin.library;

import com.ilin.library.dto.AuthorDTO;
import com.ilin.library.dto.BookDTO;
import com.ilin.library.dto.SubscriptionDTO;
import com.ilin.library.dto.UserDTO;
import com.ilin.library.entities.Author;
import com.ilin.library.entities.Book;
import com.ilin.library.entities.Subscription;
import com.ilin.library.entities.User;
import com.ilin.library.entities.enums.Language;
import com.ilin.library.entities.enums.Role;
import com.ilin.library.entities.enums.Status;
import com.ilin.library.entities.enums.Type;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public class TestHelper {
    public static Book getTestBook() {
        Author author1 = new Author(1, "Joanne Rowling", "Джоан Роулінг");
        return new Book(1, "Гаррі Поттер і Орден Фенікса", author1, "Leprechaun", 2015, Language.UA, 2);
    }

    public static BookDTO getTestBookDTO() {
        AuthorDTO author1 = new AuthorDTO(1, "Joanne Rowling", "Джоан Роулінг");
        return new BookDTO(1, "Гаррі Поттер і Орден Фенікса", author1, "Leprechaun", 2015, "UA", 2);
    }

    public static List<Book> getTestBookList() {
        List<Book> list = new ArrayList<>();
        Author author1 = new Author(1, "Joanne Rowling", "Джоан Роулінг");
        Author author2 = new Author(2, "Ivan Franko", "Іван Франко");
        Book book1 = new Book(1, "Гаррі Поттер і Орден Фенікса", author1, "Leprechaun", 2015, Language.UA, 2);
        Book book2 = new Book(2, "Оксамитові джерела", author2, "Фоліо", 2020, Language.UA, 1);
        list.add(book1);
        list.add(book2);
        return list;
    }

    public static List<BookDTO> getTestBookDTOList() {
        List<BookDTO> list = new ArrayList<>();
        AuthorDTO author1 = new AuthorDTO(1, "Joanne Rowling", "Джоан Роулінг");
        AuthorDTO author2 = new AuthorDTO(2, "Ivan Franko", "Іван Франко");
        BookDTO book1 = new BookDTO(1, "Гаррі Поттер і Орден Фенікса", author1, "Leprechaun", 2015, "UA", 2);
        BookDTO book2 = new BookDTO(2, "Оксамитові джерела", author2, "Фоліо", 2020, "UA", 1);
        list.add(book1);
        list.add(book2);
        return list;
    }

    public static User getTestUser() {
        return new User(1, "Igor", "email@site.com", "$2a$10$aAFscXxylWf9c3t1eC0IKuC2SZvfZFbOKZhp9mtx6HfGQpQEDXxZ2", "639940558",
                Role.READER, "Igor", "Igorenko", null);
    }

    public static UserDTO getTestUserDTO() {
        return new UserDTO(1, "Igor", "email@site.com", "$2a$10$aAFscXxylWf9c3t1eC0IKuC2SZvfZFbOKZhp9mtx6HfGQpQEDXxZ2", "639940558",
                "role.reader", "Igor", "Igorenko", null);
    }

    public static List<User> getTestUserList() {
        List<User> list = new ArrayList<>();
        list.add(getTestUser());
        list.add(new User(1, "Viktor", "email2@site.com", "$2a$10$aAFscXxylWf9c3t1eC0IKuC2SZvfZFbOKZhp9mtx6HfGQpQEDXxZ2", "639940559",
                Role.LIBRARIAN, "Viktor", "Viktorenko", null));
        return list;
    }

    public static List<UserDTO> getTestUserDTOList() {
        List<UserDTO> list = new ArrayList<>();
        list.add(getTestUserDTO());
        list.add(new UserDTO(1, "Viktor", "email2@site.com", "$2a$10$aAFscXxylWf9c3t1eC0IKuC2SZvfZFbOKZhp9mtx6HfGQpQEDXxZ2", "639940559",
                "role.librarian", "Viktor", "Viktorenko", null));
        return list;
    }

    public static Subscription getTestSubscription() {
        Subscription subscription = new Subscription();
        subscription.setId(1);
        subscription.setUser(getTestUser());
        subscription.setType(Type.HOME);
        subscription.setStatus(Status.REQUESTED);
        subscription.setCreatedAt(new Timestamp(21244567));
        subscription.setUpdatedAt(new Timestamp(21244567));
        subscription.setBookReturnDeadline(null);
        subscription.setFine(new BigDecimal(0));
        subscription.setBooks(getTestBookList());
        return subscription;
    }

    public static SubscriptionDTO getTestSubscriptionDTO() {
        SubscriptionDTO subscription = new SubscriptionDTO();
        subscription.setId(1);
        subscription.setUser(getTestUserDTO());
        subscription.setType("type.home");
        subscription.setStatus("status.requested");
        subscription.setCreatedAt(new Timestamp(21244567));
        subscription.setUpdatedAt(new Timestamp(21244567));
        subscription.setBookReturnDeadline(null);
        subscription.setFine(new BigDecimal(0));
        subscription.setBooks(getTestBookDTOList());
        return subscription;
    }

    public static List<Subscription> getTestSubscriptionList() {
        List<Subscription> list = new ArrayList<>();
        list.add(getTestSubscription());
        list.add(getTestSubscription());
        return list;
    }

    public static List<SubscriptionDTO> getTestSubscriptionDTOList() {
        List<SubscriptionDTO> list = new ArrayList<>();
        list.add(getTestSubscriptionDTO());
        list.add(getTestSubscriptionDTO());
        return list;
    }
}
