package com.ilin.library.services;

import com.ilin.library.dao.SubscriptionDAO;
import com.ilin.library.entities.Subscription;
import com.ilin.library.entities.enums.Status;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.*;

class FineServiceTest {
    private final SubscriptionDAO dao = mock(SubscriptionDAO.class);
    private final FineUpdater service = new FineUpdater(dao);

    @Test
    void testRunCorrectBehavior() {
        List<Subscription> list = new ArrayList<>();
        Subscription subscription = new Subscription();
        subscription.setStatus(Status.ONGOING);
        subscription.setBookReturnDeadline(new Date(1));
        subscription.setId(1);
        list.add(subscription);
        when(dao.getOngoingSubscriptions()).thenReturn(list);
        service.run();
        verify(dao).updateFine(BigDecimal.valueOf(5), 1);
    }
}