package com.ilin.library.services;

import com.ilin.library.TestHelper;
import com.ilin.library.dao.UserDAO;
import com.ilin.library.dto.UserDTO;
import com.ilin.library.entities.User;
import com.ilin.library.entities.enums.Role;
import com.ilin.library.services.implementation.UserServiceImpl;
import org.junit.jupiter.api.Test;

import java.sql.Timestamp;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class UserServiceImplTest {
    private final UserDAO userDAO = mock(UserDAO.class);
    private final UserService service = new UserServiceImpl(userDAO);

    @Test
    void testGetFullListCorrectBehavior() {
        when(userDAO.getFullList(1)).thenReturn(TestHelper.getTestUserList());
        when(userDAO.getFullList("login", "true", 3)).thenReturn(TestHelper.getTestUserList());
        when(userDAO.getFullList("login", "true", 1)).thenReturn(TestHelper.getTestUserList());
        assertEquals(TestHelper.getTestUserDTOList(), service.getFullList("login", "true", 3));
        assertEquals(TestHelper.getTestUserDTOList(), service.getFullList("login", "true", 0));
        assertEquals(TestHelper.getTestUserDTOList(), service.getFullList(null, "true", 0));
    }

    @Test
    void testGetFullReadersListCorrectBehavior() {
        when(userDAO.getFullReadersList(1)).thenReturn(TestHelper.getTestUserList());
        when(userDAO.getFullReadersList("login", "true", 3)).thenReturn(TestHelper.getTestUserList());
        when(userDAO.getFullReadersList("login", "true", 1)).thenReturn(TestHelper.getTestUserList());
        assertEquals(TestHelper.getTestUserDTOList(), service.getFullReadersList("login", "true", 3));
        assertEquals(TestHelper.getTestUserDTOList(), service.getFullReadersList("login", "true", 0));
        assertEquals(TestHelper.getTestUserDTOList(), service.getFullReadersList(null, "true", 0));
    }

    @Test
    void testGetByLoginAndPasswordCorrectBehavior() {
        when(userDAO.getByLogin("Igor")).thenReturn(TestHelper.getTestUser());
        assertEquals(TestHelper.getTestUserDTO(), service.getByLoginAndPassword("Igor", "11111"));
    }

    @Test
    void testGetByLoginAndPasswordIncorrectBehavior() {
        when(userDAO.getByLogin("Igor")).thenReturn(TestHelper.getTestUser());
        assertNull(service.getByLoginAndPassword("Igor", "22222"));
    }

    @Test
    void testGetByLoginCorrectBehavior() {
        when(userDAO.getByLogin("Igor")).thenReturn(TestHelper.getTestUser());
        assertEquals(TestHelper.getTestUserDTO(), service.getByLogin("Igor"));
    }

    @Test
    void testIsSuchLoginExistsCorrectBehavior() {
        when(userDAO.getByLogin("Igor")).thenReturn(TestHelper.getTestUser());
        when(userDAO.getByLogin("Vasya")).thenReturn(new User());
        assertTrue(service.isSuchLoginExists("Igor"));
        assertFalse(service.isSuchLoginExists("Vasya"));
    }

    @Test
    void testIsSuchEmailExistsCorrectBehavior() {
        when(userDAO.getByEmail("email@site.com")).thenReturn(TestHelper.getTestUser());
        when(userDAO.getByEmail("incorrect@site.com")).thenReturn(new User());
        assertTrue(service.isSuchEmailExists("email@site.com"));
        assertFalse(service.isSuchEmailExists("incorrect@site.com"));
    }

    @Test
    void testIsSuchPhoneNumberExistsCorrectBehavior() {
        when(userDAO.getByPhoneNumber("636662222")).thenReturn(TestHelper.getTestUser());
        when(userDAO.getByPhoneNumber("989997766")).thenReturn(new User());
        assertTrue(service.isSuchPhoneNumberExists("636662222"));
        assertFalse(service.isSuchPhoneNumberExists("989997766"));
    }

    @Test
    void testInsertUserCorrectBehavior() {
        when(userDAO.insertUser(any())).thenReturn(true);
        UserDTO dto = TestHelper.getTestUserDTO();
        assertTrue(service.insertUser(dto));
    }

    @Test
    void testUpdateUserCorrectBehavior() {
        when(userDAO.updateLogin(any(String.class), any(String.class))).thenReturn(true);
        when(userDAO.updateEmail(any(String.class), any(String.class))).thenReturn(true);
        when(userDAO.updatePassword(any(String.class), any(String.class))).thenReturn(true);
        when(userDAO.updatePhoneNumber(any(String.class), any(String.class))).thenReturn(true);
        when(userDAO.updateFirstName(any(String.class), any(String.class))).thenReturn(true);
        when(userDAO.updateLastName(any(String.class), any(String.class))).thenReturn(true);
        assertTrue(service.updateUser("login", "Vasya", "Igor"));
        assertTrue(service.updateUser("password", "password", "Igor"));
        assertTrue(service.updateUser("email", "email", "Igor"));
        assertTrue(service.updateUser("phoneNumber", "2352346", "Igor"));
        assertTrue(service.updateUser("firstName", "name", "Igor"));
        assertTrue(service.updateUser("lastName", "name", "Igor"));
    }

    @Test
    void testGetPageQuantityReadersCorrectBehavior() {
        when(userDAO.getRowsQuantityReaders()).thenReturn(25);
        assertEquals(3, service.getPageQuantityReaders());
    }

    @Test
    void testGetPageQuantityUsersCorrectBehavior() {
        when(userDAO.getRowsQuantityUsers()).thenReturn(40);
        assertEquals(4, service.getPageQuantityUsers());
    }

    @Test
    void testDeleteLibrarianCorrectBehavior() {
        User user = TestHelper.getTestUser();
        user.setRole(Role.LIBRARIAN);
        when(userDAO.getByLogin("Igor")).thenReturn(user);
        when(userDAO.getByLogin("Vasyl")).thenReturn(TestHelper.getTestUser());
        when(userDAO.deleteUser("Igor")).thenReturn(true);
        assertTrue(service.deleteLibrarian("Igor"));
        assertFalse(service.deleteLibrarian("Vasyl"));
    }

    @Test
    void testChangeBlockStatusCorrectBehavior() {
        when(userDAO.getByLogin("Igor")).thenReturn(TestHelper.getTestUser());
        User user = TestHelper.getTestUser();
        user.setBlockedAt(new Timestamp(3523532));
        when(userDAO.getByLogin("Vasyl")).thenReturn(user);
        when(userDAO.updateBlockedAt(any(Timestamp.class), any(String.class))).thenReturn(true);
        when(userDAO.updateBlockedAt(null, "Vasyl")).thenReturn(true);
        assertTrue(service.changeBlockStatus("Igor"));
        assertTrue(service.changeBlockStatus("Vasyl"));
    }
}
