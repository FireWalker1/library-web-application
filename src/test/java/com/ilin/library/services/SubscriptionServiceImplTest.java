package com.ilin.library.services;

import com.ilin.library.TestHelper;
import com.ilin.library.dao.BookDAO;
import com.ilin.library.dao.SubscriptionDAO;
import com.ilin.library.entities.Subscription;
import com.ilin.library.entities.enums.Status;
import com.ilin.library.services.implementation.SubscriptionServiceImpl;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.sql.Date;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class SubscriptionServiceImplTest {
    private final SubscriptionDAO subscriptionDAO = mock(SubscriptionDAO.class);
    private final BookDAO bookDAO = mock(BookDAO.class);
    private final SubscriptionService service = new SubscriptionServiceImpl(subscriptionDAO, bookDAO);

    @Test
    void testInsertSubscriptionCorrectBehavior() {
        when(subscriptionDAO.insertSubscription(any(Subscription.class), any(String.class))).thenReturn(true);
        assertTrue(service.insertSubscription(TestHelper.getTestSubscriptionDTO(), "Igor"));
    }

    @Test
    void testGetUserSubscriptionsCorrectBehavior() {
        when(subscriptionDAO.getUserSubscriptions(any(String.class))).thenReturn(TestHelper.getTestSubscriptionList());
        assertEquals(TestHelper.getTestSubscriptionDTOList(), service.getUserSubscriptions("Igor"));
    }

    @Test
    void testGetSubscriptionByIDCorrectBehavior() {
        when(subscriptionDAO.getSubscriptionByID(1)).thenReturn(TestHelper.getTestSubscription());
        when(bookDAO.getBySubscriptionID(1)).thenReturn(TestHelper.getTestBookList());
        assertEquals(TestHelper.getTestSubscriptionDTO(), service.getSubscriptionByID(1));
    }

    @Test
    void testGetFullListCorrectBehavior() {
        when(subscriptionDAO.getFullList(1)).thenReturn(TestHelper.getTestSubscriptionList());
        when(subscriptionDAO.getFullList("id", "true", 1))
                .thenReturn(TestHelper.getTestSubscriptionList());
        assertEquals(TestHelper.getTestSubscriptionDTOList(), service.getFullList("id", "true", 0));
        assertEquals(TestHelper.getTestSubscriptionDTOList(), service.getFullList("id", "true", 1));
        assertEquals(TestHelper.getTestSubscriptionDTOList(), service.getFullList(null, null, 1));
    }

    @Test
    void testGetPageQuantityCorrectBehavior() {
        when(subscriptionDAO.getRowsQuantity()).thenReturn(25);
        assertEquals(3, service.getPageQuantity());
    }

    @Test
    void testProgressStatusCorrectBehavior() {
        Subscription subscription = TestHelper.getTestSubscription();
        when(subscriptionDAO.getSubscriptionByID(1)).thenReturn(subscription);
        Subscription subscription2 = TestHelper.getTestSubscription();
        subscription2.setStatus(Status.CONFIRMED);
        when(subscriptionDAO.getSubscriptionByID(2)).thenReturn(subscription2);
        Subscription subscription3 = TestHelper.getTestSubscription();
        subscription3.setStatus(Status.ONGOING);
        when(subscriptionDAO.getSubscriptionByID(3)).thenReturn(subscription3);
        Subscription subscription4 = TestHelper.getTestSubscription();
        subscription4.setStatus(Status.CLOSED);
        when(bookDAO.getBySubscriptionID(3)).thenReturn(TestHelper.getTestBookList());
        when(subscriptionDAO.getSubscriptionByID(4)).thenReturn(subscription4);
        when(subscriptionDAO.updateStatusToConfirmed(1)).thenReturn(true);
        when(subscriptionDAO.updateStatusToOngoing(eq(2), any(Date.class))).thenReturn(true);
        when(subscriptionDAO.updateStatusToOngoing(eq(2), any(Date.class))).thenReturn(true);
        when(subscriptionDAO.updateStatusToOngoing(eq(2), any(Date.class))).thenReturn(true);
        when(subscriptionDAO.updateStatusToOngoing(eq(2), any(Date.class))).thenReturn(true);
        when(subscriptionDAO.updateStatusToClosed(3, TestHelper.getTestBookList())).thenReturn(true);

        assertTrue(service.progressStatus(1, null));
        assertTrue(service.progressStatus(2, "1"));
        assertTrue(service.progressStatus(2, "2"));
        assertTrue(service.progressStatus(2, "3"));
        assertTrue(service.progressStatus(2, "4"));
        assertTrue(service.progressStatus(3, null));
        assertFalse(service.progressStatus(4, null));
    }

    @Test
    void testRejectSubscriptionCorrectBehavior() {
        when(subscriptionDAO.getSubscriptionByID(1)).thenReturn(TestHelper.getTestSubscription());
        Subscription subscriptionClosed = TestHelper.getTestSubscription();
        subscriptionClosed.setStatus(Status.CLOSED);
        when(subscriptionDAO.getSubscriptionByID(2)).thenReturn(subscriptionClosed);
        when(bookDAO.getBySubscriptionID(1)).thenReturn(TestHelper.getTestBookList());
        when(subscriptionDAO.updateStatusToClosed(1, TestHelper.getTestBookList())).thenReturn(true);

        assertTrue(service.rejectSubscription(1));
        assertFalse(service.rejectSubscription(2));
    }

    @Test
    void testPayFineCorrectBehavior() {
        when(subscriptionDAO.updateFine(BigDecimal.ZERO, 1)).thenReturn(true);
        assertTrue(service.payFine(1));
        assertFalse(service.payFine(0));
    }

    @Test
    void testGetUserFinesCorrectBehavior() {
        when(subscriptionDAO.getUserFines("Igor")).thenReturn(0);
        when(subscriptionDAO.getUserFines("Vasyl")).thenReturn(30);
        assertEquals(0, service.getUserFines("Igor"));
        assertEquals(30, service.getUserFines("Vasyl"));
    }
}