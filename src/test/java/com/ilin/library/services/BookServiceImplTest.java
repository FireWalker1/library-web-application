package com.ilin.library.services;

import com.ilin.library.TestHelper;
import com.ilin.library.dao.BookDAO;
import com.ilin.library.dto.BookDTO;
import com.ilin.library.entities.Book;
import com.ilin.library.entities.enums.Language;
import com.ilin.library.services.implementation.BookServiceImpl;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class BookServiceImplTest {
    private final BookDAO bookDAO = mock(BookDAO.class);
    private final BookService service = new BookServiceImpl(bookDAO);

    @Test
    void testGetSearchResultListCorrectBehavior() {
        when(bookDAO.getSearchResultList("id", "true", 2, new String[]{"Rowling", "Franko"}))
                .thenReturn(TestHelper.getTestBookList());
        assertEquals(TestHelper.getTestBookDTOList(), service.getSearchResultList(new String[]{"Rowling", "Franko"},
                "id", "true", 2));
    }

    @Test
    void testGetSearchResultListIncorrectBehavior() {
        when(bookDAO.getSearchResultList("id", "true", 2, new String[]{"Rowling", "Franko"}))
                .thenReturn(TestHelper.getTestBookList());
        assertNotEquals(TestHelper.getTestBookDTOList(), service.getSearchResultList(new String[]{"Franko"},
                "id", "true", 2));
    }

    @Test
    void testGetFullListCorrectBehavior() {
        when(bookDAO.getFullList()).thenReturn(TestHelper.getTestBookList());
        assertEquals(TestHelper.getTestBookDTOList(), service.getFullList());
    }

    @Test
    void testAddBookToCartCorrectBehavior() {
        when(bookDAO.getByID(1)).thenReturn(TestHelper.getTestBook());
        when(bookDAO.updateQuantity(1, 1)).thenReturn(true);
        BookDTO dto = TestHelper.getTestBookDTO();
        dto.setQuantity(dto.getQuantity() - 1);
        assertEquals(TestHelper.getTestBookDTO(), service.addBookToCart(1));
    }

    @Test
    void testAddBookToCartIncorrectBehavior() {
        when(bookDAO.getByID(57)).thenReturn(new Book());
        when(bookDAO.updateQuantity(1, 1)).thenReturn(true);
        BookDTO dto = TestHelper.getTestBookDTO();
        dto.setQuantity(dto.getQuantity() - 1);
        assertNotEquals(TestHelper.getTestBookDTO(), service.addBookToCart(57));
    }

    @Test
    void testGetByIDCorrectBehavior() {
        when(bookDAO.getByID(1)).thenReturn(TestHelper.getTestBook());
        assertEquals(TestHelper.getTestBookDTO(), service.getByID(1));
    }

    @Test
    void testGetByIDIncorrectBehavior() {
        when(bookDAO.getByID(57)).thenReturn(new Book());
        assertNotEquals(TestHelper.getTestBookDTO(), service.getByID(57));
    }

    @Test
    void testDeleteBookFromCartCorrectBehavior() {
        when(bookDAO.updateQuantity(1, 3)).thenReturn(true);
        assertTrue(service.deleteBookFromCart(TestHelper.getTestBookDTO()));
    }

    @Test
    void testDeleteBookFromCartIncorrectBehavior() {
        when(bookDAO.updateQuantity(1, 3)).thenReturn(false);
        assertFalse(service.deleteBookFromCart(TestHelper.getTestBookDTO()));
    }

    @Test
    void testGetPageQuantityCorrectBehavior() {
        String[] str1 = {"Franko", "Lesya"};
        String[] str2 = new String[0];
        when(bookDAO.getRowsQuantity(str1)).thenReturn(25);
        when(bookDAO.getRowsQuantity(str2)).thenReturn(37);
        assertEquals(3, service.getPageQuantity(str1));
        assertEquals(4, service.getPageQuantity());
    }

    @Test
    void testGetPageQuantityIncorrectBehavior() {
        String[] str1 = {"Franko", "Lesya"};
        String[] str2 = new String[0];
        when(bookDAO.getRowsQuantity(str1)).thenReturn(20);
        when(bookDAO.getRowsQuantity(str2)).thenReturn(37);
        assertNotEquals(3, service.getPageQuantity(str1));
        assertNotEquals(5, service.getPageQuantity());
    }

    @Test
    void testCreateBookCorrectBehavior() {
        Book book = TestHelper.getTestBook();
        assertEquals(TestHelper.getTestBookDTO(), service.createBook(book.getTitle(), book.getAuthor().getNameUA(),
                book.getAuthor().getNameEN(), book.getPublisherHouse(), book.getYear(), book.getLanguage().toString(),
                book.getQuantity()));
    }

    @Test
    void testCreateBookIncorrectBehavior() {
        Book book = TestHelper.getTestBook();
        book.setLanguage(Language.EN);
        assertNotEquals(TestHelper.getTestBookDTO(), service.createBook(book.getTitle(), book.getAuthor().getNameUA(),
                book.getAuthor().getNameEN(), book.getPublisherHouse(), book.getYear(), book.getLanguage().toString(),
                book.getQuantity()));
    }

    @Test
    void testIsSuchBookExistsCorrectBehavior() {
        Book book = TestHelper.getTestBook();
        BookDTO dto = TestHelper.getTestBookDTO();
        when(bookDAO.getBook(book)).thenReturn(book);
        assertTrue(service.isSuchBookExists(dto));
        assertTrue(service.isSuchBookExists(dto, "title", dto.getTitle()));
    }

    @Test
    void testIsSuchBookExistsIncorrectBehavior() {
        Book book = TestHelper.getTestBook();
        BookDTO dto = TestHelper.getTestBookDTO();
        when(bookDAO.getBook(book)).thenReturn(new Book());
        assertFalse(service.isSuchBookExists(dto));
        assertFalse(service.isSuchBookExists(dto, "title", dto.getTitle()));
    }

    @Test
    void testInsertBookCorrectBehavior() {
        when(bookDAO.insertBook(TestHelper.getTestBook())).thenReturn(true);
        assertTrue(service.insertBook(TestHelper.getTestBookDTO()));
    }

    @Test
    void testInsertBookIncorrectBehavior() {
        when(bookDAO.insertBook(TestHelper.getTestBook())).thenReturn(false);
        assertFalse(service.insertBook(TestHelper.getTestBookDTO()));
    }

    @Test
    void testUpdateBookCorrectBehavior() {
        when(bookDAO.updateTitle(1, "Title")).thenReturn(true);
        when(bookDAO.updateAuthorNameUA(1, "Name")).thenReturn(true);
        when(bookDAO.updateAuthorNameEN(1, "Name")).thenReturn(true);
        when(bookDAO.updatePublisherHouse(1, "House")).thenReturn(true);
        when(bookDAO.updateYear(1, 2008)).thenReturn(true);
        when(bookDAO.updateLanguage(1, Language.EN)).thenReturn(true);
        when(bookDAO.updateQuantity(1, 5)).thenReturn(true);
        assertTrue(service.updateBook(1, "title", "Title"));
        assertTrue(service.updateBook(1, "authorNameUA", "Name"));
        assertTrue(service.updateBook(1, "authorNameEN", "Name"));
        assertTrue(service.updateBook(1, "publisherHouse", "House"));
        assertTrue(service.updateBook(1, "year", "2008"));
        assertTrue(service.updateBook(1, "language", "EN"));
        assertTrue(service.updateBook(1, "quantity", "5"));
    }

    @Test
    void testDeleteBookCorrectBehavior() {
        when(bookDAO.deleteBook(1)).thenReturn(true);
        assertFalse(service.deleteBook(0));
        assertTrue(service.deleteBook(1));
    }
}