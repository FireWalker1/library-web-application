package com.ilin.library.utils;

import com.ilin.library.TestHelper;
import com.ilin.library.dto.BookDTO;
import jakarta.servlet.http.HttpSession;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ValidatorTest {
    @Test
    void testCheckFirstNameCorrectBehavior() {
        Assertions.assertNull(Validator.checkFirstName("Valeriy"));
    }

    @Test
    void testCheckFirstNameIncorrectBehavior() {
        Assertions.assertEquals(Validator.FIRST_NAME_ERROR, Validator.checkFirstName("B"));
    }

    @Test
    void testCheckLastNameCorrectBehavior() {
        Assertions.assertNull(Validator.checkLastName("Kovalenko"));
    }

    @Test
    void testCheckLastNameIncorrectBehavior() {
        Assertions.assertEquals(Validator.LAST_NAME_ERROR, Validator.checkLastName("B"));
    }

    @Test
    void testCheckLoginCorrectBehavior() {
        Assertions.assertNull(Validator.checkLogin("Viktor"));
    }

    @Test
    void testCheckLoginIncorrectBehavior() {
        Assertions.assertEquals(Validator.LOGIN_ERROR, Validator.checkLogin("*"));
    }

    @Test
    void testCheckPasswordCorrectBehavior() {
        Assertions.assertNull(Validator.checkPassword("ViktorPass"));
    }

    @Test
    void testCheckPasswordIncorrectBehavior() {
        Assertions.assertEquals(Validator.PASSWORD_ERROR, Validator.checkPassword("123"));
    }

    @Test
    void testCheckEmailCorrectBehavior() {
        Assertions.assertNull(Validator.checkEmail("email@site.com"));
    }

    @Test
    void testCheckEmailIncorrectBehavior() {
        Assertions.assertEquals(Validator.EMAIL_ERROR, Validator.checkEmail("incorrect"));
    }

    @Test
    void testCheckPhoneNumberCorrectBehavior() {
        Assertions.assertNull(Validator.checkPhoneNumber("634879976"));
    }

    @Test
    void testCheckPhoneNumberIncorrectBehavior() {
        Assertions.assertEquals(Validator.PHONE_NUMBER_ERROR, Validator.checkPhoneNumber("235235"));
    }

    @Test
    void testCheckUserFieldValueCorrectBehavior() {
        Assertions.assertNull(Validator.checkUserFieldValue("password", "12345"));
    }

    @Test
    void testCheckUserFieldValueIncorrectBehavior() {
        Assertions.assertEquals(Validator.PASSWORD_ERROR, Validator.checkUserFieldValue("password", "1"));
    }

    @Test
    void testCheckBookFieldValueCorrectBehavior() {
        Assertions.assertNull(Validator.checkBookFieldValue("title", "Good title"));
    }

    @Test
    void testCheckBookFieldValueIncorrectBehavior() {
        Assertions.assertEquals(Validator.TITLE_ERROR, Validator.checkBookFieldValue("title", "1"));
    }

    @Test
    void testCheckTitleCorrectBehavior() {
        Assertions.assertNull(Validator.checkTitle("Good title"));
    }

    @Test
    void testCheckTitleIncorrectBehavior() {
        Assertions.assertEquals(Validator.TITLE_ERROR, Validator.checkTitle("4"));
    }

    @Test
    void testCheckAuthorNameUACorrectBehavior() {
        Assertions.assertNull(Validator.checkAuthorNameUA("Тарас Шевченко"));
    }

    @Test
    void testCheckAuthorNameUAIncorrectBehavior() {
        Assertions.assertEquals(Validator.AUTHOR_NAME_ERROR, Validator.checkAuthorNameUA("2"));
    }

    @Test
    void testCheckAuthorNameENCorrectBehavior() {
        Assertions.assertNull(Validator.checkAuthorNameEN("Taras Shevchenko"));
    }

    @Test
    void testCheckAuthorNameENIncorrectBehavior() {
        Assertions.assertEquals(Validator.AUTHOR_NAME_ERROR, Validator.checkAuthorNameEN("2"));
    }

    @Test
    void testCheckPublisherHouseCorrectBehavior() {
        Assertions.assertNull(Validator.checkPublisherHouse("Publisher"));
    }

    @Test
    void testCheckPublisherHouseIncorrectBehavior() {
        Assertions.assertEquals(Validator.PUBLISHER_HOUSE_ERROR, Validator.checkPublisherHouse("2"));
    }

    @Test
    void testCheckYearCorrectBehavior() {
        Assertions.assertNull(Validator.checkYear(1998));
    }

    @Test
    void testCheckYearIncorrectBehavior() {
        Assertions.assertEquals(Validator.YEAR_ERROR, Validator.checkYear(12));
    }

    @Test
    void testCheckQuantityCorrectBehavior() {
        Assertions.assertNull(Validator.checkQuantity(1998));
    }

    @Test
    void testCheckQuantityIncorrectBehavior() {
        Assertions.assertEquals(Validator.QUANTITY_ERROR, Validator.checkQuantity(-5));
    }

    @Test
    void testValidateIntCorrectBehavior() {
        Assertions.assertEquals(25, Validator.validateInt("25"));
    }

    @Test
    void testValidateIntIncorrectBehavior() {
        Assertions.assertEquals(-1, Validator.validateInt("number"));
    }

    @Test
    void testValidateBookListAttributeCorrectBehavior() {
        HttpSession session = mock(HttpSession.class);
        List<BookDTO> list = new ArrayList<>();
        list.add(TestHelper.getTestBookDTO());
        when(session.getAttribute("attribute")).thenReturn(list);
        Assertions.assertEquals(list, Validator.validateBookListAttribute(session, "attribute"));
    }

    @Test
    void testValidateBookListAttributeIncorrectBehavior() {
        HttpSession session = mock(HttpSession.class);
        when(session.getAttribute("attribute")).thenReturn("String");
        Assertions.assertEquals(new ArrayList<>(), Validator.validateBookListAttribute(session, "attribute"));
    }
}
