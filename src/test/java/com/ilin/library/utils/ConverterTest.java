package com.ilin.library.utils;

import com.ilin.library.TestHelper;
import com.ilin.library.dto.AuthorDTO;
import com.ilin.library.dto.BookDTO;
import com.ilin.library.dto.SubscriptionDTO;
import com.ilin.library.dto.UserDTO;
import com.ilin.library.entities.Author;
import com.ilin.library.entities.Book;
import com.ilin.library.entities.Subscription;
import com.ilin.library.entities.User;
import com.ilin.library.entities.enums.Language;
import com.ilin.library.entities.enums.Role;
import com.ilin.library.entities.enums.Status;
import com.ilin.library.entities.enums.Type;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;

public class ConverterTest {
    @Test
    void testConvertBookToDTOCorrectBehavior() {
        Assertions.assertEquals(TestHelper.getTestBookDTO(), Converter.convertBookToDTO(TestHelper.getTestBook()));
    }

    @Test
    void testConvertBookToDTOIncorrectBehavior() {
        Book book = TestHelper.getTestBook();
        book.setTitle("Incorrect Title");
        Assertions.assertNotEquals(TestHelper.getTestBookDTO(), Converter.convertBookToDTO(book));
        book = TestHelper.getTestBook();
        book.setAuthor(new Author(3, "Incorrect Author,", "Неправильний Автор"));
        Assertions.assertNotEquals(TestHelper.getTestBookDTO(), Converter.convertBookToDTO(book));
        book = TestHelper.getTestBook();
        book.setPublisherHouse("Incorrect Publisher");
        Assertions.assertNotEquals(TestHelper.getTestBookDTO(), Converter.convertBookToDTO(book));
        book = TestHelper.getTestBook();
        book.setYear(1995);
        Assertions.assertNotEquals(TestHelper.getTestBookDTO(), Converter.convertBookToDTO(book));
        book = TestHelper.getTestBook();
        book.setLanguage(Language.EN);
        Assertions.assertNotEquals(TestHelper.getTestBookDTO(), Converter.convertBookToDTO(book));
    }

    @Test
    void testConvertDTOToBookCorrectBehavior() {
        Assertions.assertEquals(TestHelper.getTestBook(), Converter.convertDTOToBook(TestHelper.getTestBookDTO()));
    }

    @Test
    void testConvertDTOToBookIncorrectBehavior() {
        BookDTO book = TestHelper.getTestBookDTO();
        book.setTitle("Incorrect Title");
        Assertions.assertNotEquals(TestHelper.getTestBook(), Converter.convertDTOToBook(book));
        book = TestHelper.getTestBookDTO();
        book.setAuthor(new AuthorDTO(3, "Incorrect Author,", "Неправильний Автор"));
        Assertions.assertNotEquals(TestHelper.getTestBook(), Converter.convertDTOToBook(book));
        book = TestHelper.getTestBookDTO();
        book.setPublisherHouse("Incorrect Publisher");
        Assertions.assertNotEquals(TestHelper.getTestBook(), Converter.convertDTOToBook(book));
        book = TestHelper.getTestBookDTO();
        book.setYear(1995);
        Assertions.assertNotEquals(TestHelper.getTestBook(), Converter.convertDTOToBook(book));
        book = TestHelper.getTestBookDTO();
        book.setLanguage("EN");
        Assertions.assertNotEquals(TestHelper.getTestBook(), Converter.convertDTOToBook(book));
    }

    @Test
    void testConvertDTOListToBookListCorrectBehavior() {
        Assertions.assertEquals(TestHelper.getTestBookList(),
                Converter.convertDTOListToBookList(TestHelper.getTestBookDTOList()));
    }

    @Test
    void testConvertDTOListToBookListIncorrectBehavior() {
        List<BookDTO> list = TestHelper.getTestBookDTOList();
        list.add(TestHelper.getTestBookDTO());
        Assertions.assertNotEquals(TestHelper.getTestBookList(),
                Converter.convertDTOListToBookList(list));
    }

    @Test
    void testConvertBookListToDTOListCorrectBehavior() {
        Assertions.assertEquals(TestHelper.getTestBookDTOList(),
                Converter.convertBookListToDTOList(TestHelper.getTestBookList()));
    }

    @Test
    void testConvertBookListToDTOListIncorrectBehavior() {
        List<Book> list = TestHelper.getTestBookList();
        list.add(TestHelper.getTestBook());
        Assertions.assertNotEquals(TestHelper.getTestBookDTOList(),
                Converter.convertBookListToDTOList(list));
    }

    @Test
    void testConvertUserToDTOCorrectBehavior() {
        Assertions.assertEquals(TestHelper.getTestUserDTO(), Converter.convertUserToDTO(TestHelper.getTestUser()));
    }

    @Test
    void testConvertUserToDTOIncorrectBehavior() {
        User user = TestHelper.getTestUser();
        user.setLogin("Incorrect login");
        Assertions.assertNotEquals(TestHelper.getTestUserDTO(), Converter.convertUserToDTO(user));
        user = TestHelper.getTestUser();
        user.setEmail("incorrect@email.com");
        Assertions.assertNotEquals(TestHelper.getTestUserDTO(), Converter.convertUserToDTO(user));
        user = TestHelper.getTestUser();
        user.setPassword("incorrect password");
        Assertions.assertNotEquals(TestHelper.getTestUserDTO(), Converter.convertUserToDTO(user));
        user = TestHelper.getTestUser();
        user.setPhoneNumber("999999999");
        Assertions.assertNotEquals(TestHelper.getTestUserDTO(), Converter.convertUserToDTO(user));
        user = TestHelper.getTestUser();
        user.setRole(Role.ADMINISTRATOR);
        Assertions.assertNotEquals(TestHelper.getTestUserDTO(), Converter.convertUserToDTO(user));
        user = TestHelper.getTestUser();
        user.setFirstName("Name");
        Assertions.assertNotEquals(TestHelper.getTestUserDTO(), Converter.convertUserToDTO(user));
        user = TestHelper.getTestUser();
        user.setLastName("Surname");
        Assertions.assertNotEquals(TestHelper.getTestUserDTO(), Converter.convertUserToDTO(user));
    }

    @Test
    void testConvertDTOToUserCorrectBehavior() {
        Assertions.assertEquals(TestHelper.getTestUser(), Converter.convertDTOToUser(TestHelper.getTestUserDTO()));
    }

    @Test
    void testConvertDTOToUserIncorrectBehavior() {
        UserDTO user = TestHelper.getTestUserDTO();
        user.setLogin("Incorrect login");
        Assertions.assertNotEquals(TestHelper.getTestUser(), Converter.convertDTOToUser(user));
        user = TestHelper.getTestUserDTO();
        user.setEmail("incorrect@email.com");
        Assertions.assertNotEquals(TestHelper.getTestUser(), Converter.convertDTOToUser(user));
        user = TestHelper.getTestUserDTO();
        user.setPassword("incorrect password");
        Assertions.assertNotEquals(TestHelper.getTestUser(), Converter.convertDTOToUser(user));
        user = TestHelper.getTestUserDTO();
        user.setPhoneNumber("999999999");
        Assertions.assertNotEquals(TestHelper.getTestUser(), Converter.convertDTOToUser(user));
        user = TestHelper.getTestUserDTO();
        user.setRole("role.administrator");
        Assertions.assertNotEquals(TestHelper.getTestUser(), Converter.convertDTOToUser(user));
        user = TestHelper.getTestUserDTO();
        user.setFirstName("Name");
        Assertions.assertNotEquals(TestHelper.getTestUser(), Converter.convertDTOToUser(user));
        user = TestHelper.getTestUserDTO();
        user.setLastName("Surname");
        Assertions.assertNotEquals(TestHelper.getTestUser(), Converter.convertDTOToUser(user));
    }

    @Test
    void testConvertUserListToDTOListCorrectBehavior() {
        Assertions.assertEquals(TestHelper.getTestUserDTOList(),
                Converter.convertUserListToDTOList(TestHelper.getTestUserList()));
    }

    @Test
    void testConvertUserListToDTOListIncorrectBehavior() {
        List<User> list = TestHelper.getTestUserList();
        list.add(TestHelper.getTestUser());
        Assertions.assertNotEquals(TestHelper.getTestUserDTOList(),
                Converter.convertUserListToDTOList(list));
    }

    @Test
    void testConvertSubscriptionToDTOCorrectBehavior() {
        Assertions.assertEquals(TestHelper.getTestSubscriptionDTO(),
                Converter.convertSubscriptionToDTO(TestHelper.getTestSubscription()));
    }

    @Test
    void testConvertSubscriptionToDTOIncorrectBehavior() {
        Subscription subscription = TestHelper.getTestSubscription();
        subscription.setUser(new User(1, "Viktor", "email2@site.com", "password2", "639940559",
                Role.LIBRARIAN, "Viktor", "Viktorenko", null));
        Assertions.assertNotEquals(TestHelper.getTestSubscriptionDTO(), Converter.convertSubscriptionToDTO(subscription));
        subscription = TestHelper.getTestSubscription();
        subscription.setType(Type.READING_ROOM);
        Assertions.assertNotEquals(TestHelper.getTestSubscriptionDTO(), Converter.convertSubscriptionToDTO(subscription));
        subscription = TestHelper.getTestSubscription();
        subscription.setStatus(Status.ONGOING);
        Assertions.assertNotEquals(TestHelper.getTestSubscriptionDTO(), Converter.convertSubscriptionToDTO(subscription));
        subscription = TestHelper.getTestSubscription();
        subscription.setCreatedAt(new Timestamp(121343));
        Assertions.assertNotEquals(TestHelper.getTestSubscriptionDTO(), Converter.convertSubscriptionToDTO(subscription));
        subscription = TestHelper.getTestSubscription();
        subscription.setUpdatedAt(new Timestamp(121343));
        Assertions.assertNotEquals(TestHelper.getTestSubscriptionDTO(), Converter.convertSubscriptionToDTO(subscription));
        subscription = TestHelper.getTestSubscription();
        subscription.setBookReturnDeadline(new Date(123556));
        Assertions.assertNotEquals(TestHelper.getTestSubscriptionDTO(), Converter.convertSubscriptionToDTO(subscription));
        subscription = TestHelper.getTestSubscription();
        subscription.setFine(new BigDecimal(10));
        Assertions.assertNotEquals(TestHelper.getTestSubscriptionDTO(), Converter.convertSubscriptionToDTO(subscription));
        subscription = TestHelper.getTestSubscription();
        subscription.setBooks(null);
        Assertions.assertNotEquals(TestHelper.getTestSubscriptionDTO(), Converter.convertSubscriptionToDTO(subscription));
    }

    @Test
    void testConvertDTOToSubscriptionCorrectBehavior() {
        Assertions.assertEquals(TestHelper.getTestSubscription(),
                Converter.convertDTOToSubscription(TestHelper.getTestSubscriptionDTO()));
    }

    @Test
    void testConvertDTOToSubscriptionIncorrectBehavior() {
        SubscriptionDTO subscription = TestHelper.getTestSubscriptionDTO();
        subscription.setUser(new UserDTO(1, "Viktor", "email2@site.com", "password2", "639940559",
                "role.librarian", "Viktor", "Viktorenko", null));
        Assertions.assertNotEquals(TestHelper.getTestSubscription(), Converter.convertDTOToSubscription(subscription));
        subscription = TestHelper.getTestSubscriptionDTO();
        subscription.setType("type.reading_room");
        Assertions.assertNotEquals(TestHelper.getTestSubscription(), Converter.convertDTOToSubscription(subscription));
        subscription = TestHelper.getTestSubscriptionDTO();
        subscription.setStatus("status.ongoing");
        Assertions.assertNotEquals(TestHelper.getTestSubscription(), Converter.convertDTOToSubscription(subscription));
        subscription = TestHelper.getTestSubscriptionDTO();
        subscription.setCreatedAt(new Timestamp(121343));
        Assertions.assertNotEquals(TestHelper.getTestSubscription(), Converter.convertDTOToSubscription(subscription));
        subscription = TestHelper.getTestSubscriptionDTO();
        subscription.setUpdatedAt(new Timestamp(121343));
        Assertions.assertNotEquals(TestHelper.getTestSubscription(), Converter.convertDTOToSubscription(subscription));
        subscription = TestHelper.getTestSubscriptionDTO();
        subscription.setBookReturnDeadline(new Date(123556));
        Assertions.assertNotEquals(TestHelper.getTestSubscription(), Converter.convertDTOToSubscription(subscription));
        subscription = TestHelper.getTestSubscriptionDTO();
        subscription.setFine(new BigDecimal(10));
        Assertions.assertNotEquals(TestHelper.getTestSubscription(), Converter.convertDTOToSubscription(subscription));
        subscription = TestHelper.getTestSubscriptionDTO();
        subscription.setBooks(null);
        Assertions.assertNotEquals(TestHelper.getTestSubscription(), Converter.convertDTOToSubscription(subscription));
    }

    @Test
    void testConvertSubscriptionListToDTOListCorrectBehavior() {
        Assertions.assertEquals(TestHelper.getTestSubscriptionDTOList(),
                Converter.convertSubscriptionListToDTOList(TestHelper.getTestSubscriptionList()));
    }

    @Test
    void testConvertSubscriptionListToDTOListIncorrectBehavior() {
        List<Subscription> list = TestHelper.getTestSubscriptionList();
        list.add(TestHelper.getTestSubscription());
        Assertions.assertNotEquals(TestHelper.getTestSubscriptionDTOList(),
                Converter.convertSubscriptionListToDTOList(list));
    }
}